0.4.0
========
 * Changes:
    o The phone can hold up to 4 URLs per contact. The KAB can only
    hold one. Worked around this limitation using insertCustom(). The
    same goes for the notes.

    o Added the Addressee view dialog. Using it, you can seethe details
    of a contact, in a single dialog. You can access it by double-clicking
    the wanted contact.

    o Implemented caller group awareness and name customisation.

    o Added a right-click menu with options:
      * Transfer to KAB
      * Update in phone
      * Show contact details

    o Reverted from using an .rc file to store the overriden phone entries
      to using a KABC::AddressBook (vcf list) file.

    o Configuration changes no longer require a program restart.

    o Added customisable name-surname seperator string.

    o KnokiiSync now discriminates between 1,2,3,3+ word names. You can
      configure how it recognise these names seperately.

    o Added a "Name configuration" dialog, which appears for each name class
      (1,2,3,3+ words) you configured KnokiiSync to ask.

    o You can now delete the overriden data of a contact by clicking the "Clear
      override data" button in the Addressee view window.

    o Started using IDs for phone numbers and addresses too. This is essential
      for syncing.

0.3.1
========
 * Changes:

    o Fixed a bug with three word names (eg. "John T. Smith")
    not being overriden properly.

    o Fixed a buglet in the configuration dialog (index out of range
    for the memory type combo box).

    o Fixed a crash that happened when you tried to read from an empty
    memory type (eg from an empty SIM card)

    o Fixed an issue with the progress dialogs not refreshing properly
    (ie. the window appeared as if it was frozen)

    o Fixed a bug where the status of the telephone was not properly
    shown in the status bar.

    o The default action of quiting when KnokiiSync could not connect to
    the phone has been reverted to just displaying an error message.

    o Fixed a bug where the memory type used was being passed as an i18n
    string, where a default of "Phone", "SIM" or "Phone + SIM" was
    expected.

    o Changed the progress dialogs from modal type, to non-modal.

    o Added Addressee view dialog (Read only for now)


0.3.0
========
 * Changes:

    o Overriden name entries on the phone now use the IMEI of
    the phone, so it is now possible to use different phones. (Idea: Giovanni Bobbio)

    o Broken the address column to more columns (Adrress,
    Locality, Region, Postal Code, Country). This should help
    a lot in the future.

    o Configuration option for postal address mismatch is now
    functional.

    o You can now override the contents of the various address
    fields if you have a address format mismatch.
    
    o The code for reading and displaying the entries is now split;
    There is two function for reading the entries (one for the KAB
    and one for the phone) and one for displaying them (again, one
    for the KAB and one for the phone).

    o When read, the entries are stored in KABC::Addressee objects
    in memory. This should make syncing in future versions a snap.

    o New configuration option: Set formatted name in KAB.

    o Bugfix: If a contact on the KAB didn't have a prefered phone set,
    then trying to transfer that contact on the phone would fail. This
    has been fixed, by setting the prefered phone to the first one available,
    and if the contact has no phone numbers (eg. a internet-only contact),
    then the first available email is used.

    o Added option to hide all those ignored/empty/erroneous entries.

    o Added i18n support.

    o Added Greek translation (el)

    o Added French translation (fr)

    o Added Polish translation (pl)
