/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <kdebug.h>
#include <kmessagebox.h>
#include <klocale.h>

void
NamesDialog::init ()
{
	curAddressee = 0;

	typesList.append (typeCB1);
	typesList.append (typeCB2);
	typesList.append (typeCB3);

	namesList.append (nameCB1);
	namesList.append (nameCB2);
	namesList.append (nameCB3);

	nameCB1->setEditable (TRUE);
	nameCB2->setEditable (TRUE);
	nameCB3->setEditable (TRUE);

	typeCB1->clear();
	typeCB2->clear();
	typeCB3->clear();

	mTypes.append ( "");
	mTypes.append (i18n ("Name"));
	mTypes.append (i18n ("Surname"));
	mTypes.append (i18n ("Additional Name"));

	typeCB1->insertStringList (mTypes, 0);
	typeCB2->insertStringList (mTypes, 0);
	typeCB3->insertStringList (mTypes, 0);

	mTypes.remove ( "");

	rememberChkBox->setChecked (TRUE);
}

void
NamesDialog::askName (KABC::Addressee * addressee, QString separator)
{
	QString tmp;
	QStringList names;
	QComboBox * nqcb = 0, * tqcb = 0;

	curAddressee = addressee;

	tmp = addressee->custom ("KnokiiSync", "E_NAMEINPHONE");
	names = QStringList::split (separator, tmp);

	QStringList::Iterator nIt = names.begin ();
	QPtrListIterator<QComboBox> tqcbIt (typesList);
	QPtrListIterator<QComboBox> nqcbIt (namesList);

	frmtNameLE->setText (tmp);

	while ((nqcb = nqcbIt.current()) != 0 && (tqcb = tqcbIt.current()) != 0)
		{
			++tqcbIt;
			++nqcbIt;

			if (nIt != names.end ())
				{
					nqcb->insertStringList (names);
					nqcb->setCurrentText (*nIt);

					++nIt;
				}
			else
				{
					tqcb->setEnabled (FALSE);
					nqcb->setEnabled (FALSE);
					break;
				}
		}
	show ();
}

void
NamesDialog::OKButtonClicked ()
{
	QComboBox * nqcb = 0, * tqcb = 0;
	QPtrListIterator<QComboBox> tqcbIt (typesList);
	QPtrListIterator<QComboBox> nqcbIt (namesList);

	QString formattedName = frmtNameLE->text ();
	if (formattedName != "")
		curAddressee->setFormattedName (formattedName);
	else
		{
			int reply = KMessageBox::warningYesNo (0, i18n ("You have not set a formatted name for this contact. If you do not do so, you will not be able to upload this contact to the phone. Do you wish to go back and set a formatted name?"));
			if (reply == KMessageBox::Yes)
				return;
		}

	while ((nqcb = nqcbIt.current ()) != 0 && (tqcb = tqcbIt.current ()) != 0)
		{
			++tqcbIt;
			++nqcbIt;

			if (tqcb->isEnabled () == FALSE)
				break;

			if (tqcb->currentText () == i18n ("Name"))
				{
					curAddressee->setGivenName (nqcb->currentText ());
					curAddressee->insertCustom ("KnokiiSync", "E_NAME_OV", "1");
				}
			else
				if (tqcb->currentText () == i18n ("Surname"))
					{
						curAddressee->setFamilyName (nqcb->currentText ());
						curAddressee->insertCustom ("KnokiiSync", "E_SURNAME_OV", "1");
					}
				else
					if (tqcb->currentText () == i18n ("Additional Name"))
						{
							curAddressee->setAdditionalName (nqcb->currentText ());
							curAddressee->insertCustom ("KnokiiSync", "E_ADDNAME_OV", "1");
						}
		}

	if (rememberChkBox->isChecked ())
		curAddressee->insertCustom ("KnokiiSync", "E_REMEMBER", "1");

	hide ();
	emit OKClicked ();
}

void
NamesDialog::cancelButtonClicked ()
{
	hide ();
	emit cancelClicked ();
}

void
NamesDialog::clear ()
{
	nameCB1->clear ();
	nameCB2->clear ();
	nameCB3->clear ();
}


void
NamesDialog::typeCB1Changed (const QString & type)
{
	int count = 0;
	QStringList types = mTypes;
	QString currentText;

	if (type != "")
		{
			count = typeCB2->count ();
			for (int i = 0; i < count; i++)
				{
					if (typeCB2->text (i) == type)
						{
							typeCB2->removeItem (i);
							break;
						}
				}
			count = typeCB3->count ();
			for (int i = 0; i < count; i++)
				{
					if (typeCB3->text (i) == type)
						{
							typeCB3->removeItem (i);
							break;
						}
				}
		}
	else
		{
			types.remove (typeCB2->currentText ());
			types.remove (typeCB3->currentText ());

			typeCB1->clear ();
			typeCB1->insertItem ("", 0);
			typeCB1->insertStringList (types, 1);

			currentText = typeCB2->currentText ();
			typeCB2->clear ();
			typeCB2->insertItem ("", 0);
			typeCB2->insertItem ("", 0);
			typeCB2->insertStringList (types, 1);
			typeCB2->setCurrentText (currentText);

			currentText = typeCB3->currentText ();
			typeCB3->clear ();
			typeCB3->insertItem ("", 0);
			typeCB3->insertItem ("", 0);
			typeCB3->insertStringList (types, 1);
			typeCB3->setCurrentText (currentText);
		}
}

void
NamesDialog::typeCB2Changed (const QString & type)
{
	int count = 0;
	QStringList types = mTypes;
	QString currentText;

	if (type != "")
		{
			count = typeCB1->count ();
			for (int i = 0; i < count; i++)
				{
					if (typeCB1->text (i) == type)
						{
							typeCB1->removeItem (i);
							break;
						}
				}
			count = typeCB3->count ();
			for (int i = 0; i < count; i++)
				{
					if (typeCB3->text (i) == type)
						{
							typeCB3->removeItem (i);
							break;
						}
				}
		}
	else
		{
			types.remove (typeCB1->currentText ());
			types.remove (typeCB3->currentText ());

			typeCB2->clear ();
			typeCB2->insertItem ("", 0);
			typeCB2->insertStringList (types, 1);

			currentText = typeCB1->currentText ();
			typeCB1->clear ();
			typeCB1->insertItem ("", 0);
			typeCB1->insertItem ("", 0);
			typeCB1->insertStringList (types, 1);
			typeCB1->setCurrentText (currentText);

			currentText = typeCB3->currentText ();
			typeCB3->clear ();
			typeCB3->insertItem ("", 0);
			typeCB3->insertItem ("", 0);
			typeCB3->insertStringList (types, 1);
			typeCB3->setCurrentText (currentText);
		}
}

void
NamesDialog::typeCB3Changed (const QString & type)
{
	int count = 0;
	QStringList types = mTypes;
	QString currentText;

	if (type != "")
		{
			count = typeCB1->count ();
			for (int i = 0; i < count; i++)
				{
					if (typeCB1->text (i) == type)
						{
							typeCB1->removeItem (i);
							break;
						}
				}
			count = typeCB2->count ();
			for (int i = 0; i < count; i++)
				{
					if (typeCB2->text (i) == type)
						{
							typeCB2->removeItem (i);
							break;
						}
				}
		}
	else
		{
			types.remove (typeCB1->currentText ());
			types.remove (typeCB2->currentText ());

			typeCB3->clear ();
			typeCB3->insertItem ("", 0);
			typeCB3->insertStringList (types, 1);

			currentText = typeCB1->currentText ();
			typeCB1->clear ();
			typeCB1->insertItem ("", 0);
			typeCB1->insertItem ("", 0);
			typeCB1->insertStringList (types, 1);
			typeCB1->setCurrentText (currentText);

			currentText = typeCB2->currentText ();
			typeCB2->clear ();
			typeCB2->insertItem ("", 0);
			typeCB2->insertItem ("", 0);
			typeCB2->insertStringList (types, 1);
			typeCB2->setCurrentText (currentText);
		}
}
