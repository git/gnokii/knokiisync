/****************************************************************************
** Form implementation generated from reading ui file 'addresseedlg.ui'
**
** Created: Σαβ Αύγ 16 18:20:37 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "addresseedlg.h"

#include <qvariant.h>
#include <qcombobox.h>
#include <qframe.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "addresseedlg.ui.h"
static const char* const image0_data[] = { 
"16 16 2 1",
". c None",
"# c #000000",
"................",
"................",
"................",
"..############..",
".##############.",
"################",
"################",
"###...####...###",
"......####......",
".....######.....",
"....###..###....",
"...####..####...",
"..############..",
"..############..",
"................",
"................"};

static const char* const image1_data[] = { 
"16 16 2 1",
". c None",
"# c #000000",
"....##..........",
"....##..........",
"....#########...",
"....#.......#...",
"....#.#####.#...",
"....#.#...#.#...",
"....#.#####.#...",
"....#.......#...",
"....#.#.#.#.#...",
"....#.......#...",
"....#.#.#.#.#...",
"....#.......#...",
"....#.#.#.#.#...",
"....#.......#...",
"....#########...",
"................"};

static const char* const image2_data[] = { 
"16 16 2 1",
". c None",
"# c #000000",
"................",
"....########....",
"...##......##...",
"..##........##..",
".##..........##.",
".##..........##.",
"..############..",
"..#..........#..",
"..#..........#..",
"..#..........#..",
"..#...####...#..",
"..#...#..#...#..",
"..#...#..#...#..",
"..#...#..#...#..",
"..############..",
"................"};

static const char* const image3_data[] = { 
"16 16 2 1",
". c None",
"# c #000000",
"................",
"............##..",
".....#....#.##..",
"....##...##.##..",
"...###..###.##..",
"..####.####.##..",
".##############.",
".##############.",
".##############.",
".##############.",
".##############.",
".##############.",
".##############.",
".##############.",
".##############.",
"................"};

static const char* const image4_data[] = { 
"16 16 2 1",
". c None",
"# c #000000",
"................",
"................",
"................",
"..##########....",
"..#.........#...",
"..#..........#..",
"..#..........#..",
"..#..........#..",
".##..........##.",
".##..........##.",
".##############.",
".##############.",
".###########.##.",
".##############.",
".##############.",
"................"};


/* 
 *  Constructs a AddresseeDlg as a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
AddresseeDlg::AddresseeDlg( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
,
      image0( (const char **) image0_data ),
      image1( (const char **) image1_data ),
      image2( (const char **) image2_data ),
      image3( (const char **) image3_data ),
      image4( (const char **) image4_data )
{
    if ( !name )
	setName( "AddresseeDlg" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setMinimumSize( QSize( 440, 400 ) );
    AddresseeDlgLayout = new QGridLayout( this, 1, 1, 11, 6, "AddresseeDlgLayout"); 

    tabWidget2 = new QTabWidget( this, "tabWidget2" );

    tab = new QWidget( tabWidget2, "tab" );
    tabLayout = new QGridLayout( tab, 1, 1, 11, 6, "tabLayout"); 

    nameInPhoneLE = new QLineEdit( tab, "nameInPhoneLE" );
    nameInPhoneLE->setReadOnly( TRUE );

    tabLayout->addWidget( nameInPhoneLE, 0, 1 );

    nameLE = new QLineEdit( tab, "nameLE" );
    nameLE->setFrameShape( QLineEdit::LineEditPanel );
    nameLE->setFrameShadow( QLineEdit::Sunken );

    tabLayout->addWidget( nameLE, 1, 1 );

    surnameLE = new QLineEdit( tab, "surnameLE" );

    tabLayout->addWidget( surnameLE, 2, 1 );

    textLabel1_3 = new QLabel( tab, "textLabel1_3" );

    tabLayout->addWidget( textLabel1_3, 0, 0 );

    textLabel2 = new QLabel( tab, "textLabel2" );

    tabLayout->addWidget( textLabel2, 1, 0 );

    textLabel1 = new QLabel( tab, "textLabel1" );
    textLabel1->setFrameShape( QLabel::NoFrame );
    textLabel1->setFrameShadow( QLabel::Plain );

    tabLayout->addWidget( textLabel1, 2, 0 );

    textLabel1_4 = new QLabel( tab, "textLabel1_4" );

    tabLayout->addWidget( textLabel1_4, 5, 0 );

    addNameLE = new QLineEdit( tab, "addNameLE" );
    addNameLE->setFrameShape( QLineEdit::LineEditPanel );
    addNameLE->setFrameShadow( QLineEdit::Sunken );

    tabLayout->addWidget( addNameLE, 3, 1 );

    textLabel2_3 = new QLabel( tab, "textLabel2_3" );

    tabLayout->addWidget( textLabel2_3, 3, 0 );

    frmtdNameLE = new QLineEdit( tab, "frmtdNameLE" );
    frmtdNameLE->setFrameShape( QLineEdit::LineEditPanel );
    frmtdNameLE->setFrameShadow( QLineEdit::Sunken );

    tabLayout->addWidget( frmtdNameLE, 4, 1 );

    textLabel2_3_2 = new QLabel( tab, "textLabel2_3_2" );

    tabLayout->addWidget( textLabel2_3_2, 4, 0 );

    groupsCB = new QComboBox( FALSE, tab, "groupsCB" );

    tabLayout->addWidget( groupsCB, 5, 1 );
    tabWidget2->insertTab( tab, "" );

    tab_2 = new QWidget( tabWidget2, "tab_2" );
    tabLayout_2 = new QGridLayout( tab_2, 1, 1, 11, 6, "tabLayout_2"); 

    frame3_2 = new QFrame( tab_2, "frame3_2" );
    frame3_2->setFrameShape( QFrame::StyledPanel );
    frame3_2->setFrameShadow( QFrame::Raised );

    phoneLE2 = new QLineEdit( frame3_2, "phoneLE2" );
    phoneLE2->setGeometry( QRect( 210, 10, 210, 30 ) );

    phoneTypeCB2 = new QComboBox( FALSE, frame3_2, "phoneTypeCB2" );
    phoneTypeCB2->setGeometry( QRect( 50, 10, 151, 30 ) );

    prefPhoneRB2 = new QRadioButton( frame3_2, "prefPhoneRB2" );
    prefPhoneRB2->setGeometry( QRect( 10, 10, 30, 30 ) );

    tabLayout_2->addWidget( frame3_2, 1, 0 );

    frame3_4 = new QFrame( tab_2, "frame3_4" );
    frame3_4->setFrameShape( QFrame::StyledPanel );
    frame3_4->setFrameShadow( QFrame::Raised );

    phoneTypeCB3 = new QComboBox( FALSE, frame3_4, "phoneTypeCB3" );
    phoneTypeCB3->setGeometry( QRect( 50, 10, 151, 30 ) );

    phoneLE3 = new QLineEdit( frame3_4, "phoneLE3" );
    phoneLE3->setGeometry( QRect( 210, 10, 210, 30 ) );

    prefPhoneRB3 = new QRadioButton( frame3_4, "prefPhoneRB3" );
    prefPhoneRB3->setGeometry( QRect( 10, 10, 30, 30 ) );

    tabLayout_2->addWidget( frame3_4, 2, 0 );

    frame3_4_2 = new QFrame( tab_2, "frame3_4_2" );
    frame3_4_2->setFrameShape( QFrame::StyledPanel );
    frame3_4_2->setFrameShadow( QFrame::Raised );

    phoneLE4 = new QLineEdit( frame3_4_2, "phoneLE4" );
    phoneLE4->setGeometry( QRect( 210, 10, 210, 30 ) );

    phoneTypeCB4 = new QComboBox( FALSE, frame3_4_2, "phoneTypeCB4" );
    phoneTypeCB4->setGeometry( QRect( 50, 10, 151, 30 ) );

    prefPhoneRB4 = new QRadioButton( frame3_4_2, "prefPhoneRB4" );
    prefPhoneRB4->setGeometry( QRect( 10, 10, 30, 30 ) );

    tabLayout_2->addWidget( frame3_4_2, 3, 0 );

    frame3_4_2_2 = new QFrame( tab_2, "frame3_4_2_2" );
    frame3_4_2_2->setFrameShape( QFrame::StyledPanel );
    frame3_4_2_2->setFrameShadow( QFrame::Raised );

    phoneLE5 = new QLineEdit( frame3_4_2_2, "phoneLE5" );
    phoneLE5->setGeometry( QRect( 210, 10, 210, 30 ) );

    phoneTypeCB5 = new QComboBox( FALSE, frame3_4_2_2, "phoneTypeCB5" );
    phoneTypeCB5->setGeometry( QRect( 50, 10, 151, 30 ) );

    prefPhoneRB5 = new QRadioButton( frame3_4_2_2, "prefPhoneRB5" );
    prefPhoneRB5->setGeometry( QRect( 10, 10, 30, 30 ) );

    tabLayout_2->addWidget( frame3_4_2_2, 4, 0 );

    frame3 = new QFrame( tab_2, "frame3" );
    frame3->setFrameShape( QFrame::StyledPanel );
    frame3->setFrameShadow( QFrame::Raised );

    phoneLE1 = new QLineEdit( frame3, "phoneLE1" );
    phoneLE1->setGeometry( QRect( 210, 10, 210, 30 ) );

    phoneTypeCB1 = new QComboBox( FALSE, frame3, "phoneTypeCB1" );
    phoneTypeCB1->setGeometry( QRect( 50, 10, 151, 30 ) );
    phoneTypeCB1->setMinimumSize( QSize( 151, 0 ) );

    prefPhoneRB1 = new QRadioButton( frame3, "prefPhoneRB1" );
    prefPhoneRB1->setGeometry( QRect( 10, 10, 30, 30 ) );

    tabLayout_2->addWidget( frame3, 0, 0 );
    tabWidget2->insertTab( tab_2, "" );

    tab_3 = new QWidget( tabWidget2, "tab_3" );
    tabLayout_3 = new QGridLayout( tab_3, 1, 1, 11, 6, "tabLayout_3"); 

    frame8 = new QFrame( tab_3, "frame8" );
    frame8->setFrameShape( QFrame::StyledPanel );
    frame8->setFrameShadow( QFrame::Raised );

    textLabel3 = new QLabel( frame8, "textLabel3" );
    textLabel3->setGeometry( QRect( 10, 10, 50, 24 ) );

    emailLE1 = new QLineEdit( frame8, "emailLE1" );
    emailLE1->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_3->addWidget( frame8, 0, 0 );

    frame8_2 = new QFrame( tab_3, "frame8_2" );
    frame8_2->setFrameShape( QFrame::StyledPanel );
    frame8_2->setFrameShadow( QFrame::Raised );

    textLabel3_2 = new QLabel( frame8_2, "textLabel3_2" );
    textLabel3_2->setGeometry( QRect( 10, 10, 50, 24 ) );

    emailLE2 = new QLineEdit( frame8_2, "emailLE2" );
    emailLE2->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_3->addWidget( frame8_2, 1, 0 );

    frame8_3 = new QFrame( tab_3, "frame8_3" );
    frame8_3->setFrameShape( QFrame::StyledPanel );
    frame8_3->setFrameShadow( QFrame::Raised );

    textLabel3_3 = new QLabel( frame8_3, "textLabel3_3" );
    textLabel3_3->setGeometry( QRect( 10, 10, 50, 24 ) );

    emailLE3 = new QLineEdit( frame8_3, "emailLE3" );
    emailLE3->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_3->addWidget( frame8_3, 2, 0 );

    frame8_4 = new QFrame( tab_3, "frame8_4" );
    frame8_4->setFrameShape( QFrame::StyledPanel );
    frame8_4->setFrameShadow( QFrame::Raised );

    textLabel3_4 = new QLabel( frame8_4, "textLabel3_4" );
    textLabel3_4->setGeometry( QRect( 9, 10, 50, 24 ) );

    emailLE4 = new QLineEdit( frame8_4, "emailLE4" );
    emailLE4->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_3->addWidget( frame8_4, 3, 0 );
    tabWidget2->insertTab( tab_3, "" );

    tab_4 = new QWidget( tabWidget2, "tab_4" );
    tabLayout_4 = new QGridLayout( tab_4, 1, 1, 11, 6, "tabLayout_4"); 

    frame8_5 = new QFrame( tab_4, "frame8_5" );
    frame8_5->setFrameShape( QFrame::StyledPanel );
    frame8_5->setFrameShadow( QFrame::Raised );

    textLabel3_5 = new QLabel( frame8_5, "textLabel3_5" );
    textLabel3_5->setGeometry( QRect( 10, 10, 50, 24 ) );

    urlLE1 = new QLineEdit( frame8_5, "urlLE1" );
    urlLE1->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_4->addWidget( frame8_5, 0, 0 );

    frame8_6 = new QFrame( tab_4, "frame8_6" );
    frame8_6->setFrameShape( QFrame::StyledPanel );
    frame8_6->setFrameShadow( QFrame::Raised );

    textLabel3_6 = new QLabel( frame8_6, "textLabel3_6" );
    textLabel3_6->setGeometry( QRect( 10, 10, 50, 24 ) );

    urlLE2 = new QLineEdit( frame8_6, "urlLE2" );
    urlLE2->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_4->addWidget( frame8_6, 1, 0 );

    frame8_7 = new QFrame( tab_4, "frame8_7" );
    frame8_7->setFrameShape( QFrame::StyledPanel );
    frame8_7->setFrameShadow( QFrame::Raised );

    textLabel3_7 = new QLabel( frame8_7, "textLabel3_7" );
    textLabel3_7->setGeometry( QRect( 10, 10, 50, 24 ) );

    urlLE3 = new QLineEdit( frame8_7, "urlLE3" );
    urlLE3->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_4->addWidget( frame8_7, 2, 0 );

    frame8_8 = new QFrame( tab_4, "frame8_8" );
    frame8_8->setFrameShape( QFrame::StyledPanel );
    frame8_8->setFrameShadow( QFrame::Raised );

    textLabel3_8 = new QLabel( frame8_8, "textLabel3_8" );
    textLabel3_8->setGeometry( QRect( 10, 10, 50, 24 ) );

    urlLE4 = new QLineEdit( frame8_8, "urlLE4" );
    urlLE4->setGeometry( QRect( 71, 10, 310, 25 ) );

    tabLayout_4->addWidget( frame8_8, 3, 0 );
    tabWidget2->insertTab( tab_4, "" );

    tab_5 = new QWidget( tabWidget2, "tab_5" );
    tabLayout_5 = new QGridLayout( tab_5, 1, 1, 11, 6, "tabLayout_5"); 

    tabWidget3 = new QTabWidget( tab_5, "tabWidget3" );

    tab_6 = new QWidget( tabWidget3, "tab_6" );
    tabLayout_6 = new QGridLayout( tab_6, 1, 1, 11, 6, "tabLayout_6"); 

    textLabel1_2 = new QLabel( tab_6, "textLabel1_2" );

    tabLayout_6->addWidget( textLabel1_2, 0, 0 );

    textLabel1_2_2 = new QLabel( tab_6, "textLabel1_2_2" );

    tabLayout_6->addWidget( textLabel1_2_2, 1, 0 );

    textLabel1_2_2_2 = new QLabel( tab_6, "textLabel1_2_2_2" );

    tabLayout_6->addWidget( textLabel1_2_2_2, 2, 0 );

    textLabel1_2_2_3 = new QLabel( tab_6, "textLabel1_2_2_3" );

    tabLayout_6->addWidget( textLabel1_2_2_3, 3, 0 );

    textLabel1_2_2_4 = new QLabel( tab_6, "textLabel1_2_2_4" );

    tabLayout_6->addWidget( textLabel1_2_2_4, 4, 0 );

    streetLE1 = new QLineEdit( tab_6, "streetLE1" );

    tabLayout_6->addWidget( streetLE1, 0, 1 );

    localityLE1 = new QLineEdit( tab_6, "localityLE1" );

    tabLayout_6->addWidget( localityLE1, 1, 1 );

    regionLE1 = new QLineEdit( tab_6, "regionLE1" );

    tabLayout_6->addWidget( regionLE1, 2, 1 );

    postalCodeLE1 = new QLineEdit( tab_6, "postalCodeLE1" );

    tabLayout_6->addWidget( postalCodeLE1, 3, 1 );

    countryLE1 = new QLineEdit( tab_6, "countryLE1" );

    tabLayout_6->addWidget( countryLE1, 4, 1 );
    tabWidget3->insertTab( tab_6, "" );

    tab_7 = new QWidget( tabWidget3, "tab_7" );
    tabLayout_7 = new QGridLayout( tab_7, 1, 1, 11, 6, "tabLayout_7"); 

    textLabel1_2_2_4_2 = new QLabel( tab_7, "textLabel1_2_2_4_2" );

    tabLayout_7->addWidget( textLabel1_2_2_4_2, 4, 0 );

    textLabel1_2_2_2_2 = new QLabel( tab_7, "textLabel1_2_2_2_2" );

    tabLayout_7->addWidget( textLabel1_2_2_2_2, 2, 0 );

    textLabel1_2_2_5 = new QLabel( tab_7, "textLabel1_2_2_5" );

    tabLayout_7->addWidget( textLabel1_2_2_5, 1, 0 );

    textLabel1_2_2_3_2 = new QLabel( tab_7, "textLabel1_2_2_3_2" );

    tabLayout_7->addWidget( textLabel1_2_2_3_2, 3, 0 );

    textLabel1_2_3 = new QLabel( tab_7, "textLabel1_2_3" );

    tabLayout_7->addWidget( textLabel1_2_3, 0, 0 );

    streetLE2 = new QLineEdit( tab_7, "streetLE2" );

    tabLayout_7->addWidget( streetLE2, 0, 1 );

    localityLE2 = new QLineEdit( tab_7, "localityLE2" );

    tabLayout_7->addWidget( localityLE2, 1, 1 );

    regionLE2 = new QLineEdit( tab_7, "regionLE2" );

    tabLayout_7->addWidget( regionLE2, 2, 1 );

    postalCodeLE2 = new QLineEdit( tab_7, "postalCodeLE2" );

    tabLayout_7->addWidget( postalCodeLE2, 3, 1 );

    countryLE2 = new QLineEdit( tab_7, "countryLE2" );

    tabLayout_7->addWidget( countryLE2, 4, 1 );
    tabWidget3->insertTab( tab_7, "" );

    tab_8 = new QWidget( tabWidget3, "tab_8" );
    tabLayout_8 = new QGridLayout( tab_8, 1, 1, 11, 6, "tabLayout_8"); 

    textLabel1_2_2_2_3 = new QLabel( tab_8, "textLabel1_2_2_2_3" );

    tabLayout_8->addWidget( textLabel1_2_2_2_3, 2, 0 );

    textLabel1_2_2_4_3 = new QLabel( tab_8, "textLabel1_2_2_4_3" );

    tabLayout_8->addWidget( textLabel1_2_2_4_3, 4, 0 );

    textLabel1_2_2_6 = new QLabel( tab_8, "textLabel1_2_2_6" );

    tabLayout_8->addWidget( textLabel1_2_2_6, 1, 0 );

    textLabel1_2_4 = new QLabel( tab_8, "textLabel1_2_4" );

    tabLayout_8->addWidget( textLabel1_2_4, 0, 0 );

    textLabel1_2_2_3_3 = new QLabel( tab_8, "textLabel1_2_2_3_3" );

    tabLayout_8->addWidget( textLabel1_2_2_3_3, 3, 0 );

    streetLE3 = new QLineEdit( tab_8, "streetLE3" );

    tabLayout_8->addWidget( streetLE3, 0, 1 );

    localityLE3 = new QLineEdit( tab_8, "localityLE3" );

    tabLayout_8->addWidget( localityLE3, 1, 1 );

    regionLE3 = new QLineEdit( tab_8, "regionLE3" );

    tabLayout_8->addWidget( regionLE3, 2, 1 );

    postalCodeLE3 = new QLineEdit( tab_8, "postalCodeLE3" );

    tabLayout_8->addWidget( postalCodeLE3, 3, 1 );

    countryLE3 = new QLineEdit( tab_8, "countryLE3" );

    tabLayout_8->addWidget( countryLE3, 4, 1 );
    tabWidget3->insertTab( tab_8, "" );

    tab_9 = new QWidget( tabWidget3, "tab_9" );
    tabLayout_9 = new QGridLayout( tab_9, 1, 1, 11, 6, "tabLayout_9"); 

    textLabel1_2_2_2_4 = new QLabel( tab_9, "textLabel1_2_2_2_4" );

    tabLayout_9->addWidget( textLabel1_2_2_2_4, 2, 0 );

    textLabel1_2_5 = new QLabel( tab_9, "textLabel1_2_5" );

    tabLayout_9->addWidget( textLabel1_2_5, 0, 0 );

    textLabel1_2_2_3_4 = new QLabel( tab_9, "textLabel1_2_2_3_4" );

    tabLayout_9->addWidget( textLabel1_2_2_3_4, 3, 0 );

    textLabel1_2_2_4_4 = new QLabel( tab_9, "textLabel1_2_2_4_4" );

    tabLayout_9->addWidget( textLabel1_2_2_4_4, 4, 0 );

    textLabel1_2_2_7 = new QLabel( tab_9, "textLabel1_2_2_7" );

    tabLayout_9->addWidget( textLabel1_2_2_7, 1, 0 );

    streetLE4 = new QLineEdit( tab_9, "streetLE4" );

    tabLayout_9->addWidget( streetLE4, 0, 1 );

    localityLE4 = new QLineEdit( tab_9, "localityLE4" );

    tabLayout_9->addWidget( localityLE4, 1, 1 );

    regionLE4 = new QLineEdit( tab_9, "regionLE4" );

    tabLayout_9->addWidget( regionLE4, 2, 1 );

    postalCodeLE4 = new QLineEdit( tab_9, "postalCodeLE4" );

    tabLayout_9->addWidget( postalCodeLE4, 3, 1 );

    countryLE4 = new QLineEdit( tab_9, "countryLE4" );

    tabLayout_9->addWidget( countryLE4, 4, 1 );
    tabWidget3->insertTab( tab_9, "" );

    tabLayout_5->addWidget( tabWidget3, 0, 0 );
    tabWidget2->insertTab( tab_5, "" );

    tab_10 = new QWidget( tabWidget2, "tab_10" );
    tabLayout_10 = new QGridLayout( tab_10, 1, 1, 11, 6, "tabLayout_10"); 

    layout5 = new QGridLayout( 0, 1, 1, 0, 6, "layout5"); 

    charsLabel2 = new QLabel( tab_10, "charsLabel2" );
    charsLabel2->setMaximumSize( QSize( 120, 32767 ) );

    layout5->addWidget( charsLabel2, 1, 1 );

    textLabel2_2_2 = new QLabel( tab_10, "textLabel2_2_2" );

    layout5->addWidget( textLabel2_2_2, 0, 0 );

    noteLE2 = new QLineEdit( tab_10, "noteLE2" );
    noteLE2->setMaxLength( 48 );
    noteLE2->setAlignment( int( QLineEdit::AlignAuto ) );

    layout5->addWidget( noteLE2, 0, 1 );

    tabLayout_10->addLayout( layout5, 1, 0 );

    layout4 = new QGridLayout( 0, 1, 1, 0, 6, "layout4"); 

    charsLabel1 = new QLabel( tab_10, "charsLabel1" );
    charsLabel1->setMaximumSize( QSize( 120, 32767 ) );

    layout4->addWidget( charsLabel1, 1, 1 );

    noteLE1 = new QLineEdit( tab_10, "noteLE1" );
    noteLE1->setMaxLength( 48 );
    noteLE1->setAlignment( int( QLineEdit::AlignAuto ) );

    layout4->addWidget( noteLE1, 0, 1 );

    textLabel2_2 = new QLabel( tab_10, "textLabel2_2" );

    layout4->addWidget( textLabel2_2, 0, 0 );

    tabLayout_10->addLayout( layout4, 0, 0 );

    layout6 = new QGridLayout( 0, 1, 1, 0, 6, "layout6"); 

    noteLE3 = new QLineEdit( tab_10, "noteLE3" );
    noteLE3->setMaxLength( 48 );
    noteLE3->setAlignment( int( QLineEdit::AlignAuto ) );

    layout6->addWidget( noteLE3, 0, 1 );

    charsLabel3 = new QLabel( tab_10, "charsLabel3" );
    charsLabel3->setMaximumSize( QSize( 120, 32767 ) );

    layout6->addWidget( charsLabel3, 1, 1 );

    textLabel2_2_3 = new QLabel( tab_10, "textLabel2_2_3" );

    layout6->addWidget( textLabel2_2_3, 0, 0 );

    tabLayout_10->addLayout( layout6, 2, 0 );

    layout7 = new QGridLayout( 0, 1, 1, 0, 6, "layout7"); 

    charsLabel4 = new QLabel( tab_10, "charsLabel4" );
    charsLabel4->setMaximumSize( QSize( 120, 32767 ) );

    layout7->addWidget( charsLabel4, 1, 1 );

    textLabel2_2_4 = new QLabel( tab_10, "textLabel2_2_4" );

    layout7->addWidget( textLabel2_2_4, 0, 0 );

    noteLE4 = new QLineEdit( tab_10, "noteLE4" );
    noteLE4->setMaxLength( 48 );
    noteLE4->setAlignment( int( QLineEdit::AlignAuto ) );

    layout7->addWidget( noteLE4, 0, 1 );

    tabLayout_10->addLayout( layout7, 3, 0 );
    tabWidget2->insertTab( tab_10, "" );

    AddresseeDlgLayout->addMultiCellWidget( tabWidget2, 0, 0, 0, 2 );

    layout2 = new QHBoxLayout( 0, 0, 6, "layout2"); 

    OKButton = new QPushButton( this, "OKButton" );
    OKButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, OKButton->sizePolicy().hasHeightForWidth() ) );
    OKButton->setMinimumSize( QSize( 110, 30 ) );
    layout2->addWidget( OKButton );

    cancelButton = new QPushButton( this, "cancelButton" );
    cancelButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, cancelButton->sizePolicy().hasHeightForWidth() ) );
    cancelButton->setMinimumSize( QSize( 110, 30 ) );
    layout2->addWidget( cancelButton );

    AddresseeDlgLayout->addLayout( layout2, 1, 2 );
    QSpacerItem* spacer = new QSpacerItem( 80, 31, QSizePolicy::Expanding, QSizePolicy::Minimum );
    AddresseeDlgLayout->addItem( spacer, 1, 1 );

    clrOvrDataButton = new QPushButton( this, "clrOvrDataButton" );
    clrOvrDataButton->setMinimumSize( QSize( 0, 30 ) );

    AddresseeDlgLayout->addWidget( clrOvrDataButton, 1, 0 );
    languageChange();
    resize( QSize(479, 425).expandedTo(minimumSizeHint()) );

    // signals and slots connections
    connect( OKButton, SIGNAL( clicked() ), this, SLOT( OKClicked() ) );
    connect( cancelButton, SIGNAL( clicked() ), this, SLOT( cancelClicked() ) );
    connect( clrOvrDataButton, SIGNAL( clicked() ), this, SLOT( clrOvrDataClicked() ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
AddresseeDlg::~AddresseeDlg()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void AddresseeDlg::languageChange()
{
    setCaption( tr( "Entry Details" ) );
    textLabel1_3->setText( tr( "Name in phone:" ) );
    textLabel2->setText( tr( "Name:" ) );
    textLabel1->setText( tr( "Surname:" ) );
    textLabel1_4->setText( tr( "Group:" ) );
    textLabel2_3->setText( tr( "Additional Name:" ) );
    textLabel2_3_2->setText( tr( "Formatted Name:" ) );
    tabWidget2->changeTab( tab, tr( "Name" ) );
    phoneTypeCB2->clear();
    phoneTypeCB2->insertItem( image0, QString::null );
    phoneTypeCB2->insertItem( image1, QString::null );
    phoneTypeCB2->insertItem( image2, QString::null );
    phoneTypeCB2->insertItem( image3, QString::null );
    phoneTypeCB2->insertItem( image4, QString::null );
    prefPhoneRB2->setText( QString::null );
    phoneTypeCB3->clear();
    phoneTypeCB3->insertItem( image0, QString::null );
    phoneTypeCB3->insertItem( image1, QString::null );
    phoneTypeCB3->insertItem( image2, QString::null );
    phoneTypeCB3->insertItem( image3, QString::null );
    phoneTypeCB3->insertItem( image4, QString::null );
    prefPhoneRB3->setText( QString::null );
    phoneTypeCB4->clear();
    phoneTypeCB4->insertItem( image0, QString::null );
    phoneTypeCB4->insertItem( image1, QString::null );
    phoneTypeCB4->insertItem( image2, QString::null );
    phoneTypeCB4->insertItem( image3, QString::null );
    phoneTypeCB4->insertItem( image4, QString::null );
    prefPhoneRB4->setText( QString::null );
    phoneTypeCB5->clear();
    phoneTypeCB5->insertItem( image0, QString::null );
    phoneTypeCB5->insertItem( image1, QString::null );
    phoneTypeCB5->insertItem( image2, QString::null );
    phoneTypeCB5->insertItem( image3, QString::null );
    phoneTypeCB5->insertItem( image4, QString::null );
    prefPhoneRB5->setText( QString::null );
    phoneTypeCB1->clear();
    phoneTypeCB1->insertItem( image0, QString::null );
    phoneTypeCB1->insertItem( image1, QString::null );
    phoneTypeCB1->insertItem( image2, QString::null );
    phoneTypeCB1->insertItem( image3, QString::null );
    phoneTypeCB1->insertItem( image4, QString::null );
    prefPhoneRB1->setText( QString::null );
    tabWidget2->changeTab( tab_2, tr( "Telephones" ) );
    textLabel3->setText( tr( "Email 1:" ) );
    textLabel3_2->setText( tr( "Email 2:" ) );
    textLabel3_3->setText( tr( "Email 3:" ) );
    textLabel3_4->setText( tr( "Email 4:" ) );
    tabWidget2->changeTab( tab_3, tr( "Emails" ) );
    textLabel3_5->setText( tr( "URL 1:" ) );
    textLabel3_6->setText( tr( "URL 2:" ) );
    textLabel3_7->setText( tr( "URL 3:" ) );
    textLabel3_8->setText( tr( "URL 4:" ) );
    tabWidget2->changeTab( tab_4, tr( "URLs" ) );
    textLabel1_2->setText( tr( "Street:" ) );
    textLabel1_2_2->setText( tr( "Locality:" ) );
    textLabel1_2_2_2->setText( tr( "Region:" ) );
    textLabel1_2_2_3->setText( tr( "Postal Code:" ) );
    textLabel1_2_2_4->setText( tr( "Country:" ) );
    tabWidget3->changeTab( tab_6, tr( "Address 1" ) );
    textLabel1_2_2_4_2->setText( tr( "Country:" ) );
    textLabel1_2_2_2_2->setText( tr( "Region:" ) );
    textLabel1_2_2_5->setText( tr( "Locality:" ) );
    textLabel1_2_2_3_2->setText( tr( "Postal Code:" ) );
    textLabel1_2_3->setText( tr( "Street:" ) );
    tabWidget3->changeTab( tab_7, tr( "Address 2" ) );
    textLabel1_2_2_2_3->setText( tr( "Region:" ) );
    textLabel1_2_2_4_3->setText( tr( "Country:" ) );
    textLabel1_2_2_6->setText( tr( "Locality:" ) );
    textLabel1_2_4->setText( tr( "Street:" ) );
    textLabel1_2_2_3_3->setText( tr( "Postal Code:" ) );
    tabWidget3->changeTab( tab_8, tr( "Address 3" ) );
    textLabel1_2_2_2_4->setText( tr( "Region:" ) );
    textLabel1_2_5->setText( tr( "Street:" ) );
    textLabel1_2_2_3_4->setText( tr( "Postal Code:" ) );
    textLabel1_2_2_4_4->setText( tr( "Country:" ) );
    textLabel1_2_2_7->setText( tr( "Locality:" ) );
    tabWidget3->changeTab( tab_9, tr( "Address 4" ) );
    tabWidget2->changeTab( tab_5, tr( "Addresses" ) );
    charsLabel2->setText( QString::null );
    textLabel2_2_2->setText( tr( "Note 2:" ) );
    charsLabel1->setText( QString::null );
    textLabel2_2->setText( tr( "Note 1:" ) );
    charsLabel3->setText( QString::null );
    textLabel2_2_3->setText( tr( "Note 3:" ) );
    charsLabel4->setText( QString::null );
    textLabel2_2_4->setText( tr( "Note 4:" ) );
    tabWidget2->changeTab( tab_10, tr( "Notes" ) );
    OKButton->setText( tr( "&OK" ) );
    cancelButton->setText( tr( "&Cancel" ) );
    clrOvrDataButton->setText( tr( "Clear override data" ) );
}

