/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : ��� ��� 28 17:02:54 EEST 2003
    copyright            : (C) 2003 by Dimitrios Stasinopoulos
    email                : dimitrios@linea.gr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <klocale.h>
#include <kmessagebox.h>

#include "knokiisync.h"

static const char *description =
	I18N_NOOP("KnokiiSync");
// INSERT A DESCRIPTION FOR YOUR APPLICATION HERE
	
KApplication * globalKApp=0;
	
static KCmdLineOptions options[] =
{
  { "+[File]", I18N_NOOP("file to open"), 0 },
  { 0, 0, 0 }
  // INSERT YOUR COMMANDLINE OPTIONS HERE
};

int main(int argc, char *argv[])
{

	KAboutData aboutData( "knokiisync", I18N_NOOP("KnokiiSync"),
		VERSION, description, KAboutData::License_GPL,
		"(c) 2003, Dimitrios Stasinopoulos", 0, 0, "dimitrios@linea.gr");
	aboutData.addAuthor("Dimitrios Stasinopoulos",0, "dimitrios@linea.gr");
	KCmdLineArgs::init( argc, argv, &aboutData );
	KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  KApplication app;
 
  if (app.isRestored())
  {
    RESTORE(KnokiiSyncApp);
  }
  else 
  {
		globalKApp=&app;	
		KnokiiSyncApp *knokiisync = new KnokiiSyncApp();
		knokiisync->show();

		knokiisync->KApp=&app;
  }

  return app.exec();
}  
