/****************************************************************************
** Form interface generated from reading ui file 'namesdlg.ui'
**
** Created: Σαβ Αύγ 16 16:20:28 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef NAMESDIALOG_H
#define NAMESDIALOG_H

#include <qvariant.h>
#include <kabc/addressee.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QCheckBox;
class QComboBox;
class QFrame;
class QLabel;
class QLineEdit;
class QPushButton;

class NamesDialog : public QDialog
{
    Q_OBJECT

public:
    NamesDialog( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~NamesDialog();

    QFrame* frame3;
    QComboBox* nameCB1;
    QComboBox* typeCB1;
    QFrame* frame3_2;
    QComboBox* typeCB2;
    QComboBox* nameCB2;
    QFrame* frame3_2_2;
    QComboBox* typeCB3;
    QComboBox* nameCB3;
    QPushButton* OKButton;
    QPushButton* cancelButton;
    QLabel* textLabel1;
    QLineEdit* frmtNameLE;
    QCheckBox* rememberChkBox;

    KABC::Addressee * curAddressee;

public slots:
    void init();
    void askName(KABC::Addressee * addressee, QString separator);
    void OKButtonClicked();
    void cancelButtonClicked();
    void clear();
    void typeCB1Changed(const QString & type);
    void typeCB2Changed(const QString & type);
    void typeCB3Changed(const QString & type);

signals:
    void OKClicked();
    void cancelClicked();

protected:

protected slots:
    virtual void languageChange();
private:
    QPtrList<QComboBox> typesList;
    QPtrList<QComboBox> namesList;
    QStringList mNames;
    QStringList mTypes;

};

#endif // NAMESDIALOG_H
