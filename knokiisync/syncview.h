/****************************************************************************
** Form interface generated from reading ui file 'syncview.ui'
**
** Created: Κυρ Αύγ 17 15:18:23 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef SYNCVIEW_H
#define SYNCVIEW_H

#include <qvariant.h>
#include <qpixmap.h>
#include <gnokii.h>
#include <kabc/addressee.h>
#include <kprogress.h>
#include <qpopupmenu.h>
#include <qwidget.h>
#include "addresseedlg.h"
#include "namesdlg.h"
#include "overridenab.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class KListView;
class KPushButton;
class QLabel;
class QListViewItem;
class QSplitter;

class SyncView : public QWidget
{
    Q_OBJECT

public:
    SyncView( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~SyncView();

    KPushButton* syncRightButton;
    KPushButton* readKABButton;
    KPushButton* syncButton;
    KPushButton* readMobButton;
    KPushButton* syncLeftButton;
    QSplitter* splitter2;
    KListView* KABListView;
    KListView* mobListView;
    QLabel* textLabel1;
    QLabel* textLabel1_2;

    void writeToKAB( QListViewItem * phoneEntry );
    void writeToPhone( QListViewItem * KABEntry );
    void busTerminate();
    void busInit();
    void displayKAB();

public slots:
    void readKAB();
    void readMobPhBook();
    void sync();
    void syncLeft();
    void syncRight();
    void slotParseConfig();
    void itemDoubleClicked(QListViewItem * item, const QPoint & pos, int c);
    void readKABClicked();
    void readMobClicked();
    void displayMobPhBook();
    void phoneViewRightClicked( QListViewItem * item, const QPoint & pos, int column);
    void showAddressee();
    void xferGlobalItem2KAB();
    void uploadContactToPhone(KABC::Addressee * phoneAddressee, int location);
    void updateGlobalItemInPhone();
    void splitName(KABC::Addressee * addressee, QString aName);
    void loadOverridenName(KABC::Addressee * addressee, QString nameInPhone);
    void handleCodes(KABC::Addressee * addressee);
    void askNames();
    void insertIntoOverriden(KABC::Addressee * addressee);
    void addreseeOKClicked();
    void removeFromOverriden(KABC::Addressee * addressee);
    void addreseeClrOvrDataClicked();
    bool isNameOverriden(KABC::Addressee * addressee);
    void setFormattedName(KABC::Addressee * addressee);
    void namesDlgOKClicked();
    void namesDlgCancelClicked();

signals:
    void connecting();
    void connected();
    void disconnecting();
    void disconnected();
    void syncing();
    void synced();

protected:
    QGridLayout* SyncViewLayout;

protected slots:
    virtual void languageChange();
private:
    NamesDialog * namesDlg;
    QString separatorStr;
    OverridenAB * overridenAB;
    QPopupMenu * phonePopupMenu;
    KProgress * kpw;
    bool setFmtNameInKab;
    KABC::Addressee ** phoneAddressees;
    QString addrFmtMismatch;
    QString nameFmtMismatch;
    QString preferredPhoneOrder;
    bool phoneConnected;
    bool startupReadPhone;
    bool startupReadKab;
    bool startupConnectToPhone;
    bool transferAllKab2Phone;
    bool transferAllPhone2Kab;
    QString phoneEncoding;
    QString nameFormat;
    QString addressFormat;
    QString memType;
    QString IMEI;
    KABC::Addressee ** KABAddressees;
    AddresseeDlg * addresseeDlg;
    QString dontDisplayProblematic;
    QStringList groupNames;
    QListViewItem * globalItem;
    QString W1Action;
    QString W2Action;
    QString W3Action;
    QPtrList<KABC::Addressee> AskList;
    QString W3PAction;

    QPixmap image0;
    QPixmap image1;

    void init();

};

#endif // SYNCVIEW_H
