/****************************************************************************
** Form interface generated from reading ui file 'addresseedlg.ui'
**
** Created: Σαβ Αύγ 16 18:20:32 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef ADDRESSEEDLG_H
#define ADDRESSEEDLG_H

#include <qvariant.h>
#include <qpixmap.h>
#include <kabc/addressee.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QComboBox;
class QFrame;
class QLabel;
class QLineEdit;
class QPushButton;
class QRadioButton;
class QTabWidget;
class QWidget;

class AddresseeDlg : public QDialog
{
    Q_OBJECT

public:
    AddresseeDlg( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~AddresseeDlg();

    QTabWidget* tabWidget2;
    QWidget* tab;
    QLineEdit* nameInPhoneLE;
    QLineEdit* nameLE;
    QLineEdit* surnameLE;
    QLabel* textLabel1_3;
    QLabel* textLabel2;
    QLabel* textLabel1;
    QLabel* textLabel1_4;
    QLineEdit* addNameLE;
    QLabel* textLabel2_3;
    QLineEdit* frmtdNameLE;
    QLabel* textLabel2_3_2;
    QComboBox* groupsCB;
    QWidget* tab_2;
    QFrame* frame3_2;
    QLineEdit* phoneLE2;
    QComboBox* phoneTypeCB2;
    QRadioButton* prefPhoneRB2;
    QFrame* frame3_4;
    QComboBox* phoneTypeCB3;
    QLineEdit* phoneLE3;
    QRadioButton* prefPhoneRB3;
    QFrame* frame3_4_2;
    QLineEdit* phoneLE4;
    QComboBox* phoneTypeCB4;
    QRadioButton* prefPhoneRB4;
    QFrame* frame3_4_2_2;
    QLineEdit* phoneLE5;
    QComboBox* phoneTypeCB5;
    QRadioButton* prefPhoneRB5;
    QFrame* frame3;
    QLineEdit* phoneLE1;
    QComboBox* phoneTypeCB1;
    QRadioButton* prefPhoneRB1;
    QWidget* tab_3;
    QFrame* frame8;
    QLabel* textLabel3;
    QLineEdit* emailLE1;
    QFrame* frame8_2;
    QLabel* textLabel3_2;
    QLineEdit* emailLE2;
    QFrame* frame8_3;
    QLabel* textLabel3_3;
    QLineEdit* emailLE3;
    QFrame* frame8_4;
    QLabel* textLabel3_4;
    QLineEdit* emailLE4;
    QWidget* tab_4;
    QFrame* frame8_5;
    QLabel* textLabel3_5;
    QLineEdit* urlLE1;
    QFrame* frame8_6;
    QLabel* textLabel3_6;
    QLineEdit* urlLE2;
    QFrame* frame8_7;
    QLabel* textLabel3_7;
    QLineEdit* urlLE3;
    QFrame* frame8_8;
    QLabel* textLabel3_8;
    QLineEdit* urlLE4;
    QWidget* tab_5;
    QTabWidget* tabWidget3;
    QWidget* tab_6;
    QLabel* textLabel1_2;
    QLabel* textLabel1_2_2;
    QLabel* textLabel1_2_2_2;
    QLabel* textLabel1_2_2_3;
    QLabel* textLabel1_2_2_4;
    QLineEdit* streetLE1;
    QLineEdit* localityLE1;
    QLineEdit* regionLE1;
    QLineEdit* postalCodeLE1;
    QLineEdit* countryLE1;
    QWidget* tab_7;
    QLabel* textLabel1_2_2_4_2;
    QLabel* textLabel1_2_2_2_2;
    QLabel* textLabel1_2_2_5;
    QLabel* textLabel1_2_2_3_2;
    QLabel* textLabel1_2_3;
    QLineEdit* streetLE2;
    QLineEdit* localityLE2;
    QLineEdit* regionLE2;
    QLineEdit* postalCodeLE2;
    QLineEdit* countryLE2;
    QWidget* tab_8;
    QLabel* textLabel1_2_2_2_3;
    QLabel* textLabel1_2_2_4_3;
    QLabel* textLabel1_2_2_6;
    QLabel* textLabel1_2_4;
    QLabel* textLabel1_2_2_3_3;
    QLineEdit* streetLE3;
    QLineEdit* localityLE3;
    QLineEdit* regionLE3;
    QLineEdit* postalCodeLE3;
    QLineEdit* countryLE3;
    QWidget* tab_9;
    QLabel* textLabel1_2_2_2_4;
    QLabel* textLabel1_2_5;
    QLabel* textLabel1_2_2_3_4;
    QLabel* textLabel1_2_2_4_4;
    QLabel* textLabel1_2_2_7;
    QLineEdit* streetLE4;
    QLineEdit* localityLE4;
    QLineEdit* regionLE4;
    QLineEdit* postalCodeLE4;
    QLineEdit* countryLE4;
    QWidget* tab_10;
    QLabel* charsLabel2;
    QLabel* textLabel2_2_2;
    QLineEdit* noteLE2;
    QLabel* charsLabel1;
    QLineEdit* noteLE1;
    QLabel* textLabel2_2;
    QLineEdit* noteLE3;
    QLabel* charsLabel3;
    QLabel* textLabel2_2_3;
    QLabel* charsLabel4;
    QLabel* textLabel2_2_4;
    QLineEdit* noteLE4;
    QPushButton* OKButton;
    QPushButton* cancelButton;
    QPushButton* clrOvrDataButton;

    void init();

public slots:
    void OKClicked();
    virtual void cancelClicked();
    void setAddressee(KABC::Addressee * addressee);
    KABC::Addressee * getAddressee();
    void displayAddressee();
    void clrOvrDataClicked();

signals:
    void OKButtonClicked();
    void clrOvrButtonClicked();

protected:
    QGridLayout* AddresseeDlgLayout;
    QGridLayout* tabLayout;
    QGridLayout* tabLayout_2;
    QGridLayout* tabLayout_3;
    QGridLayout* tabLayout_4;
    QGridLayout* tabLayout_5;
    QGridLayout* tabLayout_6;
    QGridLayout* tabLayout_7;
    QGridLayout* tabLayout_8;
    QGridLayout* tabLayout_9;
    QGridLayout* tabLayout_10;
    QGridLayout* layout5;
    QGridLayout* layout4;
    QGridLayout* layout6;
    QGridLayout* layout7;
    QHBoxLayout* layout2;

protected slots:
    virtual void languageChange();
private:
    KABC::Addressee * mAddressee;
    QPtrList<QLineEdit> countryQLEList;
    QPtrList<QLineEdit> postalCodeQLEList;
    QPtrList<QLineEdit> regionQLEList;
    QPtrList<QLineEdit> localityQLEList;
    QPtrList<QLineEdit>streetQLEList;
    QPtrList<QRadioButton> prefPhoneQRBList;
    QPtrList<QComboBox> phoneQCBList;
    QPtrList<QLineEdit> phoneQLEList;
    QPtrList<QLineEdit> emailQLEList;
    QPtrList<QLineEdit> urlQLEList;
    QPtrList<QLineEdit>notesQLEList;

    QPixmap image0;
    QPixmap image1;
    QPixmap image2;
    QPixmap image3;
    QPixmap image4;

};

#endif // ADDRESSEEDLG_H
