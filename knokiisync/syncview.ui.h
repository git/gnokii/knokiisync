/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <kconfig.h>
#include <kapp.h>
#include <kabc/stdaddressbook.h>
#include <kabc/phonenumber.h>
#include <kabc/vcardformat.h>
#include <kabc/addressee.h>
#include <kstandarddirs.h>
#include <kabc/address.h>
#include <kmessagebox.h>
#include <kprogress.h>
#include <klocale.h>
#include <kdebug.h>

#include <qtextcodec.h>
#include <qtimer.h>
#include <qinputdialog.h>

#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "namesdlg.h"

#define KAB_POS_COL 0
#define KAB_SURN_COL 1
#define KAB_NAME_COL 2
#define KAB_PREF_COL 3
#define KAB_CELL_COL 4
#define KAB_GEN_COL 5
#define KAB_HOME_COL 6
#define KAB_WORK_COL 7
#define KAB_FAX_COL 8
#define KAB_EMAIL_COL 9
#define KAB_URL_COL 10
#define KAB_ADDR_COL 11
#define KAB_LOCL_COL 12
#define KAB_REGN_COL 13
#define KAB_POST_COL 14
#define KAB_CNTR_COL 15
#define KAB_NOTE_COL 16
#define KAB_GRP_COL 17

#define MOB_POS_COL 0
#define MOB_SURN_COL 1
#define MOB_NAME_COL 2
#define MOB_PREF_COL 3
#define MOB_CELL_COL 4
#define MOB_GEN_COL 5
#define MOB_HOME_COL 6
#define MOB_WORK_COL 7
#define MOB_FAX_COL 8
#define MOB_EMAIL_COL 9
#define MOB_URL_COL 10
#define MOB_ADDR_COL 11
#define MOB_LOCL_COL 12
#define MOB_REGN_COL 13
#define MOB_POST_COL 14
#define MOB_CNTR_COL 15
#define MOB_NOTE_COL 16
#define MOB_GRP_COL 17

extern KApplication *globalKApp;

typedef QValueListIterator<KABC::PhoneNumber> PhoneIterator;
typedef QValueListIterator<KABC::Address> AddressIterator;
static char *lockfile = NULL;
static struct gn_statemachine state;
static gn_data data;
int addr_phone = 0;
int lastUsed = 0;

QString
loc_enc2utf8 (QCString locallyEncoded, QString phoneEncoding)
{
	QTextCodec *codec = QTextCodec::codecForName (phoneEncoding);
	QString unicodeString = codec->toUnicode (locallyEncoded);
	return unicodeString;
}

QCString
utf82loc_enc (QString unicodeString, QString phoneEncoding)
{
	QTextCodec *codec = QTextCodec::codecForName (phoneEncoding);
	QCString locallyEncoded = codec->fromUnicode (unicodeString);
	return locallyEncoded;
}

void
SyncView::init ()
{
	phonePopupMenu = 0;
	phoneAddressees = 0;
	KABAddressees = 0;
	addresseeDlg = 0;
	phoneConnected = 0;
	overridenAB = 0;
	globalItem = 0;
	namesDlg = 0;
	kpw = 0;

	slotParseConfig();

	KABListView->addColumn (i18n ("Position"));
	KABListView->addColumn (KABC::Addressee::familyNameLabel ());
	KABListView->addColumn (KABC::Addressee::givenNameLabel ());
	KABListView->addColumn (i18n ("Preferred Number"));
	KABListView->addColumn (KABC::Addressee::mobilePhoneLabel ());
	KABListView->addColumn (i18n ("General Number"));
	KABListView->addColumn (KABC::Addressee::homePhoneLabel ());
	KABListView->addColumn (KABC::Addressee::businessPhoneLabel ());
	KABListView->addColumn (i18n ("Fax"));
	KABListView->addColumn (KABC::Addressee::emailLabel ());
	KABListView->addColumn (KABC::Addressee::urlLabel ());
	KABListView->addColumn (i18n ("Address"));
	KABListView->addColumn (i18n ("Locality"));
	KABListView->addColumn (i18n ("Region"));
	KABListView->addColumn (i18n ("Postal Code"));
	KABListView->addColumn (i18n ("Country"));
	KABListView->addColumn (KABC::Addressee::noteLabel ());
	KABListView->addColumn (i18n ("Group"));
	KABListView->setSelectionModeExt (KListView::FileManager);

	mobListView->addColumn (i18n ("Position"));
	mobListView->addColumn (KABC::Addressee::familyNameLabel ());
	mobListView->addColumn (KABC::Addressee::givenNameLabel ());
	mobListView->addColumn (i18n ("Preferred Number"));
	mobListView->addColumn (KABC::Addressee::mobilePhoneLabel ());
	mobListView->addColumn (i18n ("General Number"));
	mobListView->addColumn (KABC::Addressee::homePhoneLabel ());
	mobListView->addColumn (KABC::Addressee::businessPhoneLabel ());
	mobListView->addColumn (i18n ("Fax"));
	mobListView->addColumn (KABC::Addressee::emailLabel ());
	mobListView->addColumn (KABC::Addressee::urlLabel ());
	mobListView->addColumn (i18n ("Address"));
	mobListView->addColumn (i18n ("Locality"));
	mobListView->addColumn (i18n ("Region"));
	mobListView->addColumn (i18n ("Postal Code"));
	mobListView->addColumn (i18n ("Country"));
	mobListView->addColumn (KABC::Addressee::noteLabel ());
	mobListView->addColumn (i18n ("Group"));

	connect ( mobListView, SIGNAL (doubleClicked ( QListViewItem *, const QPoint &, int )), this, SLOT (itemDoubleClicked (QListViewItem *, const QPoint &, int)));
	mobListView->setSelectionModeExt (KListView::FileManager);

	/* For GNU gettext */
#ifdef ENABLE_NLS
	textdomain ("gnokii");
	setlocale (LC_ALL, "");
#endif

	if (gn_cfg_read_default () < 0)
		exit (1);

	if (!gn_cfg_phone_load ("", &state))
		exit (1);

	phonePopupMenu = new QPopupMenu;
	phonePopupMenu->insertItem ("Transfer to KAB", this, SLOT (xferGlobalItem2KAB ()));
	phonePopupMenu->insertItem ("Update in phone", this, SLOT (updateGlobalItemInPhone()));
	phonePopupMenu->insertItem ("Contact Details", this, SLOT (showAddressee ()));

	if (startupConnectToPhone)
		busInit ();

	if (startupReadKab)
		QTimer::singleShot (100, this, SLOT (readKAB ()));

	if (startupReadPhone)
		QTimer::singleShot (100, this, SLOT (readMobPhBook ()));
}

void
SyncView::readKAB ()
{
	int i = 0, addr_kab = 0;
	int KABEntries = 0;
	KABC::AddressBook * addressBook = KABC::StdAddressBook::self ();
	KABC::AddressBook::Iterator it;
	QString location;

	for (it = addressBook->begin (); it != addressBook->end (); ++it)
		KABEntries++;

	if (KABAddressees)
		{
			while (KABAddressees[i] != 0)
				delete KABAddressees[i++];
			delete KABAddressees;
		}

	KABAddressees = new KABC::Addressee * [KABEntries+1];

	for (i = 0; i < KABEntries; i++)
		KABAddressees[i] = new KABC::Addressee;

	KABAddressees[KABEntries] = 0;

	KProgressDialog * kpdlg = new KProgressDialog(this, "kpdlg", i18n ("Getting KAB Entries"), i18n ("Please wait while getting all the KAB entries."), FALSE);
	kpdlg->showCancelButton (FALSE);
	kpdlg->setAllowCancel (FALSE);
	kpdlg->setAutoClose (TRUE);
	kpdlg->setMinimumDuration(0);

	kpw = kpdlg->progressBar ();
	kpw->setTotalSteps (KABEntries);
	kpw->setValue(0);

	kpdlg->show();

	for (it = addressBook->begin (); it != addressBook->end (); ++it)
		{
			location = QString::number (addr_kab);

			if (addr_kab < 10)
				location = "00" + location;
			else if (addr_kab < 100)
				location = "0" + location;

			KABAddressees[addr_kab]->insertCustom ("KnokiiSync", "Position", location);

			QString familyName = (*it).familyName ();
			KABAddressees[addr_kab]->setFamilyName (familyName);

			QString givenName = (*it).givenName ();
			KABAddressees[addr_kab]->setGivenName (givenName);

			QString additionalName = (*it).additionalName ();
			KABAddressees[addr_kab]->setAdditionalName (additionalName);

			QString formattedName = (*it).formattedName ();
			KABAddressees[addr_kab]->setFormattedName (formattedName);

			QString groupId = (*it).custom ("KnokiiSync", "GROUP_ID");
			KABAddressees[addr_kab]->insertCustom ("KnokiiSync", "GROUP_ID", groupId);

			kdDebug () << familyName << ", " << givenName << endl;

			KABC::PhoneNumber::List phoneNumbers = (*it).phoneNumbers ();
			for (PhoneIterator pI = phoneNumbers.begin ();
					pI != phoneNumbers.end ();
					++pI)
				{
				KABAddressees[addr_kab]->insertPhoneNumber (*pI);
				kdDebug () << (*pI).number() << ": " << (*pI).id() << endl;
				}

			QStringList emails = (*it).emails ();
			for (QStringList::Iterator emI = emails.begin ();
					emI != emails.end (); ++emI)
				KABAddressees[addr_kab]->insertEmail (*emI);

			KABC::Address::List addresses = (*it).addresses ();
			for (AddressIterator aI = addresses.begin ();
					aI != addresses.end ();
					++aI)
				{
				KABAddressees[addr_kab]->insertAddress (*aI);
				kdDebug () << (*aI).street() << ": " << (*aI).id() << endl;
				}

			kdDebug () << "***********************************" << endl;

			QString tmpName;
			int tmpCounter = 1;
			QString url;
			KURL _url = (*it).url ();
			if (_url.prettyURL () != "")
				{
					KABAddressees[addr_kab]->setUrl (_url);
					do
						{
							tmpName = "E_URL" + QString::number (tmpCounter);
							url = (*it).custom ("KnokiiSync", tmpName);
							if (url != "")
								{
								KABAddressees[addr_kab]->insertCustom ("KnokiiSync", tmpName, url);
								}
							tmpCounter++;
						}
					while (url != "");
				}

			tmpCounter = 1;
			QString note = (*it).note ();
			if (note != "")
				{
					KABAddressees[addr_kab]->setNote (note);
					do
						{
							tmpName = "E_NOTE" + QString::number (tmpCounter);
							note = (*it).custom ("KnokiiSync", tmpName);
							if (note != "")
								{
								KABAddressees[addr_kab]->insertCustom ("KnokiiSync", tmpName, note);
								}
							tmpCounter++;
						}
					while (note != "");
				}

			kpw->advance (1);
			globalKApp->processEvents();
			addr_kab++;
		}

	kpdlg->hide();
	delete kpdlg;
	kpw = 0;
	displayKAB ();
}

void
SyncView::readMobPhBook ()
{
	gn_error error;
	gn_phonebook_entry entry;

	if (!startupConnectToPhone)
		busInit ();

	if(!phoneConnected)
		return;

	gn_data_clear (&data);
	char c_imei[64], c_model[64], c_rev[64], c_manufacturer[64];
	data.manufacturer = c_manufacturer;
	data.model = c_model;
	data.revision = c_rev;
	data.imei = c_imei;
	strcpy (c_imei, "(unknown)");
	error = gn_sm_functions (GN_OP_Identify, &data, &state);
	if (error != GN_ERR_NONE)
		{
			int reply = KMessageBox::questionYesNo (0, i18n ("Couldn't identify the phone. Should I continue with reduced functionality?"));
			if (reply == KMessageBox::No)
				{
					busTerminate ();
					return;
				}
			else
				IMEI = "no_imei";
		}
	else
		IMEI = c_imei;

	if (overridenAB)
		delete overridenAB;

	overridenAB = new OverridenAB (IMEI);
	if (!overridenAB->load())
		{
			kdDebug () <<  i18n ("I can't load the modified phone book. No need to panic though.") << endl;;
		}

	int used = 0;
	char memory_type_string[3];
	gn_data_clear (&data);
	gn_memory_status phonememorystatus;
	phonememorystatus.used = 0;
	phonememorystatus.free = 0;
	if (memType == i18n("Phone + SIM"))
		{
			sprintf(memory_type_string, "MT");
			phonememorystatus.memory_type = GN_MT_MT;
		}
	else
		if (memType == i18n("Phone"))
			{
				sprintf(memory_type_string, "ME");
				phonememorystatus.memory_type = GN_MT_ME;
			}
		else
			if (memType == i18n("SIM"))
				{
					sprintf(memory_type_string, "SM");
					phonememorystatus.memory_type = GN_MT_SM;
				}
	entry.memory_type = gn_str2memory_type (memory_type_string);
	data.memory_status = &phonememorystatus;
	if (gn_sm_functions (GN_OP_GetMemoryStatus, &data, &state) == GN_ERR_NONE)
		used = phonememorystatus.used;
	else
		{
			KMessageBox::sorry (0, i18n ("Can't get memory status. Sorry. Not my fault. Really."));
			if (!startupConnectToPhone)
				busTerminate ();
			return;
		}

	if (used == 0)
		{
			KMessageBox::sorry (0, i18n ("The selected memory (%1) is empty!").arg(memType));
			if (!startupConnectToPhone)
				busTerminate ();
			return;
		}

	int i = 0;
	if (phoneAddressees)
		{
			while (phoneAddressees[i] != 0)
				delete phoneAddressees[i++];
			delete phoneAddressees;
		}

	AskList.clear ();

	i = used;
	lastUsed = 1;

	KProgressDialog * kpdlg = new KProgressDialog(this, "kpdlg", i18n ("Getting phone entries"), i18n ("Please wait while getting all the entries from the mobile phone."), FALSE);
	kpdlg->showCancelButton (FALSE);
	kpdlg->setAllowCancel (FALSE);
	kpdlg->setAutoClose (TRUE);
	kpdlg->setMinimumDuration (0);

	kpw = kpdlg->progressBar ();
	kpw->setTotalSteps (1);
	kpw->setValue (0);

	globalKApp->processEvents();

	kpdlg->show();

	globalKApp->processEvents();

	while (used > 0)
		{
			globalKApp->processEvents();
			gn_data_clear (&data);
			entry.memory_type = gn_str2memory_type (memory_type_string);
			entry.location = lastUsed;

			data.phonebook_entry = &entry;
			error = gn_sm_functions (GN_OP_ReadPhonebook, &data, &state);

			if (error == GN_ERR_NONE)
				used--;
			lastUsed++;
		}

	used = i;

	phoneAddressees = new KABC::Addressee * [lastUsed];
	for (i = 0; i < lastUsed; i++)
		phoneAddressees[i] = new KABC::Addressee;

	phoneAddressees[lastUsed-1] = 0;

	int lcount = 1;

	gn_data_clear (&data);
	entry.memory_type = gn_str2memory_type (memory_type_string);
	lcount = 1;

	kpw->setTotalSteps (lastUsed-1);
	kpw->setValue (0);

	kpdlg->show();

	addr_phone = 0;
	KABC::PhoneNumber phoneNumber;
	KABC::Address kAddress;

	for (addr_phone = 0; addr_phone < lastUsed-1; addr_phone++)
		{
			QString location, name, familyName, givenName;
			givenName = "";
			familyName = "";
			entry.location = lcount;

			globalKApp->processEvents();

			data.phonebook_entry = &entry;
			error = gn_sm_functions (GN_OP_ReadPhonebook, &data, &state);

			if (error == GN_ERR_NONE)
				{
					location = QString::number (entry.location);

					if (entry.location < 10)
						location = "00" + location;
					else if (entry.location < 100)
						location = "0" + location;

					phoneAddressees[addr_phone]->insertCustom ("KnokiiSync", "GROUP_ID", QString::number (entry.caller_group));

					name = loc_enc2utf8 (entry.name, phoneEncoding);

					phoneAddressees[addr_phone]->insertCustom ("KnokiiSync", "MOB_POS", location);
					phoneAddressees[addr_phone]->setUid ("KNSYNC_"+location);
					phoneAddressees[addr_phone]->insertCustom ("KnokiiSync", "E_NAMEINPHONE", name);

					if (!isNameOverriden (phoneAddressees[addr_phone]))
						{
							splitName (phoneAddressees[addr_phone], name);
							handleCodes (phoneAddressees[addr_phone]);
						}
					else
						{
							loadOverridenName (phoneAddressees[addr_phone], name);
						}

					setFormattedName (phoneAddressees[addr_phone]);

					int phType = 0, tmpCounter = 1, addressCount = 0;
					QString utf8_data, tmpName, note;
					KURL kurl;
					for (int i = 0; i < entry.subentries_count; i++)
						{
							utf8_data = loc_enc2utf8 (entry.subentries[i].data.number, phoneEncoding);
							switch (entry.subentries[i].entry_type)
								{
									case 0x0b:
										phType = 0;
										switch (entry.subentries[i].number_type)
											{
												case 0x02:	phType = KABC::PhoneNumber::Home;
																		break;
												case 0x03:	phType = KABC::PhoneNumber::Cell;
																		break;
								  			case 0x04:	phType = KABC::PhoneNumber::Fax;
																		break;
												case 0x06:	phType = KABC::PhoneNumber::Work;
																		break;
												case 0x0a:	phType = KABC::PhoneNumber::Voice;
																		break;
											}
										phoneNumber.setNumber (utf8_data);
										if (entry.number == phoneNumber.number ())
											phType += KABC::PhoneNumber::Pref;
										phoneNumber.setType (phType);
										phoneNumber.setId ("KNSYNC_"+location+utf8_data);
										phoneAddressees[addr_phone]->insertPhoneNumber (phoneNumber);
										break;
									case 0x08:	phoneAddressees[addr_phone]->insertEmail (utf8_data);
															break;
									case 0x2c:	kurl = phoneAddressees[addr_phone]->url ();
															if (kurl.prettyURL () == "")
																{
																	phoneAddressees[addr_phone]->setUrl (KURL (utf8_data));
																}
															else
																{
																	tmpName = "E_URL" + QString::number (tmpCounter);
																	phoneAddressees[addr_phone]->insertCustom ("KnokiiSync", tmpName, utf8_data);
																	tmpCounter++;
																}
															break;
									case 0x09:	break;
									case 0x0a:	note = phoneAddressees[addr_phone]->note ();
															if (note == "")
																{
																	phoneAddressees[addr_phone]->setNote (utf8_data);
																}
															else
																{
																	tmpName = "E_NOTE" + QString::number (tmpCounter);
																	phoneAddressees[addr_phone]->insertCustom ("KnokiiSync", tmpName, utf8_data);
																	tmpCounter++;
																}
															break;
									default:		phoneAddressees[addr_phone]->setNote (utf8_data); // Unknown entry, put it in the notes
															break;
								}

							if (entry.subentries[i].entry_type == 0x09 && utf8_data != "") // if this is an address...
								{
									addressCount++;
									kAddress.clear();
									QString address = utf8_data;
									QStringList addressFields = QStringList::split (", ", address);
									QStringList addrFormat = QStringList::split (", ", addressFormat);

									if (addressFields.count () == addrFormat.count ())
										{
											QStringList::Iterator it1 = addrFormat.begin ();
											QStringList::Iterator it2 = addressFields.begin ();
											for ( ; it1 != addrFormat.end (); ++it1, ++it2 )
												{
													QChar c = (*it1).at (1);
													switch (c)
														{
															case 'S': kAddress.setStreet(*it2);
																				break;
															case 'L': kAddress.setLocality(*it2);
																				break;
															case 'R': kAddress.setRegion(*it2);
																				break;
															case 'P': kAddress.setPostalCode(*it2);
																		break;
															case 'C': kAddress.setCountry(*it2);
																		break;
															default:  break;
														}
												}
										}
									else
										{
											if (addrFmtMismatch == "Notify")
												{
													QStringList::Iterator it2 = addressFields.begin ();

													if (it2 != addressFields.end ())
													kAddress.setStreet (*it2++);
													if (it2 != addressFields.end ())
													kAddress.setLocality (*it2++);
													if (it2 != addressFields.end ())
													kAddress.setRegion (*it2++);
													if (it2 != addressFields.end ())
													kAddress.setPostalCode (*it2++);
													if (it2 != addressFields.end ())
													kAddress.setCountry (*it2);

													KConfig *knsync_cfg = globalKApp->config ();

													if (knsync_cfg->hasGroup ("Entry_"+location+"_"+IMEI))
														{
															knsync_cfg->setGroup ("Entry_"+location+"_"+IMEI);
															if (knsync_cfg->readEntry ("Address", "") != "")
																kAddress.setStreet (knsync_cfg->readEntry ("Address"));
															if (knsync_cfg->readEntry ("Locality", "") != "")
																kAddress.setLocality (knsync_cfg->readEntry ("Locality"));
															if (knsync_cfg->readEntry ("Region", "") != "")
																kAddress.setRegion (knsync_cfg->readEntry ("Region"));
															if (knsync_cfg->readEntry ("PostalCode", "") != "")
																kAddress.setPostalCode (knsync_cfg->readEntry ("PostalCode"));
															if (knsync_cfg->readEntry ("Country", "") != "")
																kAddress.setCountry (knsync_cfg->readEntry ("Country"));

															if (kAddress.street () == "KNOKIISYNC_IGNORE");
																kAddress.setStreet ("");
															if (kAddress.locality () == "KNOKIISYNC_IGNORE");
																kAddress.setLocality ("");
															if (kAddress.region () == "KNOKIISYNC_IGNORE");
																kAddress.setRegion ("");
															if (kAddress.postalCode () == "KNOKIISYNC_IGNORE");
																kAddress.setPostalCode ("");
															if (kAddress.country () == "KNOKIISYNC_IGNORE");
																kAddress.setCountry ("");
														}
													else
														KMessageBox::sorry (0, i18n ("The phone entry \"%1\" does not have the postal address written as set in the configuration dialog. Assuming defaults. ").arg(name));
												}
											else
												{
													if (addrFmtMismatch == "Ignore")
														{
															kAddress.setStreet ("");
															kAddress.setLocality ("");
															kAddress.setRegion ("");
															kAddress.setPostalCode ("");
															kAddress.setCountry ("");
														}
												}
 										}
								kAddress.setId ("KNSYNC_"+location+"_"+QString::number(addressCount));
								kdDebug () << "Address ID: " << kAddress.id () << endl;
								phoneAddressees[addr_phone]->insertAddress (kAddress);
								}
						}
				}
			else
				{
					phoneAddressees[addr_phone]->insertCustom ("KnokiiSync", "MOB_POS", "***");
					phoneAddressees[addr_phone]->setFamilyName (i18n ("Error in entry %1").arg(QString::number(lcount)));
					phoneAddressees[addr_phone]->setGivenName (QString (gn_error_print (error)));
				}
			lcount++;
			kpw->advance(1);
		}

	if (!startupConnectToPhone)
		busTerminate ();

	delete kpdlg;
	kpw = 0;

	askNames ();
}

void
SyncView::sync ()
{
KMessageBox::sorry(0, i18n ("Sorry, the 1-Click Sync(TM) functionality is not yet implemented. Please manually transfer any entries you wish to."));
//syncLeft();
//syncRight();
}

void
SyncView::syncLeft ()
{
	QPtrList < QListViewItem > selectedEntries;
	selectedEntries = mobListView->selectedItems ();

	if (selectedEntries.count () == 0 && !transferAllPhone2Kab)
		{
			KMessageBox::information (0, i18n ("You need to select some entries first."));
			return;
		}
	else if (selectedEntries.count () == 0 && transferAllPhone2Kab)
		{
			mobListView->selectAll (TRUE);
			selectedEntries = mobListView->selectedItems ();
		}

	QPtrListIterator < QListViewItem > it (selectedEntries);
	QListViewItem *entry;
	while ((entry = it.current ()) != 0)
		{
			++it;
			writeToKAB (entry);
		}
	readKAB ();
}

void
SyncView::syncRight ()
{
	QPtrList < QListViewItem > selectedEntries;
	selectedEntries = KABListView->selectedItems ();

	if (selectedEntries.count () == 0 && !transferAllKab2Phone)
		{
			KMessageBox::information (0, i18n ("You need to select some entries first."));
			return;
		}
	else if (selectedEntries.count () == 0 && transferAllKab2Phone)
		{
			KABListView->selectAll (TRUE);
			selectedEntries = KABListView->selectedItems ();
		}

	QPtrListIterator < QListViewItem > it (selectedEntries);
	QListViewItem *entry;

	if (!startupConnectToPhone)
		busInit ();

	while ((entry = it.current ()) != 0)
		{
			++it;
			writeToPhone (entry);
		}

	readMobPhBook ();

	if (!startupConnectToPhone)
		busTerminate ();
}

void
SyncView::writeToKAB (QListViewItem * phoneEntry)
{
	KABC::Ticket * goldenTicket = 0;
	KABC::AddressBook * addressBook = KABC::StdAddressBook::self ();
	KABC::Addressee * addressee = 0;
	QString tmp;
	QString location;
	QString uid;
	int found = 0;
	int i = 0;
	QString formattedName;

	location = phoneEntry->text (MOB_POS_COL);

	if (location == "***")
		return;

		uid = "KNSYNC_" + location;

	for (i = 0; phoneAddressees[i] != 0; i++)
		{
			if (phoneAddressees[i]->uid() == uid)
				{
					addressee = phoneAddressees[i];
					break;
				}
		}

	if (!addressee)
		{
			KMessageBox::sorry (0, "writeToKAB: Internal error. Please contact the author.");
			return;
		}

	KABC::AddressBook::Iterator it;
	for (it = addressBook->begin (); it != addressBook->end (); ++it)
		{
			if ((*it).uid () == uid)
				{
					if ((*it).familyName () == addressee->familyName () && (*it).givenName () == addressee->givenName ())
						{
							found = 1;
							break;
						}
					else
						{
							int reply = KMessageBox::questionYesNoCancel (0, i18n ("A possible match was found (Surname: %1, Name: %2), however it has a different name name than the requested entry (Surname: %3, Name: %4). Do you want to replace this entry, continue searching, or cancel?").arg (phoneAddressees[i]->familyName ()).arg (phoneAddressees[i]->givenName ()).arg ( addressee->familyName ()).arg (addressee->givenName ()), i18n ("Question"), KGuiItem(i18n ("Replace")), KGuiItem(i18n ("Continue")));
							if (reply == KMessageBox::Yes)
								{
									found = 1;
									break;
								}
							else
								if (reply == KMessageBox::No)
									{
									}
								else
									if (reply == KMessageBox::Cancel)
										{
											return;
										}
						}
				}
		}

	if (!found)
		{
			for (it = addressBook->begin (); it != addressBook->end (); ++it)
				{
					if ((*it).familyName () == addressee->familyName () && (*it).givenName () == addressee->givenName ())
						{
							int reply = KMessageBox::questionYesNoCancel (0, i18n ("A possible match was found, with the same surname and name as the entry being transfered (Surname: %1, Name: %2). Do you want to replace this entry, continue searching, or cancel?").arg (phoneAddressees[i]->familyName ()).arg (phoneAddressees[i]->givenName ()), i18n ("Question"), KGuiItem(i18n ("Replace")), KGuiItem(i18n ("Continue")));
							if (reply == KMessageBox::Yes)
								{
									addressBook->removeAddressee (it);
									found = 1;
									break;
								}
							else
								if (reply == KMessageBox::No)
									{
									}
								else
									if (reply == KMessageBox::Cancel)
										{
											return;
										}
						}
				}
		}

			if (!found)
				{
					int reply = KMessageBox::questionYesNo (0, i18n ("A match for this contact (Surname: %1, Name: %2) was not found. Do you want to add this entry to the address book?").arg (addressee->familyName ()).arg (addressee->givenName ()));
					if (reply == KMessageBox::No)
						{
							return;
						}
				}

			if (!setFmtNameInKab)
				{
					KABC::Addressee modAddressee = *addressee;
					modAddressee.setFormattedName ("");
					addressBook->insertAddressee (modAddressee);
					
				}
			else
				addressBook->insertAddressee (*addressee);

			goldenTicket = addressBook->requestSaveTicket ();

			if (goldenTicket)
				addressBook->save (goldenTicket);
			else
				{
					KMessageBox::sorry (0, "The address book didn't return a save ticket. I can't save the address book!");
					return;
				}
}

void
SyncView::writeToPhone (QListViewItem * KABEntry)
{
	int i = 0;
	int location = 1;
	gn_error error;
	gn_phonebook_entry entry;
	gn_phonebook_entry aux_entry;
	KABC::Addressee * KABAddressee = 0;
	KABC::Addressee * phoneAddressee = 0;

	for (i = 0; KABAddressees[i] != 0; i++)
		{
			if (KABEntry->text (KAB_POS_COL) == KABAddressees[i]->custom ("KnokiiSync", "Position"))
				{
					KABAddressee = KABAddressees[i];
					break;
				}
		}

	for (i = 0; phoneAddressees[i] != 0; i++)
		{
			if(KABAddressee->uid () == phoneAddressees[i]->uid ())
				{
					if (KABAddressee->familyName () == phoneAddressees[i]->familyName () &&
					KABAddressee->givenName () == phoneAddressees[i]->givenName ())
						{
							phoneAddressee = phoneAddressees[i];
						}
				}
		}

	if (!phoneAddressee)
		{
			for (i = 0; phoneAddressees[i] != 0; i++)
				{
					if (KABAddressee->familyName () == phoneAddressees[i]->familyName () &&
					KABAddressee->givenName () == phoneAddressees[i]->givenName ())
						{
							phoneAddressee = phoneAddressees[i];
						}
				}
		}

	if (!phoneAddressee)
		{
			int reply = KMessageBox::questionYesNo (0, "A match for this contact (Surname: " + KABEntry->text (KAB_SURN_COL)  + ", Name: " + KABEntry->text (KAB_NAME_COL) + ") was not found. Do you want to add this entry to the phone's address book?");
			if (reply == KMessageBox::No)
				{
					return;
				}

			while (1)
				{
					gn_data_clear (&data);
					entry = aux_entry;
					data.phonebook_entry = &entry;
					entry.location = location;

					if (memType == i18n("Phone + SIM"))
						entry.memory_type=GN_MT_MT;
					else
						if (memType == i18n("Phone"))
							entry.memory_type=GN_MT_ME;
						else
							if (memType == i18n("SIM"))
								entry.memory_type=GN_MT_SM;

					error = gn_sm_functions (GN_OP_ReadPhonebook, &data, &state);
					if (error == GN_ERR_EMPTYLOCATION)
						break;
					else if (error == GN_ERR_NONE)
						location++;
					else
						{
							KMessageBox::sorry (0, i18n ("Can't find a free slot on the phone %1").arg (QString (gn_error_print (error))));
							return;
						}
				}
		}
	else
		{
			QString tmp = phoneAddressee->custom ("KnokiiSync", "MOB_POS");
			location = tmp.toInt ();
		}

	phoneAddressee = KABAddressee;
	uploadContactToPhone (phoneAddressee, location);
}

void
SyncView::busTerminate ()
{
	if (phoneConnected)
		{
			emit disconnecting ();
			gn_sm_functions (GN_OP_Terminate, NULL, &state);
			if (lockfile)
				gn_device_unlock (lockfile);
			phoneConnected = FALSE;
			emit disconnected ();
		}
}

void
SyncView::busInit ()
{
	gn_error error;
	char *aux;

	if (phoneConnected)
		return;

	emit connecting ();
	globalKApp->processEvents();
	gn_data_clear (&data);
	aux = gn_cfg_get (gn_cfg_info, "global", "use_locking");

	if (aux && !strcmp (aux, "yes"))
		{
			lockfile = gn_device_lock (state.config.port_device);
			if (lockfile == NULL)
				{
					KMessageBox::sorry (0, "Lock file error. I can't connect to the phone.");
					return;
				}
		}

	globalKApp->processEvents();
	error = gn_gsm_initialise (&state);
	globalKApp->processEvents();
	if (error != GN_ERR_NONE)
		{
			KMessageBox::sorry (0, "Telephone interface init failed: " + QString (gn_error_print (error)));
			if (lockfile)
				gn_device_unlock (lockfile);
		}
	else
		{
			phoneConnected = TRUE;
			emit connected ();
			globalKApp->processEvents();
		}
}

void
SyncView::slotParseConfig ()
{
	KConfig *knsync_cfg = globalKApp->config ();

	knsync_cfg->setGroup ("General");
	transferAllKab2Phone = knsync_cfg->readBoolEntry ("TranferAllKAB2Phone", FALSE);
	transferAllPhone2Kab = knsync_cfg->readBoolEntry ("TranferAllPhone2KAB", FALSE);
	dontDisplayProblematic = knsync_cfg->readBoolEntry("DontDisplayProblematic");

	phoneEncoding = knsync_cfg->readEntry ("PhoneEncoding", "Latin1");
	memType = knsync_cfg->readEntry ("MemoryType", i18n("Phone + SIM"));

	if(memType != i18n("Phone + SIM") && memType != i18n("Phone") && memType != i18n("SIM"))
		memType = i18n("Phone + SIM");

	preferredPhoneOrder = knsync_cfg->readEntry ("PreferredPhoneOrder", "%C %H %B %G %F");

	knsync_cfg->setGroup ("Startup");
	startupConnectToPhone = knsync_cfg->readBoolEntry ("ConnectToPhone", FALSE);
	startupReadKab = knsync_cfg->readBoolEntry ("ReadKAB", FALSE);
	startupReadPhone = knsync_cfg->readBoolEntry ("ReadPhoneBook", FALSE);

	knsync_cfg->setGroup ("Name Format");
	separatorStr = knsync_cfg->readEntry ("SeparatorString", " ");
	setFmtNameInKab = knsync_cfg->readBoolEntry ("SetFormattedNameInKAB");
	W1Action = knsync_cfg->readEntry ("W1Action", "Ask");
	W2Action = knsync_cfg->readEntry ("W2Action", "Ask");
	W3Action = knsync_cfg->readEntry ("W3Action", "Ask");
	W3PAction = knsync_cfg->readEntry ("W3PAction", "Ask");

	knsync_cfg->setGroup ("Address Format");
	addressFormat = knsync_cfg->readEntry ("PostalAddressFormat", "%S, %L, %P, %R, %C");
	addrFmtMismatch=knsync_cfg->readEntry("PostalAddressFormatMismatch", "Notify");
		if (addrFmtMismatch == "Ask") // This is for compatibility-upgrade reasons.
			{
				knsync_cfg->writeEntry("PostalAddressFormatMismatch", "Notify");
				knsync_cfg->sync ();
				addrFmtMismatch=knsync_cfg->readEntry("PostalAddressFormatMismatch", "Notify");
			}

	knsync_cfg->setGroup("Caller Groups");
	groupNames.clear ();
	groupNames.append (knsync_cfg->readEntry ("Group0Name", i18n ("Family")));
	groupNames.append (knsync_cfg->readEntry ("Group1Name", i18n ("VIP")));
	groupNames.append (knsync_cfg->readEntry ("Group2Name", i18n ("Friends")));
	groupNames.append (knsync_cfg->readEntry ("Group3Name", i18n ("Colleagues")));
	groupNames.append (knsync_cfg->readEntry ("Group4Name", i18n ("Other")));
	groupNames.append (knsync_cfg->readEntry ("Group5Name", i18n ("No group")));
}

void
SyncView::itemDoubleClicked (QListViewItem * item, const QPoint & pos, int c)
{
	if (pos.x () && c);

	if (!item->isSelectable ())
		return;

	globalItem = item;
	showAddressee ();
}

void
SyncView::displayKAB ()
{
	int i = 0, column = 0;
	KABListView->clear ();
	KABC::AddressBook::Iterator it;
	typedef QValueListIterator<KABC::PhoneNumber> PhoneIterator;
	typedef QValueListIterator<KABC::Address> AddressIterator;
	KListViewItem *klvi = 0;

	if(!KABAddressees)
		return;

	KABListView->clear ();

	for (i = 0; KABAddressees[i] != 0; i++)
		{
			klvi = new KListViewItem (KABListView);

			klvi->setText (KAB_POS_COL, KABAddressees[i]->custom ("KnokiiSync", "Position"));

			klvi->setText (KAB_SURN_COL, KABAddressees[i]->familyName ());
			klvi->setText (KAB_NAME_COL, KABAddressees[i]->givenName ());

			QString group = KABAddressees[i]->custom ("KnokiiSync", "GROUP_ID");
			int groupId = 0;
			if (group != "")
				groupId = group.toInt ();
			else
				groupId = 5;

			QStringList::Iterator grpNamesIt = groupNames.begin ();
			for (int j = 0; j < GN_PHONEBOOK_CALLER_GROUPS_MAX_NUMBER; j++)
				{
					if (j != groupId && grpNamesIt != groupNames.end ())
							grpNamesIt++;
					else
						break;
				}

			klvi->setText (KAB_GRP_COL, *grpNamesIt);

			KABC::PhoneNumber::List phoneNumbers = KABAddressees[i]->phoneNumbers ();

			for (PhoneIterator pI = phoneNumbers.begin (); pI != phoneNumbers.end (); ++pI)
				{
					int type = (*pI).type ();
					column = KAB_GEN_COL;

					if (type & KABC::PhoneNumber::Home)
						column = KAB_HOME_COL;

					if (type & KABC::PhoneNumber::Work)
						column = KAB_WORK_COL;

					if (type & KABC::PhoneNumber::Cell)
						column = KAB_CELL_COL;

					if (type & KABC::PhoneNumber::Fax)
						column = KAB_FAX_COL;

					if (type & KABC::PhoneNumber::Voice)
						column = KAB_GEN_COL;

					if (column)
						{
							if (klvi->text (column) != "")
								{
									QListViewItem * child = klvi->firstChild ();

									while (child && child->text (column) != "")
										child = child->nextSibling ();

									if (!child)
										child = new KListViewItem (klvi);

									child->setText (column, (*pI).number ());
									child->setSelectable (FALSE);
									klvi->setOpen (TRUE);
								}
							else
								klvi->setText (column, (*pI).number ());
						}
				}

			column = KAB_EMAIL_COL;
			QStringList emails = KABAddressees[i]->emails ();
			for (QStringList::Iterator emI = emails.begin ();
					emI != emails.end (); ++emI)
				{
					if (klvi->text (column) != "")
						{
							QListViewItem * child = klvi->firstChild ();

							while (child && child->text (column) != "")
								child = child->nextSibling ();

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (column, *emI);
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						klvi->setText (column, *emI);
				}

			QString tmpName;
			int tmpCounter = 1;
			QStringList urls;
			QString url;
			KURL _url = KABAddressees[i]->url ();
			if (_url.prettyURL () != "")
				{
					urls.append (_url.prettyURL ());
					do
						{
							tmpName = "E_URL" + QString::number (tmpCounter);
							url = KABAddressees[i]->custom ("KnokiiSync", tmpName);
							if (url != "")
								{
								urls.append (url);
								}
							tmpCounter++;
						}
					while (url != "");
				}

			column = KAB_URL_COL;
			for (QStringList::Iterator urI = urls.begin ();
					urI != urls.end (); ++urI)
				{
					if (klvi->text (column) != "")
						{
							QListViewItem * child = klvi->firstChild ();

							while (child && child->text (column) != "")
								child = child->nextSibling ();

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (column, *urI);
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						klvi->setText (column, *urI);
				}

			KABC::Address::List addresses = KABAddressees[i]->addresses ();
			column = KAB_ADDR_COL;

			for (AddressIterator aI = addresses.begin ();
					aI != addresses.end ();
					++aI)
				{
					int occupied = 0;

					for(column = KAB_ADDR_COL; column <= KAB_CNTR_COL; column++)
						if (klvi->text (column) != "")
							occupied = 1;
						else
							occupied = 0;

					if (occupied)
						{
							QListViewItem * child = klvi->firstChild ();

							while (child )
								{
									for(column = KAB_ADDR_COL; column <= KAB_CNTR_COL; column++)
										if (klvi->text (column) != "")
											occupied = 1;
										else
											occupied = 0;

									if(occupied)
										child = child->nextSibling ();
								}

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (KAB_ADDR_COL, (*aI).street ());
							child->setText (KAB_LOCL_COL, (*aI).locality ());
							child->setText (KAB_REGN_COL, (*aI).region ());
							child->setText (KAB_POST_COL, (*aI).postalCode ());
							child->setText (KAB_CNTR_COL, (*aI).country ());
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						{
							klvi->setText (KAB_ADDR_COL, (*aI).street ());
							klvi->setText (KAB_LOCL_COL, (*aI).locality ());
							klvi->setText (KAB_REGN_COL, (*aI).region ());
							klvi->setText (KAB_POST_COL, (*aI).postalCode ());
							klvi->setText (KAB_CNTR_COL, (*aI).country ());
						}
				}

			tmpCounter = 1;
			QStringList notes;
			QString note =  KABAddressees[i]->note ();
			if (note != "")
				{
					notes.append (note);
					do
						{
							tmpName = "E_NOTE" + QString::number (tmpCounter);
							note = KABAddressees[i]->custom ("KnokiiSync", tmpName);
							if (note != "")
								{
								notes.append (note);
								}
							tmpCounter++;
						}
					while (note != "");
				}

			column = KAB_NOTE_COL;
			for (QStringList::Iterator nI = notes.begin ();
					nI != notes.end (); ++nI)
				{
					if (klvi->text (column) != "")
						{
							QListViewItem * child = klvi->firstChild ();

							while (child && child->text (column) != "")
								child = child->nextSibling ();

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (column, *nI);
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						klvi->setText (column, *nI);
				}

			KABC::PhoneNumber::List prefPhones = KABAddressees[i]->phoneNumbers (KABC::PhoneNumber::Pref);

			if (prefPhones.count () > 1)
				{
					QStringList prefPhoneFormat = QStringList::split(" ", preferredPhoneOrder);
					for (QStringList::Iterator it = prefPhoneFormat.begin (); it != prefPhoneFormat.end (); ++it)
						{
							QChar c = (*it).at (1);
							switch (c)
								{
									case 'C': prefPhones = KABAddressees[i]->phoneNumbers (KABC::PhoneNumber::Pref & KABC::PhoneNumber::Cell);
														break;
									case 'H': prefPhones = KABAddressees[i]->phoneNumbers (KABC::PhoneNumber::Pref & KABC::PhoneNumber::Home);
														break;
									case 'B': prefPhones = KABAddressees[i]->phoneNumbers (KABC::PhoneNumber::Pref & KABC::PhoneNumber::Work);
														break;
									case 'G': prefPhones = KABAddressees[i]->phoneNumbers (KABC::PhoneNumber::Pref & KABC::PhoneNumber::Voice);
														break;
									case 'F': prefPhones = KABAddressees[i]->phoneNumbers (KABC::PhoneNumber::Pref & KABC::PhoneNumber::Fax);
														break;
									default:	prefPhones = KABAddressees[i]->phoneNumbers (KABC::PhoneNumber::Pref & KABC::PhoneNumber::Voice);
														break;
								}
						}
				}

			PhoneIterator pI = prefPhones.begin ();
			QString prefPhone = (*pI).number ();

			klvi->setText (KAB_PREF_COL, prefPhone);
		}
}

void
SyncView::readKABClicked ()
{
	readKAB ();
	displayKAB ();
}

void
SyncView::readMobClicked ()
{
	readMobPhBook ();
}

void
SyncView::displayMobPhBook ()
{
	int i = 0;
	int column;
	KListViewItem * klvi = NULL;
	QString location, name, familyName, givenName;

	typedef QValueListIterator<KABC::PhoneNumber> PhoneIterator;
	typedef QValueListIterator<KABC::Address> AddressIterator;
	mobListView->clear ();

	if(!phoneAddressees)
		return;

	for (i = 0; phoneAddressees[i] != 0; i++)
		{
			location = phoneAddressees[i]->custom ("KnokiiSync", "MOB_POS");
			if (location == "***" && dontDisplayProblematic)
				continue;

			klvi = new KListViewItem (mobListView);
			klvi->setText (MOB_POS_COL, location);
			klvi->setText (MOB_SURN_COL, phoneAddressees[i]->familyName ());
			klvi->setText (MOB_NAME_COL, phoneAddressees[i]->givenName ());

			QString group = phoneAddressees[i]->custom ("KnokiiSync", "GROUP_ID");
			int groupId = 0;
			if (group != "")
				groupId = group.toInt ();
			else
				groupId = 5;

			QStringList::Iterator grpNamesIt = groupNames.begin ();
			for (int j = 0; j < GN_PHONEBOOK_CALLER_GROUPS_MAX_NUMBER; j++)
				{
					if (j != groupId && grpNamesIt != groupNames.end ())
							grpNamesIt++;
					else
						break;
				}

			klvi->setText (MOB_GRP_COL, *grpNamesIt);

			KABC::PhoneNumber::List phoneNumbers = phoneAddressees[i]->phoneNumbers ();

			for (PhoneIterator pI = phoneNumbers.begin (); pI != phoneNumbers.end (); ++pI)
				{
					int type = (*pI).type ();
					column = MOB_GEN_COL;
					if (type & KABC::PhoneNumber::Home)
						column = MOB_HOME_COL;

					if (type & KABC::PhoneNumber::Work)
						column = MOB_WORK_COL;

					if (type & KABC::PhoneNumber::Cell)
						column = MOB_CELL_COL;

					if (type & KABC::PhoneNumber::Fax)
						column = MOB_FAX_COL;

					if (type & KABC::PhoneNumber::Voice)
						column = MOB_GEN_COL;

					if (column)
						{
							if (klvi->text (column) != "")
								{
									QListViewItem * child = klvi->firstChild ();

									while (child && child->text (column) != "")
										child = child->nextSibling ();

									if (!child)
										child = new KListViewItem (klvi);

									child->setText (column, (*pI).number ());
									child->setSelectable (FALSE);
									klvi->setOpen (TRUE);
								}
							else
								klvi->setText (column, (*pI).number ());
						}
				}

			column = MOB_EMAIL_COL;
			QStringList emails = phoneAddressees[i]->emails ();
			for (QStringList::Iterator emI = emails.begin ();
					emI != emails.end (); ++emI)
				{
					if (klvi->text (column) != "")
						{
							QListViewItem * child = klvi->firstChild ();

							while (child && child->text (column) != "")
								child = child->nextSibling ();

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (column, *emI);
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						klvi->setText (column, *emI);
				}

			QString tmpName;
			int tmpCounter = 1;
			QStringList urls;
			QString url;
			KURL _url = phoneAddressees[i]->url ();
			if (_url.prettyURL () != "")
				{
					urls.append (_url.prettyURL ());
					do
						{
							tmpName = "E_URL" + QString::number (tmpCounter);
							url = phoneAddressees[i]->custom ("KnokiiSync", tmpName);
							if (url != "")
								{
								urls.append (url);
								}
							tmpCounter++;
						}
					while (url != "");
				}

			column = MOB_URL_COL;
			for (QStringList::Iterator urI = urls.begin ();
					urI != urls.end (); ++urI)
				{
					if (klvi->text (column) != "")
						{
							QListViewItem * child = klvi->firstChild ();

							while (child && child->text (column) != "")
								child = child->nextSibling ();

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (column, *urI);
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						klvi->setText (column, *urI);
				}

			KABC::Address::List addresses = phoneAddressees[i]->addresses ();
			column = MOB_ADDR_COL;

			for (AddressIterator aI = addresses.begin ();
					aI != addresses.end ();
					++aI)
				{
					int occupied = 0;

					for(column = MOB_ADDR_COL; column <= MOB_CNTR_COL; column++)
						if (klvi->text (column) != "")
							occupied = 1;
						else
							occupied = 0;

					if (occupied)
						{
							QListViewItem * child = klvi->firstChild ();

							while (child )
								{
									for(column = MOB_ADDR_COL; column <= MOB_CNTR_COL; column++)
										if (klvi->text (column) != "")
											occupied = 1;
										else
											occupied = 0;

									if(occupied)
										child = child->nextSibling ();
								}

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (MOB_ADDR_COL, (*aI).street ());
							child->setText (MOB_LOCL_COL, (*aI).locality ());
							child->setText (MOB_REGN_COL, (*aI).region ());
							child->setText (MOB_POST_COL, (*aI).postalCode ());
							child->setText (MOB_CNTR_COL, (*aI).country ());
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						{
							klvi->setText (MOB_ADDR_COL, (*aI).street ());
							klvi->setText (MOB_LOCL_COL, (*aI).locality ());
							klvi->setText (MOB_REGN_COL, (*aI).region ());
							klvi->setText (MOB_POST_COL, (*aI).postalCode ());
							klvi->setText (MOB_CNTR_COL, (*aI).country ());
						}
				}

			tmpCounter = 1;
			QStringList notes;
			QString note =  phoneAddressees[i]->note ();
			if (note != "")
				{
					notes.append (note);
					do
						{
							tmpName = "E_NOTE" + QString::number (tmpCounter);
							note = phoneAddressees[i]->custom ("KnokiiSync", tmpName);
							if (note != "")
								{
								notes.append (note);
								}
							tmpCounter++;
						}
					while (note != "");
				}

			column = MOB_NOTE_COL;
			for (QStringList::Iterator nI = notes.begin ();
					nI != notes.end (); ++nI)
				{
					if (klvi->text (column) != "")
						{
							QListViewItem * child = klvi->firstChild ();

							while (child && child->text (column) != "")
								child = child->nextSibling ();

							if (!child)
								child = new KListViewItem (klvi);

							child->setText (column, *nI);
							child->setSelectable (FALSE);
							klvi->setOpen (TRUE);
						}
					else
						klvi->setText (column, *nI);
				}

			KABC::PhoneNumber kPrefPhone = phoneAddressees[i]->phoneNumber (KABC::PhoneNumber::Pref);
			QString prefPhone = kPrefPhone.number ();

			klvi->setText (MOB_PREF_COL, prefPhone);
		}
}

void
SyncView::phoneViewRightClicked (QListViewItem * item, const QPoint & pos, int column)
{
	column = column;
	globalItem = item;
	phonePopupMenu->exec (pos);
}

void
SyncView::showAddressee ()
{
	KABC::Addressee * addressee = 0;
	QString location;
	QString uid;
	int i = 0;

	location = globalItem->text (MOB_POS_COL);

	if (location == "***")
		return;

	uid = "KNSYNC_" + location;

	for (i = 0; phoneAddressees[i] != 0; i++)
		{
			if (phoneAddressees[i]->uid() == uid)
				{
					addressee = phoneAddressees[i];
					break;
				}
		}

	if (!addressee)
		{
			KMessageBox::sorry (0, "showAddressee: Internal error. Please contact the author.");
			return;
		}

	if (addresseeDlg)
		{
			delete addresseeDlg;
			addresseeDlg = 0;
		}

	addresseeDlg = new AddresseeDlg;
	connect (addresseeDlg, SIGNAL (OKButtonClicked ()), this, SLOT (addreseeOKClicked ()));
	connect (addresseeDlg, SIGNAL (clrOvrButtonClicked ()), this, SLOT (addreseeClrOvrDataClicked ()));
	addresseeDlg->setAddressee(addressee);
	addresseeDlg->displayAddressee();
	addresseeDlg->show ();
}

void
SyncView::xferGlobalItem2KAB ()
{
	writeToKAB (globalItem);
	readKAB ();
	displayKAB ();
}

void
SyncView::uploadContactToPhone (KABC::Addressee * phoneAddressee, int location)
{
	gn_error error;
	gn_phonebook_entry entry;
	QCString locEncData;
	int subentry = 0;
	typedef QValueListIterator<KABC::PhoneNumber> PhoneIterator;
	typedef QValueListIterator<KABC::Address> AddressIterator;

	if (!startupConnectToPhone)
		busInit ();

	gn_data_clear (&data);
	data.phonebook_entry = &entry;
	entry.memory_type = GN_MT_ME;
	entry.location = location;

	QString formattedName = phoneAddressee->formattedName ();
	if (formattedName == "")
		{
			KMessageBox::sorry (0, i18n ("This contact hasn't got a formatted name. I can't upload this contact."));
			if (!startupConnectToPhone)
				busTerminate ();
			return;
		}
	locEncData = utf82loc_enc (formattedName, phoneEncoding);
	strcpy (entry.name, locEncData);

	QString group = phoneAddressee->custom ("KnokiiSync", "GROUP_ID");
	if (group != "")
		entry.caller_group = group.toInt ();
	else
		entry.caller_group = 5;

	KABC::PhoneNumber::List prefPhones = phoneAddressee->phoneNumbers (KABC::PhoneNumber::Pref);
	PhoneIterator pI = prefPhones.begin ();
	QString prefPhone = (*pI).number ();
	locEncData = utf82loc_enc (prefPhone, phoneEncoding);
	if (locEncData != "")
		strcpy (entry.number, locEncData);

	KABC::PhoneNumber::List phoneNumbers = phoneAddressee->phoneNumbers ();

	if (prefPhone == "")
		{
			 pI = phoneNumbers.begin ();
			 prefPhone = (*pI).number ();
			 locEncData = utf82loc_enc (prefPhone, phoneEncoding);
			 if (locEncData != "")
				 strcpy (entry.number, locEncData);
		}

	for (pI = phoneNumbers.begin (); pI != phoneNumbers.end (); ++pI)
		{
			if((*pI).number () != "")
				{
					int type = (*pI).type ();
					entry.subentries[subentry].entry_type = GN_PHONEBOOK_ENTRY_Number;
					entry.subentries[subentry].number_type = GN_PHONEBOOK_NUMBER_General;

					if (type & KABC::PhoneNumber::Home)
						entry.subentries[subentry].number_type = GN_PHONEBOOK_NUMBER_Home;

					if (type & KABC::PhoneNumber::Work)
						entry.subentries[subentry].number_type = GN_PHONEBOOK_NUMBER_Work;

					if (type & KABC::PhoneNumber::Cell)
						entry.subentries[subentry].number_type = GN_PHONEBOOK_NUMBER_Mobile;

					if (type & KABC::PhoneNumber::Fax)
						entry.subentries[subentry].number_type = GN_PHONEBOOK_NUMBER_Fax;

					if (type & KABC::PhoneNumber::Voice)
						entry.subentries[subentry].number_type = GN_PHONEBOOK_NUMBER_General;

					locEncData = utf82loc_enc ((*pI).number (), phoneEncoding);
					strcpy (entry.subentries[subentry].data.number, locEncData);
					entry.subentries[subentry].id = subentry;
					subentry++;
				}
		}

	QStringList emails = phoneAddressee->emails ();
	for (QStringList::Iterator emI = emails.begin ();
			emI != emails.end (); ++emI)
		{
			if(*emI != "")
				{
					locEncData = utf82loc_enc (*emI, phoneEncoding);
					entry.subentries[subentry].entry_type = GN_PHONEBOOK_ENTRY_Email;
					strcpy (entry.subentries[subentry].data.number, locEncData);
					if (prefPhone == "")
						{
							prefPhone = *emI;
							strcpy (entry.number, locEncData);
						}
					entry.subentries[subentry].id = subentry;
					subentry++;
				}
		}

	KURL kurl = phoneAddressee->url ();
	QString url = kurl.prettyURL ();
	QStringList urls;
	if (url != "")
		{
			urls.append (url);
			QString tmpName;
			int tmpCounter = 1;
			do
				{
					tmpName = "E_URL" + QString::number (tmpCounter);
					url = phoneAddressee->custom ("KnokiiSync", tmpName);
					if (url != "")
						urls.append (url);
					tmpCounter++;
				}
			while (url != "");
		}

	for (QStringList::Iterator urI = urls.begin (); urI != urls.end (); ++urI)
		{
			url = *urI;
			if (url != "")
				{
					if (url.length () > GN_PHONEBOOK_NUMBER_MAX_LENGTH)
						url = url.left (GN_PHONEBOOK_NUMBER_MAX_LENGTH - 3) + "...";
					locEncData = utf82loc_enc (url, phoneEncoding);
					entry.subentries[subentry].entry_type = GN_PHONEBOOK_ENTRY_URL;
					strcpy (entry.subentries[subentry].data.number, locEncData);
					entry.subentries[subentry].id = subentry;
					subentry++;
				}
		}

	QString note = phoneAddressee->note ();
	QStringList notes;
	if (note != "")
		{
			notes.append (note);
			QString tmpName;
			int tmpCounter = 1;
			do
				{
					tmpName = "E_NOTE" + QString::number (tmpCounter);
					note = phoneAddressee->custom ("KnokiiSync", tmpName);
					if (note != "")
						notes.append (note);
					tmpCounter++;
				}
			while (note != "");
		}

	for (QStringList::Iterator nI = notes.begin (); nI != notes.end (); ++nI)
		{
			note = *nI;
			locEncData = utf82loc_enc (note, phoneEncoding);
			if (locEncData.length () > GN_PHONEBOOK_NUMBER_MAX_LENGTH)
				locEncData = locEncData.left (GN_PHONEBOOK_NUMBER_MAX_LENGTH - 3) + "...";
			entry.subentries[subentry].entry_type = GN_PHONEBOOK_ENTRY_Note;
			strcpy (entry.subentries[subentry].data.number, locEncData);
			entry.subentries[subentry].id = subentry;
			subentry++;
		}

	KABC::Address::List addresses = phoneAddressee->addresses ();
	for (AddressIterator aI = addresses.begin (); aI != addresses.end (); ++aI)
		{
			QString address = addressFormat;
			address.replace ("%S", (*aI).street ());
			address.replace ("%L", (*aI).locality ());
			address.replace ("%R", (*aI).region ());
			address.replace ("%P", (*aI).postalCode ());
			address.replace ("%C", (*aI).country ());

			while (address.contains ( ", , "))
				address.replace ( ", , ", ", ");

			if (address == ", ")
				address = "";

			if (address != "")
				{
					locEncData = utf82loc_enc (address, phoneEncoding);
					if (locEncData.length () > GN_PHONEBOOK_NUMBER_MAX_LENGTH)
						locEncData = locEncData.left (GN_PHONEBOOK_NUMBER_MAX_LENGTH - 3) + "...";
					entry.subentries[subentry].entry_type = GN_PHONEBOOK_ENTRY_Postal;
					strcpy (entry.subentries[subentry].data.number, locEncData);
					entry.subentries[subentry].id = subentry;
					subentry++;
				}
		}

	entry.subentries_count = subentry;
	entry.empty = 1;

	error = gn_sm_functions (GN_OP_WritePhonebook, &data, &state);

	if (error != GN_ERR_NONE)
		KMessageBox::sorry (0,
			i18n ("Writing an entry to the phone failed: %1. There could be problems.").arg (QString (gn_error_print (error))));

	if (!startupConnectToPhone)
		busTerminate ();
}

void
SyncView::updateGlobalItemInPhone ()
{
	KABC::Addressee * addressee = 0;
	QString tmpLoc;
	int location;
	QString uid;
	int i = 0;
	QString formattedName;

	int reply = KMessageBox::questionYesNo(0, i18n ("Are you sure you want to FULLY update the contact in the phone, as it appears in this window?"), i18n ("Question"));
	if (reply != KMessageBox::Yes)
		return;
	
	tmpLoc = globalItem->text (MOB_POS_COL);
	location = tmpLoc.toInt ();

	if (tmpLoc == "***")
		return;

	uid = "KNSYNC_" + tmpLoc;

	for (i = 0; phoneAddressees[i] != 0; i++)
		{
			if (phoneAddressees[i]->uid() == uid)
				{
					addressee = phoneAddressees[i];
					break;
				}
		}

	if (!addressee)
		{
			KMessageBox::sorry (0, "updateGlobalItemInPhone: Internal error. Please contact the author.");
			return;
		}

	uploadContactToPhone (addressee, location);
}

void
SyncView::splitName (KABC::Addressee * addressee, QString aName)
{
	QString partialName;

	if (aName == "")
		{
			addressee->insertCustom ("KnokiiSync", "E_CODE", "E_NONAME");
			return;
		}

	switch (aName.contains (separatorStr))
		{
			case 0: if (W1Action == "Ask")
								{
									addressee->insertCustom ("KnokiiSync", "E_CODE", "E_ASK");
								}
							else
								if (W1Action == "Name")
									{
										addressee->setGivenName (aName);
										addressee->insertCustom ("KnokiiSync", "E_CODE", "E_OK");
									}
								else
									if (W1Action == "Surname")
										{
											addressee->setFamilyName (aName);
											addressee->insertCustom ("KnokiiSync", "E_CODE", "E_OK");
										}
									else
										{
											addressee->insertCustom ("KnokiiSync", "E_CODE", "E_W1IGNORED");
										}
							addressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W1");
							break;
			case 1:	if (W2Action == "Ask")
								{
									addressee->insertCustom ("KnokiiSync", "E_CODE", "E_ASK");
								}
							else
								if (W2Action == "NameSurname")
									{
										addressee->setGivenName (aName.section(separatorStr, 0, 0));
										addressee->setFamilyName (aName.section(separatorStr, 1, 1));
										addressee->insertCustom ("KnokiiSync", "E_CODE", "E_OK");
									}
								else
									if (W2Action == "SurnameName")
										{
											addressee->setGivenName (aName.section(separatorStr, 1, 1));
											addressee->setFamilyName (aName.section(separatorStr, 0, 0));
											addressee->insertCustom ("KnokiiSync", "E_CODE", "E_OK");
										}
									else
										{
											addressee->insertCustom ("KnokiiSync", "E_CODE", "E_W2IGNORED");
										}
							addressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W2");
							break;
			case 2: if (W3Action == "Ask")
								{
									addressee->insertCustom ("KnokiiSync", "E_CODE", "E_ASK");
								}
							else
								if (W3Action == "NameAddSurname")
									{
										addressee->setGivenName (aName.section(separatorStr, 0, 0));
										addressee->setAdditionalName (aName.section(separatorStr, 1, 1));
										addressee->setFamilyName (aName.section(separatorStr, 2, 2));
										addressee->insertCustom ("KnokiiSync", "E_CODE", "E_OK");
									}
								else
									if (W3Action == "SurnameAddName")
										{
											addressee->setGivenName (aName.section(separatorStr, 2, 2));
											addressee->setAdditionalName (aName.section(separatorStr, 1, 1));
											addressee->setFamilyName (aName.section(separatorStr, 0, 0));
											addressee->insertCustom ("KnokiiSync", "E_CODE", "E_OK");
										}
									else
										{
											addressee->insertCustom ("KnokiiSync", "E_CODE", "E_W3IGNORED");
										}
							addressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W3");
							break;
			default: if (W3PAction == "Ask")
								{
									addressee->insertCustom ("KnokiiSync", "E_CODE", "E_ASK");
								}
							else
								if (W3PAction == "Ignore")
									{
										addressee->insertCustom ("KnokiiSync", "E_CODE", "E_W3PIGNORED");
									}
							addressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W3P");
							break;
		}
}

void SyncView::loadOverridenName (KABC::Addressee * addressee, QString nameInPhone)
{
	QString tmp, uid, eClass;
	int namesCount = 0;

	uid = addressee->uid ();
	KABC::Addressee foundAddressee = overridenAB->findByUid (uid);

	tmp = foundAddressee.custom ("KnokiiSync", "E_NAMEINPHONE");
	if (tmp != nameInPhone)
		return;

	tmp = foundAddressee.custom ("KnokiiSync", "E_NAME_OV");
	if (tmp == "1")
		addressee->setGivenName (foundAddressee.givenName ());

	tmp = foundAddressee.custom ("KnokiiSync", "E_SURNAME_OV");
	if (tmp == "1")
		addressee->setFamilyName (foundAddressee.familyName ());

	tmp = foundAddressee.custom ("KnokiiSync", "E_ADDNAME_OV");
	if (tmp == "1")
		addressee->setAdditionalName (foundAddressee.additionalName ());

	if (addressee->givenName () != "")
		namesCount++;
	if (addressee->familyName () != "")
		namesCount++;
	if (addressee->additionalName () != "")
		namesCount++;

	switch (namesCount)
		{
			case 1:	eClass = "E_W1";
							break;
			case 2:	eClass = "E_W2";
							break;
			case 3:	eClass = "E_W3";
							break;
			default:	eClass = "E_W3P";
							break;
		}
	addressee->insertCustom ("KnokiiSync", "E_CLASS", eClass);
}

void SyncView::handleCodes (KABC::Addressee * addressee)
{
	QString tmp, code;
	int pos = 0;

	code = addressee->custom ("KnokiiSync", "E_CODE");
	tmp = addressee->custom ("KnokiiSync", "MOB_POS");
	pos = tmp.toInt ();

	if (code == "E_OK")
		{
			addressee->removeCustom ("KnokiiSync", "E_CODE");
			return;
		}

	if (code.right (8) == "IGNORED")
		{
			addressee->removeCustom ("KnokiiSync", "E_CODE");
			addressee->insertCustom ("KnokiiSync", "MOB_POS", "***");
			addressee->setFamilyName (i18n ("Entry %1 ignored").arg(QString::number(pos)));
			tmp = code.left (3);
			if (tmp == "W1A")
				tmp = "1";
			else
				if (tmp == "W2A")
					tmp = "2";
				else
					if (tmp == "W3A")
						tmp = "3";
					else
						tmp = "3+";
			addressee->setGivenName (i18n ("Configured to ignore %1 word names.").arg(QString::number(pos)));
			return;
		}

	if (code == "E_NONAME")
		{
			addressee->insertCustom ("KnokiiSync", "MOB_POS", "***");
			addressee->setFamilyName (i18n ("Entry %1 ignored").arg(QString::number(pos)));
			addressee->setGivenName (i18n ("Entry has no name"));
			addressee->removeCustom ("KnokiiSync", "E_CODE");
			return;
		}

	if (code == "E_ASK")
		{
			addressee->removeCustom ("KnokiiSync", "E_CODE");
			AskList.append (addressee);
			return;
		}
}

void
SyncView::askNames ()
{
	QString tmp, uid;

	if (namesDlg && namesDlg->curAddressee)
		{
			uid = namesDlg->curAddressee->uid ();
			tmp = namesDlg->curAddressee->custom ("KnokiiSync", "E_REMEMBER");
			if (tmp == "1")
				{
					namesDlg->curAddressee->removeCustom ("KnokiiSync", "E_REMEMBER");
					overridenAB->insertAddressee (*(namesDlg->curAddressee));
					overridenAB->saveAddrBook ();
				}

			delete namesDlg;
			namesDlg = 0;
		}

	namesDlg = new NamesDialog;
	connect (namesDlg, SIGNAL (OKClicked ()), this, SLOT (namesDlgOKClicked ()));
	connect (namesDlg, SIGNAL (cancelClicked ()), this, SLOT (namesDlgCancelClicked ()));

	KABC::Addressee * addressee = AskList.first ();
	if (addressee)
		{
			namesDlg->clear ();
			namesDlg->askName (addressee, separatorStr);
			AskList.remove (addressee);
		}
	else
		{
			displayMobPhBook ();
		}
}

void
SyncView::insertIntoOverriden (KABC::Addressee * addressee)
{
	if (!overridenAB)
		return;

	overridenAB->insertAddressee (*addressee);
	overridenAB->saveAddrBook ();
}

void
SyncView::addreseeOKClicked ()
{
	QString overriden;

	KABC::Addressee * addressee = addresseeDlg->getAddressee ();
	overriden = addressee->custom ("KnokiiSync", "E_OVERRIDEN");
	if (overriden == "1")
		{
			addressee->removeCustom ("KnokiiSync", "E_OVERRIDEN");
			insertIntoOverriden (addressee);
		}
	displayMobPhBook ();
}

void
SyncView::removeFromOverriden (KABC::Addressee * addressee)
{
	if (!overridenAB)
		return;

	overridenAB->removeAddressee (*addressee);
	overridenAB->saveAddrBook ();
}

void
SyncView::addreseeClrOvrDataClicked ()
{
	int reply = KMessageBox::questionYesNo (0, i18n ("Are you sure you want to remove any overriden data for this contact?"));
		if (reply == KMessageBox::Yes)
			{
				KABC::Addressee * addressee1 = addresseeDlg->getAddressee ();
				if (isNameOverriden (addressee1))
					{
						removeFromOverriden (addressee1);
						addresseeDlg->hide ();
					}
				else
					KMessageBox::information (0, i18n ("This contact has no data overriden."));
			}
}

bool
SyncView::isNameOverriden (KABC::Addressee * addressee)
{
	QString uid, nameInPhone, overridenNameInPhone;

	if (!overridenAB)
		return false;

	uid = addressee->uid ();
	KABC::Addressee foundAddressee = overridenAB->findByUid (uid);

	nameInPhone = addressee->custom ("KnokiiSync", "E_NAMEINPHONE");
	overridenNameInPhone = foundAddressee.custom ("KnokiiSync", "E_NAMEINPHONE");

	if (foundAddressee.uid () == uid && nameInPhone == overridenNameInPhone)
		return true;
	else
		return false;
}

void
SyncView::setFormattedName (KABC::Addressee * addressee)
{
	QString eClass = addressee->custom ("KnokiiSync", "E_CLASS");
	QString name = addressee->givenName ();
	QString surname = addressee->familyName ();
	QString addName = addressee->additionalName ();
	QString frmtdname;

	if (eClass == "E_W1")
		{
			if (W1Action == "Name" || W1Action == "Surname")
				{
					frmtdname = name+surname;
				}
		}
	else
		{
			if (eClass == "E_W2")
				{
					if (W2Action == "NameSurname")
						{
							frmtdname = name+separatorStr+surname;
						}
					else
						{
							if (W2Action == "SurnameName")
								{
									frmtdname = surname+separatorStr+name;
								}
						}
				}
			else
				{
					if (eClass == "E_W3")
						{
							if (W2Action == "NameAddSurname")
								{
									frmtdname = name+separatorStr+addName+separatorStr+surname;
								}
							else
								{
									if (W2Action == "SurnameAddName")
										{
											frmtdname = surname+separatorStr+addName+separatorStr+name;
										}
								}
						}
				}
		}

	if (frmtdname == "")
		frmtdname = addressee->custom ("KnokiiSync", "E_NAMEINPHONE");

	addressee->setFormattedName (frmtdname);
}

void
SyncView::namesDlgOKClicked ()
{
	askNames ();
}

void
SyncView::namesDlgCancelClicked ()
{
	delete namesDlg;
	namesDlg = 0;
	displayMobPhBook ();
}
