#include "overridenab.h"

#include <kabc/resource.h>
#include <kstandarddirs.h>
#include <kabc/resourcefile.h>
#include <kabc/vcardformatplugin.h>

bool
OverridenAB::saveAddrBook ()
{
	KABC::Resource *resource = 0;
	QPtrList<KABC::Resource> list;

	deleteRemovedAddressees ();

	list = this->resources ();
	resource = list.at (0);
	KABC::Ticket * ticket = this->requestSaveTicket (resource);
	if (!ticket || !this->save (ticket))
		return false;
	else
		return true;
}

void
OverridenAB::setIMEI (QString IMEI)
{
	QString overridenABFile = locateLocal ("data", "knokiisync/"+IMEI+".vcf");
	KABC::Resource * resource = new KABC::ResourceFile (this, overridenABFile, new KABC::VCardFormatPlugin);
	resource->setReadOnly (false);
	resource->setFastResource (true);

	if (!addResource (resource))
		delete resource;

	setStandardResource (resource);
}

OverridenAB::OverridenAB ()
{
}

OverridenAB::OverridenAB (QString IMEI)
{
	QString overridenABFile = locateLocal ("data", "knokiisync/"+IMEI+".vcf");
	KABC::Resource * resource = new KABC::ResourceFile (this, overridenABFile, new KABC::VCardFormatPlugin);
	resource->setReadOnly (false);
	resource->setFastResource (true);

	if (!addResource (resource))
		delete resource;

	setStandardResource (resource);
	load ();
}

OverridenAB::~OverridenAB ()
{
}
