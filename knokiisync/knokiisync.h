/***************************************************************************
                          knokiisync.h  -  description
                             -------------------
    begin                : ��� ��� 28 17:02:54 EEST 2003
    copyright            : (C) 2003 by Dimitrios Stasinopoulos
    email                : dimitrios@linea.gr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KNOKIISYNC_H
#define KNOKIISYNC_H
 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

// include files for Qt

// include files for KDE 
#include <kapp.h>
#include <kmainwindow.h>
#include <kaccel.h>
#include <kaction.h>
#include <kurl.h>

#include "conf_dialog.h"

class SyncView;

class KnokiiSyncApp : public KMainWindow
{
  Q_OBJECT

  friend class SyncView;
  friend class cfgDlg;	

  public:

    KnokiiSyncApp(QWidget* parent=0, const char* name=0);
    ~KnokiiSyncApp();

		KApplication * KApp;

    /** the configuration object of the application */
    KConfig *config;

  protected:
    /** save general Options like all bar positions and status as well as the geometry and the recent file list to the configuration
     * file
     */ 	
    void saveOptions();
    /** read general Options again and initialize all variables like the recent file list
     */
    void readOptions();
    /** initializes the KActions of the application */
    void initActions();
    /** sets up the statusbar for the main window by initialzing a statuslabel.
     */
    void initStatusBar();
    /** creates the centerwidget of the KTMainWindow instance and sets it as the view
     */
    void initView();

    /** queryExit is called by KTMainWindow when the last window of the application is going to be closed during the closeEvent().
     * Against the default implementation that just returns true, this calls saveOptions() to save the settings of the last window's	
     * properties.
     * @see KTMainWindow#queryExit
     * @see KTMainWindow#closeEvent
     */
    virtual bool queryExit();
    /** saves the window properties for each open window during session end to the session config file, including saving the currently
     * opened file by a temporary filename provided by KApplication.
     * @see KTMainWindow#saveProperties
     */

  public slots:
    /** open a new application window by creating a new instance of KnokiiSyncApp */
    void slotFileNewWindow();
    /** clears the document in the actual view to reuse it as the new document */
    void slotFileNew();
    /** open a file and load it into the document*/
    void slotFileOpen();
    /** save a document */
    void slotFileSave();
    /** save a document by a new filename*/
    void slotFileSaveAs();
    /** asks for saving if the file is modified, then closes the actual file and window*/
    void slotFileClose();
    /** print the actual file */
    void slotFilePrint();
    /** closes all open windows by calling close() on each memberList item until the list is empty, then quits the application.
     * If queryClose() returns false because the user canceled the saveModified() dialog, the closing breaks.
     */
    void slotFileQuit();
    /** put the marked text/object into the clipboard and remove
     *	it from the document
     */
    void slotEditCut();
    /** put the marked text/object into the clipboard
     */
    void slotEditCopy();
    /** paste the clipboard into the document
     */
    void slotEditPaste();
    /** toggles the toolbar
     */
    void slotViewToolBar();
    /** toggles the statusbar
     */
    void slotViewStatusBar();
    /** changes the statusbar contents for the standard label permanently, used to indicate current actions.
     * @param text the text that is displayed in the statusbar
     */
    void slotStatusMsg(const QString &text);

    /** No descriptions */
    void slotConnecting();
    /** No descriptions */
    void slotConnected();
    /** No descriptions */
    void slotDisconnected();
    /** No descriptions */
    void slotDisconnecting();

    void slotShowCfgDlg();

    void slotCfgDlgOKed();

  private:

    /** view is the main widget which represents your working area. The View
     * class should handle all events of the view widget.  It is kept empty so
     * you can create your view according to your application's needs by
     * changing the view class.
     */

    SyncView * view;
    cfgDlg * cfgDialog;
    bool startupConnectToPhone;

    // KAction pointers to enable/disable actions
    KAction * fileClose;
    KAction * filePrint;
    KAction * fileQuit;
    KAction * editCut;
    KAction * editCopy;
    KAction * editPaste;
    KToggleAction * viewToolBar;
    KToggleAction * viewStatusBar;
    KAction * settingsConfigure;

};
 
#endif // KNOKIISYNC_H
