/****************************************************************************
** Form implementation generated from reading ui file 'conf_dialog.ui'
**
** Created: Κυρ Αύγ 17 16:53:35 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "conf_dialog.h"

#include <qvariant.h>
#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include "conf_dialog.ui.h"

/* 
 *  Constructs a cfgDlg as a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
cfgDlg::cfgDlg( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )

{
    if ( !name )
	setName( "cfgDlg" );
    cfgDlgLayout = new QGridLayout( this, 1, 1, 11, 6, "cfgDlgLayout"); 
    QSpacerItem* spacer = new QSpacerItem( 460, 31, QSizePolicy::Expanding, QSizePolicy::Minimum );
    cfgDlgLayout->addItem( spacer, 1, 0 );

    OKButton = new QPushButton( this, "OKButton" );
    OKButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, OKButton->sizePolicy().hasHeightForWidth() ) );
    OKButton->setMinimumSize( QSize( 110, 30 ) );

    cfgDlgLayout->addWidget( OKButton, 1, 1 );

    cancelButton = new QPushButton( this, "cancelButton" );
    cancelButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, cancelButton->sizePolicy().hasHeightForWidth() ) );
    cancelButton->setMinimumSize( QSize( 110, 30 ) );

    cfgDlgLayout->addWidget( cancelButton, 1, 2 );

    tabWidget = new QTabWidget( this, "tabWidget" );

    generalPage = new QWidget( tabWidget, "generalPage" );
    generalPageLayout = new QGridLayout( generalPage, 1, 1, 11, 6, "generalPageLayout"); 

    transferPhone2KabChkBox = new QCheckBox( generalPage, "transferPhone2KabChkBox" );

    generalPageLayout->addMultiCellWidget( transferPhone2KabChkBox, 1, 1, 0, 3 );

    transferKab2PhoneChkBox = new QCheckBox( generalPage, "transferKab2PhoneChkBox" );

    generalPageLayout->addMultiCellWidget( transferKab2PhoneChkBox, 0, 0, 0, 3 );

    textLabel1_5 = new QLabel( generalPage, "textLabel1_5" );
    textLabel1_5->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1_5->sizePolicy().hasHeightForWidth() ) );
    textLabel1_5->setMinimumSize( QSize( 230, 0 ) );
    textLabel1_5->setPaletteForegroundColor( QColor( 170, 0, 0 ) );
    QFont textLabel1_5_font(  textLabel1_5->font() );
    textLabel1_5_font.setPointSize( 8 );
    textLabel1_5->setFont( textLabel1_5_font ); 
    textLabel1_5->setScaledContents( TRUE );
    textLabel1_5->setAlignment( int( QLabel::WordBreak | QLabel::AlignCenter ) );

    generalPageLayout->addWidget( textLabel1_5, 3, 2 );

    textLabel1_4 = new QLabel( generalPage, "textLabel1_4" );

    generalPageLayout->addWidget( textLabel1_4, 4, 0 );

    encComboBox = new QComboBox( FALSE, generalPage, "encComboBox" );
    encComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, encComboBox->sizePolicy().hasHeightForWidth() ) );
    encComboBox->setAutoCompletion( TRUE );
    encComboBox->setDuplicatesEnabled( FALSE );

    generalPageLayout->addWidget( encComboBox, 3, 1 );
    QSpacerItem* spacer_2 = new QSpacerItem( 80, 30, QSizePolicy::Expanding, QSizePolicy::Minimum );
    generalPageLayout->addItem( spacer_2, 3, 3 );

    memComboBox = new QComboBox( FALSE, generalPage, "memComboBox" );
    memComboBox->setDuplicatesEnabled( FALSE );

    generalPageLayout->addWidget( memComboBox, 4, 1 );
    QSpacerItem* spacer_3 = new QSpacerItem( 335, 31, QSizePolicy::Expanding, QSizePolicy::Minimum );
    generalPageLayout->addMultiCell( spacer_3, 4, 4, 2, 3 );

    textLabel1 = new QLabel( generalPage, "textLabel1" );

    generalPageLayout->addWidget( textLabel1, 3, 0 );

    groupBox1 = new QGroupBox( generalPage, "groupBox1" );
    groupBox1->setColumnLayout(0, Qt::Vertical );
    groupBox1->layout()->setSpacing( 6 );
    groupBox1->layout()->setMargin( 11 );
    groupBox1Layout = new QGridLayout( groupBox1->layout() );
    groupBox1Layout->setAlignment( Qt::AlignTop );

    textLabel1_3 = new QLabel( groupBox1, "textLabel1_3" );

    groupBox1Layout->addWidget( textLabel1_3, 0, 0 );

    preferredOrderLE = new QLineEdit( groupBox1, "preferredOrderLE" );

    groupBox1Layout->addWidget( preferredOrderLE, 0, 1 );

    textLabel2_2 = new QLabel( groupBox1, "textLabel2_2" );

    groupBox1Layout->addMultiCellWidget( textLabel2_2, 1, 1, 0, 1 );

    generalPageLayout->addMultiCellWidget( groupBox1, 5, 5, 0, 3 );

    dontDisplayProblematicCB = new QCheckBox( generalPage, "dontDisplayProblematicCB" );

    generalPageLayout->addMultiCellWidget( dontDisplayProblematicCB, 2, 2, 0, 3 );
    tabWidget->insertTab( generalPage, "" );

    startupPage = new QWidget( tabWidget, "startupPage" );

    readPhoneBookChkBox = new QCheckBox( startupPage, "readPhoneBookChkBox" );
    readPhoneBookChkBox->setGeometry( QRect( 10, 90, 370, 31 ) );

    connPhoneChkBox = new QCheckBox( startupPage, "connPhoneChkBox" );
    connPhoneChkBox->setGeometry( QRect( 10, 10, 370, 30 ) );

    readKabChkBox = new QCheckBox( startupPage, "readKabChkBox" );
    readKabChkBox->setGeometry( QRect( 10, 50, 370, 30 ) );
    tabWidget->insertTab( startupPage, "" );

    confirmationPage = new QWidget( tabWidget, "confirmationPage" );

    overwriteKab2PhoneChkBox = new QCheckBox( confirmationPage, "overwriteKab2PhoneChkBox" );
    overwriteKab2PhoneChkBox->setGeometry( QRect( 10, 10, 668, 30 ) );

    overwritePhone2PKabChkBox = new QCheckBox( confirmationPage, "overwritePhone2PKabChkBox" );
    overwritePhone2PKabChkBox->setGeometry( QRect( 10, 50, 668, 30 ) );
    tabWidget->insertTab( confirmationPage, "" );

    syncPage = new QWidget( tabWidget, "syncPage" );
    syncPageLayout = new QGridLayout( syncPage, 1, 1, 11, 6, "syncPageLayout"); 

    buttonGroup1 = new QButtonGroup( syncPage, "buttonGroup1" );
    buttonGroup1->setColumnLayout(0, Qt::Vertical );
    buttonGroup1->layout()->setSpacing( 6 );
    buttonGroup1->layout()->setMargin( 11 );
    buttonGroup1Layout = new QGridLayout( buttonGroup1->layout() );
    buttonGroup1Layout->setAlignment( Qt::AlignTop );

    syncAskChkBox = new QRadioButton( buttonGroup1, "syncAskChkBox" );

    buttonGroup1Layout->addWidget( syncAskChkBox, 2, 0 );

    syncPhoneRulesChkBox = new QRadioButton( buttonGroup1, "syncPhoneRulesChkBox" );

    buttonGroup1Layout->addWidget( syncPhoneRulesChkBox, 1, 0 );

    syncKabRulesChkBox = new QRadioButton( buttonGroup1, "syncKabRulesChkBox" );

    buttonGroup1Layout->addWidget( syncKabRulesChkBox, 0, 0 );

    syncPageLayout->addWidget( buttonGroup1, 0, 0 );
    tabWidget->insertTab( syncPage, "" );

    tab = new QWidget( tabWidget, "tab" );
    tabLayout = new QGridLayout( tab, 1, 1, 11, 6, "tabLayout"); 

    setFmtNameInKabCB = new QCheckBox( tab, "setFmtNameInKabCB" );

    tabLayout->addMultiCellWidget( setFmtNameInKabCB, 0, 0, 3, 4 );

    textLabel2 = new QLabel( tab, "textLabel2" );

    tabLayout->addWidget( textLabel2, 0, 0 );

    separatorLE = new QLineEdit( tab, "separatorLE" );

    tabLayout->addMultiCellWidget( separatorLE, 0, 0, 1, 2 );

    buttonGroup6 = new QButtonGroup( tab, "buttonGroup6" );
    buttonGroup6->setColumnLayout(0, Qt::Vertical );
    buttonGroup6->layout()->setSpacing( 6 );
    buttonGroup6->layout()->setMargin( 11 );
    buttonGroup6Layout = new QGridLayout( buttonGroup6->layout() );
    buttonGroup6Layout->setAlignment( Qt::AlignTop );

    W1NameRB = new QRadioButton( buttonGroup6, "W1NameRB" );

    buttonGroup6Layout->addWidget( W1NameRB, 1, 0 );

    W1SurnameRB = new QRadioButton( buttonGroup6, "W1SurnameRB" );

    buttonGroup6Layout->addWidget( W1SurnameRB, 2, 0 );

    W1AskRB = new QRadioButton( buttonGroup6, "W1AskRB" );

    buttonGroup6Layout->addWidget( W1AskRB, 0, 0 );

    W1IgnoreRB = new QRadioButton( buttonGroup6, "W1IgnoreRB" );

    buttonGroup6Layout->addWidget( W1IgnoreRB, 3, 0 );

    tabLayout->addMultiCellWidget( buttonGroup6, 1, 1, 0, 1 );

    buttonGroup2_2 = new QButtonGroup( tab, "buttonGroup2_2" );
    buttonGroup2_2->setColumnLayout(0, Qt::Vertical );
    buttonGroup2_2->layout()->setSpacing( 6 );
    buttonGroup2_2->layout()->setMargin( 11 );
    buttonGroup2_2Layout = new QGridLayout( buttonGroup2_2->layout() );
    buttonGroup2_2Layout->setAlignment( Qt::AlignTop );

    W3AskRB = new QRadioButton( buttonGroup2_2, "W3AskRB" );

    buttonGroup2_2Layout->addWidget( W3AskRB, 0, 0 );

    W3IgnoreRB = new QRadioButton( buttonGroup2_2, "W3IgnoreRB" );

    buttonGroup2_2Layout->addWidget( W3IgnoreRB, 3, 0 );

    W3NameAddSurnameRB = new QRadioButton( buttonGroup2_2, "W3NameAddSurnameRB" );

    buttonGroup2_2Layout->addWidget( W3NameAddSurnameRB, 1, 0 );

    W3SurnameAddNameRB = new QRadioButton( buttonGroup2_2, "W3SurnameAddNameRB" );

    buttonGroup2_2Layout->addWidget( W3SurnameAddNameRB, 2, 0 );

    tabLayout->addMultiCellWidget( buttonGroup2_2, 2, 2, 0, 3 );

    buttonGroup2_3 = new QButtonGroup( tab, "buttonGroup2_3" );
    buttonGroup2_3->setColumnLayout(0, Qt::Vertical );
    buttonGroup2_3->layout()->setSpacing( 6 );
    buttonGroup2_3->layout()->setMargin( 11 );
    buttonGroup2_3Layout = new QGridLayout( buttonGroup2_3->layout() );
    buttonGroup2_3Layout->setAlignment( Qt::AlignTop );

    W3PAskRB = new QRadioButton( buttonGroup2_3, "W3PAskRB" );

    buttonGroup2_3Layout->addWidget( W3PAskRB, 0, 0 );

    W3PIgnoreRB = new QRadioButton( buttonGroup2_3, "W3PIgnoreRB" );

    buttonGroup2_3Layout->addWidget( W3PIgnoreRB, 3, 0 );

    tabLayout->addWidget( buttonGroup2_3, 2, 4 );

    buttonGroup2 = new QButtonGroup( tab, "buttonGroup2" );
    buttonGroup2->setColumnLayout(0, Qt::Vertical );
    buttonGroup2->layout()->setSpacing( 6 );
    buttonGroup2->layout()->setMargin( 11 );
    buttonGroup2Layout = new QGridLayout( buttonGroup2->layout() );
    buttonGroup2Layout->setAlignment( Qt::AlignTop );

    W2AskRB = new QRadioButton( buttonGroup2, "W2AskRB" );

    buttonGroup2Layout->addWidget( W2AskRB, 0, 0 );

    W2IgnoreRB = new QRadioButton( buttonGroup2, "W2IgnoreRB" );

    buttonGroup2Layout->addWidget( W2IgnoreRB, 3, 0 );

    W2SurnameNameRB = new QRadioButton( buttonGroup2, "W2SurnameNameRB" );

    buttonGroup2Layout->addWidget( W2SurnameNameRB, 2, 0 );

    W2NameSurnameRB = new QRadioButton( buttonGroup2, "W2NameSurnameRB" );

    buttonGroup2Layout->addWidget( W2NameSurnameRB, 1, 0 );

    tabLayout->addMultiCellWidget( buttonGroup2, 1, 1, 2, 4 );
    tabWidget->insertTab( tab, "" );

    tab_2 = new QWidget( tabWidget, "tab_2" );
    tabLayout_2 = new QGridLayout( tab_2, 1, 1, 11, 6, "tabLayout_2"); 

    buttonGroup5 = new QButtonGroup( tab_2, "buttonGroup5" );
    buttonGroup5->setColumnLayout(0, Qt::Vertical );
    buttonGroup5->layout()->setSpacing( 6 );
    buttonGroup5->layout()->setMargin( 11 );
    buttonGroup5Layout = new QGridLayout( buttonGroup5->layout() );
    buttonGroup5Layout->setAlignment( Qt::AlignTop );

    pAddressFormatNotifyRB = new QRadioButton( buttonGroup5, "pAddressFormatNotifyRB" );

    buttonGroup5Layout->addWidget( pAddressFormatNotifyRB, 0, 0 );

    pAddressFormatIgnoreRB = new QRadioButton( buttonGroup5, "pAddressFormatIgnoreRB" );

    buttonGroup5Layout->addWidget( pAddressFormatIgnoreRB, 1, 0 );

    tabLayout_2->addWidget( buttonGroup5, 1, 0 );

    buttonGroup3 = new QButtonGroup( tab_2, "buttonGroup3" );
    buttonGroup3->setColumnLayout(0, Qt::Vertical );
    buttonGroup3->layout()->setSpacing( 6 );
    buttonGroup3->layout()->setMargin( 11 );
    buttonGroup3Layout = new QGridLayout( buttonGroup3->layout() );
    buttonGroup3Layout->setAlignment( Qt::AlignTop );

    pAddrFrmtLEdit_2 = new QLineEdit( buttonGroup3, "pAddrFrmtLEdit_2" );
    pAddrFrmtLEdit_2->setReadOnly( TRUE );

    buttonGroup3Layout->addMultiCellWidget( pAddrFrmtLEdit_2, 4, 4, 0, 2 );

    textLabel2_4 = new QLabel( buttonGroup3, "textLabel2_4" );

    buttonGroup3Layout->addMultiCellWidget( textLabel2_4, 3, 3, 0, 2 );

    textLabel1_2_3_2 = new QLabel( buttonGroup3, "textLabel1_2_3_2" );
    textLabel1_2_3_2->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    buttonGroup3Layout->addWidget( textLabel1_2_3_2, 1, 2 );

    textLabel1_2_4_2 = new QLabel( buttonGroup3, "textLabel1_2_4_2" );
    textLabel1_2_4_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignLeft ) );

    buttonGroup3Layout->addWidget( textLabel1_2_4_2, 2, 0 );

    textLabel1_2_6 = new QLabel( buttonGroup3, "textLabel1_2_6" );
    textLabel1_2_6->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignLeft ) );

    buttonGroup3Layout->addWidget( textLabel1_2_6, 1, 0 );

    textLabel1_2_5_2 = new QLabel( buttonGroup3, "textLabel1_2_5_2" );
    textLabel1_2_5_2->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    buttonGroup3Layout->addWidget( textLabel1_2_5_2, 2, 2 );

    textLabel1_2_2_2 = new QLabel( buttonGroup3, "textLabel1_2_2_2" );
    textLabel1_2_2_2->setAlignment( int( QLabel::WordBreak | QLabel::AlignCenter ) );

    buttonGroup3Layout->addMultiCellWidget( textLabel1_2_2_2, 1, 2, 1, 1 );

    pAddrFrmtLEdit = new QLineEdit( buttonGroup3, "pAddrFrmtLEdit" );

    buttonGroup3Layout->addMultiCellWidget( pAddrFrmtLEdit, 0, 0, 0, 2 );

    tabLayout_2->addWidget( buttonGroup3, 0, 0 );
    tabWidget->insertTab( tab_2, "" );

    tab_3 = new QWidget( tabWidget, "tab_3" );
    tabLayout_3 = new QGridLayout( tab_3, 1, 1, 11, 6, "tabLayout_3"); 

    groupBox2 = new QGroupBox( tab_3, "groupBox2" );
    groupBox2->setColumnLayout(0, Qt::Vertical );
    groupBox2->layout()->setSpacing( 6 );
    groupBox2->layout()->setMargin( 11 );
    groupBox2Layout = new QGridLayout( groupBox2->layout() );
    groupBox2Layout->setAlignment( Qt::AlignTop );

    textLabel2_3 = new QLabel( groupBox2, "textLabel2_3" );

    groupBox2Layout->addWidget( textLabel2_3, 0, 0 );

    groupName1LE = new QLineEdit( groupBox2, "groupName1LE" );

    groupBox2Layout->addWidget( groupName1LE, 1, 1 );

    groupName2LE = new QLineEdit( groupBox2, "groupName2LE" );

    groupBox2Layout->addWidget( groupName2LE, 2, 1 );

    groupName3LE = new QLineEdit( groupBox2, "groupName3LE" );

    groupBox2Layout->addWidget( groupName3LE, 3, 1 );

    groupName4LE = new QLineEdit( groupBox2, "groupName4LE" );

    groupBox2Layout->addWidget( groupName4LE, 4, 1 );

    groupName5LE = new QLineEdit( groupBox2, "groupName5LE" );

    groupBox2Layout->addWidget( groupName5LE, 5, 1 );

    textLabel2_3_2 = new QLabel( groupBox2, "textLabel2_3_2" );

    groupBox2Layout->addWidget( textLabel2_3_2, 1, 0 );

    textLabel2_3_3 = new QLabel( groupBox2, "textLabel2_3_3" );

    groupBox2Layout->addWidget( textLabel2_3_3, 2, 0 );

    textLabel2_3_4 = new QLabel( groupBox2, "textLabel2_3_4" );

    groupBox2Layout->addWidget( textLabel2_3_4, 3, 0 );

    textLabel2_3_5 = new QLabel( groupBox2, "textLabel2_3_5" );

    groupBox2Layout->addWidget( textLabel2_3_5, 4, 0 );

    textLabel2_3_6 = new QLabel( groupBox2, "textLabel2_3_6" );

    groupBox2Layout->addWidget( textLabel2_3_6, 5, 0 );

    groupName0LE = new QLineEdit( groupBox2, "groupName0LE" );

    groupBox2Layout->addWidget( groupName0LE, 0, 1 );

    tabLayout_3->addWidget( groupBox2, 0, 0 );
    tabWidget->insertTab( tab_3, "" );

    cfgDlgLayout->addMultiCellWidget( tabWidget, 0, 0, 0, 2 );
    languageChange();
    resize( QSize(714, 468).expandedTo(minimumSizeHint()) );

    // signals and slots connections
    connect( OKButton, SIGNAL( clicked() ), this, SLOT( OKClicked() ) );
    connect( cancelButton, SIGNAL( clicked() ), this, SLOT( cancelClicked() ) );
    connect( separatorLE, SIGNAL( textChanged(const QString&) ), this, SLOT( separatorLEChanged(const QString&) ) );
    connect( separatorLE, SIGNAL( lostFocus() ), this, SLOT( separatorLELostFocus() ) );
    connect( pAddrFrmtLEdit, SIGNAL( textChanged(const QString&) ), this, SLOT( pAddrLEChanged(const QString&) ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
cfgDlg::~cfgDlg()
{
    destroy();
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void cfgDlg::languageChange()
{
    setCaption( tr( "Configuration" ) );
    OKButton->setText( tr( "OK" ) );
    cancelButton->setText( tr( "Cancel" ) );
    transferPhone2KabChkBox->setText( tr( "Transfer entries copies all the entries (Phone->KAB)" ) );
    QToolTip::add( transferPhone2KabChkBox, tr( "Normally, you have to select which entries you want to transfer. By checking this option, if no entries are selected, then all of them are transfered." ) );
    transferKab2PhoneChkBox->setText( tr( "Transfer entries copies all the entries (KAB->Phone)" ) );
    QToolTip::add( transferKab2PhoneChkBox, tr( "Normally, you have to select which entries you want to transfer. By checking this option, if no entries are selected, then all of them are transfered." ) );
    textLabel1_5->setText( tr( "Note! Set this properly, or KnokiiSync will crash when you will try to read the phone!" ) );
    textLabel1_4->setText( tr( "Memory in use:" ) );
    encComboBox->clear();
    encComboBox->insertItem( tr( "Latin1" ) );
    encComboBox->insertItem( tr( "Big5" ) );
    encComboBox->insertItem( tr( "Big5-HKSCS" ) );
    encComboBox->insertItem( tr( "eucJP" ) );
    encComboBox->insertItem( tr( "eucKR" ) );
    encComboBox->insertItem( tr( "GB2312" ) );
    encComboBox->insertItem( tr( "GBK" ) );
    encComboBox->insertItem( tr( "GB18030" ) );
    encComboBox->insertItem( tr( "JIS7" ) );
    encComboBox->insertItem( tr( "Shift-JIS" ) );
    encComboBox->insertItem( tr( "TSCII" ) );
    encComboBox->insertItem( tr( "KOI8-R" ) );
    encComboBox->insertItem( tr( "KOI8-U" ) );
    encComboBox->insertItem( tr( "ISO8859-1" ) );
    encComboBox->insertItem( tr( "ISO8859-2" ) );
    encComboBox->insertItem( tr( "ISO8859-3" ) );
    encComboBox->insertItem( tr( "ISO8859-4" ) );
    encComboBox->insertItem( tr( "ISO8859-5" ) );
    encComboBox->insertItem( tr( "ISO8859-6" ) );
    encComboBox->insertItem( tr( "ISO8859-7" ) );
    encComboBox->insertItem( tr( "ISO8859-8" ) );
    encComboBox->insertItem( tr( "ISO8859-8-i" ) );
    encComboBox->insertItem( tr( "ISO8859-9" ) );
    encComboBox->insertItem( tr( "ISO8859-10" ) );
    encComboBox->insertItem( tr( "ISO8859-15" ) );
    encComboBox->insertItem( tr( "IBM 850" ) );
    encComboBox->insertItem( tr( "IBM 866" ) );
    encComboBox->insertItem( tr( "CP874" ) );
    encComboBox->insertItem( tr( "CP1250" ) );
    encComboBox->insertItem( tr( "CP1251" ) );
    encComboBox->insertItem( tr( "CP1252" ) );
    encComboBox->insertItem( tr( "CP1253" ) );
    encComboBox->insertItem( tr( "CP1254" ) );
    encComboBox->insertItem( tr( "CP1255" ) );
    encComboBox->insertItem( tr( "CP1256" ) );
    encComboBox->insertItem( tr( "CP1257" ) );
    encComboBox->insertItem( tr( "CP1258" ) );
    encComboBox->insertItem( tr( "Apple Roman" ) );
    encComboBox->insertItem( tr( "TIS-620" ) );
    encComboBox->insertItem( tr( "UTF8" ) );
    QToolTip::add( encComboBox, tr( "If you get strange characters or question marks instead of correct entries, then you must define a proper encoding here." ) );
    memComboBox->clear();
    memComboBox->insertItem( tr( "Phone + SIM" ) );
    memComboBox->insertItem( tr( "Phone" ) );
    memComboBox->insertItem( tr( "SIM" ) );
    QToolTip::add( memComboBox, tr( "Defines which memory the phone uses to store entries." ) );
    textLabel1->setText( tr( "Phone Encoding:" ) );
    QToolTip::add( textLabel1, tr( "If you get strange characters or question marks instead of correct entries, then you must define a proper encoding here." ) );
    groupBox1->setTitle( tr( "Default Preferred number on KAB" ) );
    QToolTip::add( groupBox1, tr( "If a contact on the KDE Address Book does not have a phone selected as the default, then the phones specified here will be used, in the order specified. The first one available is used." ) );
    textLabel1_3->setText( tr( "Preferred phone order:" ) );
    preferredOrderLE->setText( tr( "%C %H %B %G %F" ) );
    textLabel2_2->setText( tr( "%C = Cellular	%H = Home	%B = Business	%G = General	%F = Fax" ) );
    dontDisplayProblematicCB->setText( tr( "Don't display empty, ignored or erroneous entries" ) );
    tabWidget->changeTab( generalPage, tr( "General" ) );
    readPhoneBookChkBox->setText( tr( "Read Phone Book" ) );
    QToolTip::add( readPhoneBookChkBox, tr( "Read the mobile phone's book on startup." ) );
    connPhoneChkBox->setText( tr( "Connect to phone" ) );
    QToolTip::add( connPhoneChkBox, tr( "Connect to the phone on startup, and disconnect on exiting." ) );
    readKabChkBox->setText( tr( "Read KDE Address Book" ) );
    QToolTip::add( readKabChkBox, tr( "Read the KDE Address Book on startup." ) );
    tabWidget->changeTab( startupPage, tr( "Startup" ) );
    overwriteKab2PhoneChkBox->setText( tr( "Overwrite entries (KAB->Phone)" ) );
    QToolTip::add( overwriteKab2PhoneChkBox, tr( "Ask whether to overwrite entries when transfering from the KDE Address Book to the Phone." ) );
    overwritePhone2PKabChkBox->setText( tr( "Overwrite entries (Phone->KAB)" ) );
    QToolTip::add( overwritePhone2PKabChkBox, tr( "Ask whether to overwrite entries when transfering from the Phone to KDE Address Book." ) );
    tabWidget->changeTab( confirmationPage, tr( "Confirmation" ) );
    buttonGroup1->setTitle( tr( "When 2 subentries conflict, give priority to..." ) );
    QToolTip::add( buttonGroup1, tr( "Not yet functional" ) );
    syncAskChkBox->setText( tr( "Neither. Ask me what to do." ) );
    syncPhoneRulesChkBox->setText( tr( "Phone's entries" ) );
    syncKabRulesChkBox->setText( tr( "KDE's Address Book entries" ) );
    tabWidget->changeTab( syncPage, tr( "Sync Conflicts" ) );
    setFmtNameInKabCB->setText( tr( "Set formatted name in KAB" ) );
    textLabel2->setText( tr( "Word seperator string:" ) );
    separatorLE->setText( QString::null );
    buttonGroup6->setTitle( tr( "If the entry has one word..." ) );
    W1NameRB->setText( tr( "Assume it's a name" ) );
    W1SurnameRB->setText( tr( "Assume it's a surname" ) );
    W1AskRB->setText( tr( "Ask what to do" ) );
    W1IgnoreRB->setText( tr( "Totally ignore entry" ) );
    buttonGroup2_2->setTitle( tr( "If entry has three words..." ) );
    W3AskRB->setText( tr( "Ask what to do" ) );
    W3IgnoreRB->setText( tr( "Totally ignore entry" ) );
    W3NameAddSurnameRB->setText( tr( "Assume \"Name Additional Name Surname\"" ) );
    W3SurnameAddNameRB->setText( tr( "Assume \"Surname Additional Name Name\"" ) );
    buttonGroup2_3->setTitle( tr( "If entry has more than three words..." ) );
    W3PAskRB->setText( tr( "Ask what to do" ) );
    W3PIgnoreRB->setText( tr( "Totally ignore entry" ) );
    buttonGroup2->setTitle( tr( "If entry has two words..." ) );
    W2AskRB->setText( tr( "Ask what to do" ) );
    W2IgnoreRB->setText( tr( "Totally ignore entry" ) );
    W2SurnameNameRB->setText( tr( "Assume \"Surname Name\"" ) );
    W2NameSurnameRB->setText( tr( "Assume \"Name Surname\"" ) );
    tabWidget->changeTab( tab, tr( "Name Format" ) );
    buttonGroup5->setTitle( tr( "If Postal Address Format does not match..." ) );
    QToolTip::add( buttonGroup5, tr( "Configures what happens if the postal address on the phone does not match with the format above." ) );
    pAddressFormatNotifyRB->setText( tr( "Notify and assume defaults" ) );
    pAddressFormatIgnoreRB->setText( tr( "Ignore address only" ) );
    buttonGroup3->setTitle( tr( "Postal Address Format on Phone" ) );
    pAddrFrmtLEdit_2->setText( tr( "1 Oxford Street, Mains Park, XY123B4, London, UK" ) );
    textLabel2_4->setText( tr( "Example address:" ) );
    textLabel1_2_3_2->setText( tr( "%R = Region" ) );
    textLabel1_2_4_2->setText( tr( "%P = Post Code" ) );
    textLabel1_2_6->setText( tr( "%S = Street" ) );
    textLabel1_2_5_2->setText( tr( "%C = Country" ) );
    textLabel1_2_2_2->setText( tr( "%L = Locality" ) );
    pAddrFrmtLEdit->setText( tr( "%S, %L, %P, %R, %C" ) );
    tabWidget->changeTab( tab_2, tr( "Address Format" ) );
    groupBox2->setTitle( tr( "Caller group names" ) );
    textLabel2_3->setText( tr( "Caller group 0:" ) );
    textLabel2_3_2->setText( tr( "Caller group 1:" ) );
    textLabel2_3_3->setText( tr( "Caller group 2:" ) );
    textLabel2_3_4->setText( tr( "Caller group 3:" ) );
    textLabel2_3_5->setText( tr( "Caller group 4:" ) );
    textLabel2_3_6->setText( tr( "Groupless contacts:" ) );
    tabWidget->changeTab( tab_3, tr( "Caller groups" ) );
}

