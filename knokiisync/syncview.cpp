/****************************************************************************
** Form implementation generated from reading ui file 'syncview.ui'
**
** Created: Κυρ Αύγ 17 15:18:24 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "syncview.h"

#include <qvariant.h>
#include <klistview.h>
#include <kpushbutton.h>
#include <qheader.h>
#include <qlabel.h>
#include <qsplitter.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "syncview.ui.h"
static const char* const image0_data[] = { 
"22 20 208 2",
"Qt c None",
".s c #046899",
".t c #0d6f9c",
".u c #15759f",
".q c #18569a",
".r c #1969a2",
".v c #1e7ca0",
".p c #2253a4",
".w c #25809b",
".y c #267a96",
".x c #278199",
"aX c #3a549f",
"a0 c #3c57a8",
"aZ c #3e57ab",
".z c #3e8aac",
"aY c #3f58a8",
"a2 c #415da6",
"a3 c #4260a0",
"bm c #43569a",
"bl c #435794",
"aW c #455e9c",
"bn c #465479",
"aV c #465c9b",
"aU c #465d99",
"a1 c #4863b2",
"bi c #4964a7",
"a4 c #496fb0",
"be c #4970b3",
"aT c #4a5a86",
".o c #4a67a9",
"#A c #4b6db4",
"a5 c #4b7cc9",
"bj c #4c69b9",
"bg c #4c6cb9",
"bk c #4d5d9b",
"b# c #4e6d9c",
"bc c #4e70b7",
"bh c #5066a0",
".A c #5395c5",
"ag c #5577b2",
"bd c #566aa5",
"af c #576aa4",
"a9 c #5778c5",
"#X c #587bb3",
"b. c #596fab",
"aR c #597ec2",
"#B c #5b7eb2",
"bf c #5c89de",
"ay c #5d7bc1",
"#Z c #5e95de",
"aP c #5e99dd",
"au c #5e9bde",
"az c #5f73a8",
"aS c #5f76a8",
".Z c #5f7fc2",
"aO c #5f9fdf",
"aA c #6080af",
"a7 c #6095e1",
"aj c #6297e1",
"a6 c #629bea",
"#0 c #629de6",
"aN c #629fde",
"av c #62a1e4",
"#h c #637dbb",
"ap c #63a9e7",
"ae c #6487c9",
"ah c #6493e1",
"a8 c #6494de",
"#1 c #649ee7",
"ab c #64a4e1",
"#Y c #6597df",
"#W c #6676b4",
"aw c #669ce2",
"#T c #669edb",
"ac c #66a2de",
"aa c #66a6e6",
"ao c #66ace8",
"#V c #6782d1",
"aQ c #6795e2",
".0 c #6795e6",
"#C c #6798d9",
"#D c #689bda",
"#R c #68a3e5",
"bb c #6991d9",
"ai c #699bea",
"#U c #6a97da",
"ak c #6a97e8",
"#2 c #6aa3e8",
"aM c #6aa5e1",
"#S c #6aa5e7",
"ba c #6b94d6",
"am c #6b9edd",
"at c #6ba2e5",
"aB c #6c99d5",
"#E c #6ca4de",
"a# c #6ca7eb",
"#6 c #6caee8",
"ad c #6d9ddf",
"#F c #6da9e8",
"#7 c #6dadea",
"#k c #6ea0db",
"#5 c #6eafe9",
"#j c #6f9cde",
"al c #6f9de8",
"#3 c #6fa6e6",
"#4 c #6faae6",
"#G c #6fabe7",
".G c #708aae",
"ax c #709fe3",
".1 c #70a6e8",
"#w c #70a7e7",
"#9 c #71a6e8",
"ar c #71a6ea",
"#l c #71a7e7",
"aq c #71aaef",
"#x c #71ade9",
"#H c #72afe5",
"an c #72afe6",
"#I c #72b1e4",
"#g c #739cba",
"#Q c #73aaea",
".2 c #73ade6",
"#i c #749ae7",
"as c #74a6eb",
"#z c #74a7de",
"#8 c #74b0ee",
"#v c #75a9e3",
"#J c #75b4e9",
"a. c #76abef",
"#P c #76acea",
"#N c #76b1eb",
"#K c #77b8ee",
".# c #788da8",
".f c #789ec2",
"#O c #78b0e9",
"aC c #79a7e3",
"aD c #79ace1",
"#M c #79b6ef",
"#L c #79b7f0",
"#m c #7aaeea",
"aL c #7bb2e8",
"aE c #7cabdf",
".4 c #7cb2ee",
".3 c #7cb6f5",
".b c #7d9ac2",
"aH c #7db6e1",
"aI c #7dbce8",
".c c #7e9bc3",
"#n c #7fb2eb",
"#c c #7fb4ea",
"#y c #7fb9eb",
"#a c #7fbae2",
"aJ c #7fbdec",
"#s c #80b7f0",
"#b c #81b6ea",
"#r c #81b9f2",
"#u c #82b3eb",
"aG c #82b7e3",
"aK c #82b7eb",
"aF c #83b1e3",
"#t c #83b6eb",
"#d c #84b8e8",
"#o c #84b9ed",
".7 c #84bdea",
".8 c #84c1ed",
".a c #859fc2",
".5 c #85baee",
".V c #85bbea",
"#p c #85bcf2",
"#q c #86bff6",
".9 c #86c3f2",
".U c #88beec",
".F c #899db8",
".Y c #89a1bd",
".6 c #89bfee",
"#. c #89c8f3",
".C c #8ac3ee",
".n c #8b9ebc",
"## c #8bc6ee",
".j c #8eb0cc",
"#e c #8fc2ed",
".e c #90a1bf",
".i c #90a6be",
"#f c #96c4e8",
".H c #97b7e3",
".W c #99c5ea",
".g c #9ac1ec",
".B c #9cd8ff",
".D c #9dcdf3",
".d c #a2c2f1",
".k c #a3cef1",
".l c #a7d0f0",
".X c #a8cbe9",
".E c #a9ccec",
".T c #a9d7f8",
".m c #aacde9",
".h c #adcaea",
".S c #aed3ee",
".I c #b1cef1",
".J c #b2cce7",
".K c #bddaf8",
".L c #c3e0fe",
".R c #c4dbfa",
".M c #c6e0fb",
".Q c #cddcfb",
".N c #cde1fa",
".O c #d0dbf7",
".P c #d4ddfa",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQt.#QtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQt.a.bQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQt.c.d.eQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQt.f.g.h.iQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQt.j.k.l.m.nQtQtQtQtQt",
".o.p.q.r.s.t.u.v.w.x.y.z.A.B.C.D.E.FQtQtQtQt",
".G.H.I.J.K.L.M.N.O.P.Q.R.S.T.U.V.W.X.YQtQtQt",
".Z.0.1.2.3.4.5.6.7.8.9#.###a#b#c#d#e#f#gQtQt",
"#h#i#j#k#l#m#n#o#p#q#r#s.5#t#u#v#w#x#y#z#AQt",
"#B#C#D#E#F#G#H#I#J#K#L#M#N#O#P#Q#R#S#T#U#V#W",
"#X#Y#Z#0#1#2#3#4#5#6#7#8#9a.a#aaabacadaeafQt",
"agahaiajakalamanaoapaqarasatauavawaxayazQtQt",
"aAaBaCaDaEaFaGaHaIaJaKaLaMaNaOaPaQaRaSQtQtQt",
"aTaUaVaWaXaYaZa0a1a2a3a4a5a6a7a8a9b.QtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtb#babbbcbdQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtbebfbgbhQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtbibjbkQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtblbmQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtbnQtQtQtQtQtQtQtQtQt"};

static const char* const image1_data[] = { 
"22 20 208 2",
"Qt c None",
".B c #046899",
".A c #0d6f9c",
".z c #15759f",
".D c #18569a",
".C c #1969a2",
".y c #1e7ca0",
".E c #2253a4",
".x c #25809b",
".v c #267a96",
".w c #278199",
"a6 c #3a549f",
"a3 c #3c57a8",
"a4 c #3e57ab",
".u c #3e8aac",
"a5 c #3f58a8",
"a1 c #415da6",
"a0 c #4260a0",
"bl c #43569a",
"bm c #435794",
"a7 c #455e9c",
"bn c #465479",
"a8 c #465c9b",
"a9 c #465d99",
"a2 c #4863b2",
"bk c #4964a7",
"aZ c #496fb0",
"bh c #4970b3",
"b. c #4a5a86",
".F c #4a67a9",
"#h c #4b6db4",
"aY c #4b7cc9",
"bj c #4c69b9",
"bf c #4c6cb9",
"bi c #4d5d9b",
"bd c #4e6d9c",
"ba c #4e70b7",
"be c #5066a0",
".t c #5395c5",
"az c #5577b2",
"b# c #566aa5",
"#X c #576aa4",
"aU c #5778c5",
"af c #587bb3",
"aT c #596fab",
"aB c #597ec2",
"#W c #5b7eb2",
"bg c #5c89de",
"ah c #5d7bc1",
"ad c #5e95de",
"aD c #5e99dd",
"al c #5e9bde",
"ag c #5f73a8",
"aA c #5f76a8",
"#g c #5f7fc2",
"aE c #5f9fdf",
"aS c #6080af",
"aW c #6095e1",
"aw c #6297e1",
"aX c #629bea",
"ac c #629de6",
"aF c #629fde",
"ak c #62a1e4",
"#A c #637dbb",
"aq c #63a9e7",
"#Y c #6487c9",
"ay c #6493e1",
"aV c #6494de",
"ab c #649ee7",
"#1 c #64a4e1",
"ae c #6597df",
"#B c #6676b4",
"aj c #669ce2",
"#E c #669edb",
"#0 c #66a2de",
"#2 c #66a6e6",
"ar c #66ace8",
"#C c #6782d1",
"aC c #6795e2",
"#f c #6795e6",
"#V c #6798d9",
"#U c #689bda",
"#G c #68a3e5",
"bb c #6991d9",
"ax c #699bea",
"#D c #6a97da",
"av c #6a97e8",
"aa c #6aa3e8",
"aG c #6aa5e1",
"#F c #6aa5e7",
"bc c #6b94d6",
"at c #6b9edd",
"am c #6ba2e5",
"aR c #6c99d5",
"#T c #6ca4de",
"#3 c #6ca7eb",
"#8 c #6caee8",
"#Z c #6d9ddf",
"#S c #6da9e8",
"#7 c #6dadea",
"#x c #6ea0db",
"#9 c #6eafe9",
"#y c #6f9cde",
"au c #6f9de8",
"a# c #6fa6e6",
"a. c #6faae6",
"#R c #6fabe7",
".Y c #708aae",
"ai c #709fe3",
"#e c #70a6e8",
"#l c #70a7e7",
"#5 c #71a6e8",
"ao c #71a6ea",
"#w c #71a7e7",
"ap c #71aaef",
"#k c #71ade9",
"#Q c #72afe5",
"as c #72afe6",
"#P c #72b1e4",
".Z c #739cba",
"#H c #73aaea",
"#d c #73ade6",
"#z c #749ae7",
"an c #74a6eb",
"#i c #74a7de",
"#6 c #74b0ee",
"#m c #75a9e3",
"#O c #75b4e9",
"#4 c #76abef",
"#I c #76acea",
"#K c #76b1eb",
"#N c #77b8ee",
".# c #788da8",
".i c #789ec2",
"#J c #78b0e9",
"aQ c #79a7e3",
"aP c #79ace1",
"#L c #79b6ef",
"#M c #79b7f0",
"#v c #7aaeea",
"aH c #7bb2e8",
"aO c #7cabdf",
"#b c #7cb2ee",
"#c c #7cb6f5",
".a c #7d9ac2",
"aL c #7db6e1",
"aK c #7dbce8",
".e c #7e9bc3",
"#u c #7fb2eb",
".3 c #7fb4ea",
"#j c #7fb9eb",
".5 c #7fbae2",
"aJ c #7fbdec",
"#p c #80b7f0",
".4 c #81b6ea",
"#q c #81b9f2",
"#n c #82b3eb",
"aM c #82b7e3",
"aI c #82b7eb",
"aN c #83b1e3",
"#o c #83b6eb",
".2 c #84b8e8",
"#t c #84b9ed",
"#. c #84bdea",
".9 c #84c1ed",
".b c #859fc2",
"#a c #85baee",
".J c #85bbea",
"#s c #85bcf2",
"#r c #86bff6",
".8 c #86c3f2",
".K c #88beec",
".o c #899db8",
".G c #89a1bd",
"## c #89bfee",
".7 c #89c8f3",
".r c #8ac3ee",
".j c #8b9ebc",
".6 c #8bc6ee",
".n c #8eb0cc",
".1 c #8fc2ed",
".c c #90a1bf",
".f c #90a6be",
".0 c #96c4e8",
".X c #97b7e3",
".I c #99c5ea",
".h c #9ac1ec",
".s c #9cd8ff",
".q c #9dcdf3",
".d c #a2c2f1",
".m c #a3cef1",
".l c #a7d0f0",
".H c #a8cbe9",
".p c #a9ccec",
".L c #a9d7f8",
".k c #aacde9",
".g c #adcaea",
".M c #aed3ee",
".W c #b1cef1",
".V c #b2cce7",
".U c #bddaf8",
".T c #c3e0fe",
".N c #c4dbfa",
".S c #c6e0fb",
".O c #cddcfb",
".R c #cde1fa",
".Q c #d0dbf7",
".P c #d4ddfa",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQt.#QtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQt.a.bQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQt.c.d.eQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQt.f.g.h.iQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQt.j.k.l.m.nQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQt.o.p.q.r.s.t.u.v.w.x.y.z.A.B.C.D.E.F",
"QtQtQt.G.H.I.J.K.L.M.N.O.P.Q.R.S.T.U.V.W.X.Y",
"QtQt.Z.0.1.2.3.4.5.6.7.8.9#.###a#b#c#d#e#f#g",
"Qt#h#i#j#k#l#m#n#o#a#p#q#r#s#t#u#v#w#x#y#z#A",
"#B#C#D#E#F#G#H#I#J#K#L#M#N#O#P#Q#R#S#T#U#V#W",
"Qt#X#Y#Z#0#1#2#3#4#5#6#7#8#9a.a#aaabacadaeaf",
"QtQtagahaiajakalamanaoapaqarasatauavawaxayaz",
"QtQtQtaAaBaCaDaEaFaGaHaIaJaKaLaMaNaOaPaQaRaS",
"QtQtQtQtaTaUaVaWaXaYaZa0a1a2a3a4a5a6a7a8a9b.",
"QtQtQtQtQtb#babbbcbdQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtbebfbgbhQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtbibjbkQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtblbmQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtbnQtQtQtQtQtQtQtQtQtQtQtQt"};


/* 
 *  Constructs a SyncView as a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 */
SyncView::SyncView( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl ),
      image0( (const char **) image0_data ),
      image1( (const char **) image1_data )
{
    if ( !name )
	setName( "SyncView" );
    SyncViewLayout = new QGridLayout( this, 1, 1, 11, 6, "SyncViewLayout"); 
    QSpacerItem* spacer = new QSpacerItem( 298, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    SyncViewLayout->addMultiCell( spacer, 2, 2, 1, 2 );
    QSpacerItem* spacer_2 = new QSpacerItem( 298, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    SyncViewLayout->addMultiCell( spacer_2, 2, 2, 4, 5 );

    syncRightButton = new KPushButton( this, "syncRightButton" );
    syncRightButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, syncRightButton->sizePolicy().hasHeightForWidth() ) );
    syncRightButton->setMinimumSize( QSize( 50, 30 ) );
    syncRightButton->setPixmap( image0 );

    SyncViewLayout->addWidget( syncRightButton, 0, 2 );

    readKABButton = new KPushButton( this, "readKABButton" );
    readKABButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, readKABButton->sizePolicy().hasHeightForWidth() ) );
    readKABButton->setMaximumSize( QSize( 32767, 30 ) );

    SyncViewLayout->addWidget( readKABButton, 2, 0 );

    syncButton = new KPushButton( this, "syncButton" );
    syncButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, syncButton->sizePolicy().hasHeightForWidth() ) );
    syncButton->setMaximumSize( QSize( 32767, 30 ) );

    SyncViewLayout->addWidget( syncButton, 2, 3 );

    readMobButton = new KPushButton( this, "readMobButton" );
    readMobButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, readMobButton->sizePolicy().hasHeightForWidth() ) );
    readMobButton->setMaximumSize( QSize( 32767, 30 ) );

    SyncViewLayout->addWidget( readMobButton, 2, 6 );

    syncLeftButton = new KPushButton( this, "syncLeftButton" );
    syncLeftButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, syncLeftButton->sizePolicy().hasHeightForWidth() ) );
    syncLeftButton->setMinimumSize( QSize( 50, 30 ) );
    syncLeftButton->setPixmap( image1 );

    SyncViewLayout->addWidget( syncLeftButton, 0, 4 );

    splitter2 = new QSplitter( this, "splitter2" );
    splitter2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, splitter2->sizePolicy().hasHeightForWidth() ) );
    splitter2->setOrientation( QSplitter::Horizontal );

    KABListView = new KListView( splitter2, "KABListView" );
    KABListView->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, KABListView->sizePolicy().hasHeightForWidth() ) );
    KABListView->setMinimumSize( QSize( 0, 0 ) );
    KABListView->setAllColumnsShowFocus( TRUE );
    KABListView->setShowSortIndicator( TRUE );

    mobListView = new KListView( splitter2, "mobListView" );
    mobListView->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, mobListView->sizePolicy().hasHeightForWidth() ) );
    mobListView->setMinimumSize( QSize( 0, 0 ) );
    mobListView->setAllColumnsShowFocus( TRUE );
    mobListView->setShowSortIndicator( TRUE );

    SyncViewLayout->addMultiCellWidget( splitter2, 1, 1, 0, 6 );

    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setFrameShape( QLabel::NoFrame );
    textLabel1->setFrameShadow( QLabel::Plain );
    textLabel1->setAlignment( int( QLabel::WordBreak | QLabel::AlignCenter ) );

    SyncViewLayout->addMultiCellWidget( textLabel1, 0, 0, 0, 1 );

    textLabel1_2 = new QLabel( this, "textLabel1_2" );
    textLabel1_2->setFrameShape( QLabel::NoFrame );
    textLabel1_2->setFrameShadow( QLabel::Plain );
    textLabel1_2->setAlignment( int( QLabel::WordBreak | QLabel::AlignCenter ) );

    SyncViewLayout->addMultiCellWidget( textLabel1_2, 0, 0, 5, 6 );
    languageChange();
    resize( QSize(811, 584).expandedTo(minimumSizeHint()) );

    // signals and slots connections
    connect( syncButton, SIGNAL( clicked() ), this, SLOT( sync() ) );
    connect( syncRightButton, SIGNAL( clicked() ), this, SLOT( syncRight() ) );
    connect( syncLeftButton, SIGNAL( clicked() ), this, SLOT( syncLeft() ) );
    connect( readKABButton, SIGNAL( clicked() ), this, SLOT( readKABClicked() ) );
    connect( readMobButton, SIGNAL( clicked() ), this, SLOT( readMobClicked() ) );
    connect( mobListView, SIGNAL( rightButtonClicked(QListViewItem*,const QPoint&,int) ), this, SLOT( phoneViewRightClicked(QListViewItem*,const QPoint&,int) ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
SyncView::~SyncView()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void SyncView::languageChange()
{
    setCaption( tr( "KnokiiSync" ) );
    syncRightButton->setText( QString::null );
    QToolTip::add( syncRightButton, tr( "Transfer Entries" ) );
    readKABButton->setText( tr( "Read" ) );
    syncButton->setText( tr( "Sync!" ) );
    readMobButton->setText( tr( "Read" ) );
    syncLeftButton->setText( QString::null );
    QToolTip::add( syncLeftButton, tr( "Transfer Entries" ) );
    textLabel1->setText( tr( "KDE Address Book" ) );
    textLabel1_2->setText( tr( "Mobile Phone Address Book" ) );
}

