#include <kabc/addressbook.h>

class OverridenAB : public KABC::AddressBook
{
	public:
		bool saveAddrBook ();
		void setIMEI (QString IMEI);

		OverridenAB ();
		OverridenAB (QString IMEI);
		~OverridenAB ();
};
