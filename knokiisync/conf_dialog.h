 /****************************************************************************
** Form interface generated from reading ui file 'conf_dialog.ui'
**
** Created: Κυρ Αύγ 17 16:53:27 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef CFGDLG_H
#define CFGDLG_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QButtonGroup;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QRadioButton;
class QTabWidget;
class QWidget;

class cfgDlg : public QDialog
{
    Q_OBJECT

public:
    cfgDlg( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~cfgDlg();

    QPushButton* OKButton;
    QPushButton* cancelButton;
    QTabWidget* tabWidget;
    QWidget* generalPage;
    QCheckBox* transferPhone2KabChkBox;
    QCheckBox* transferKab2PhoneChkBox;
    QLabel* textLabel1_5;
    QLabel* textLabel1_4;
    QComboBox* encComboBox;
    QComboBox* memComboBox;
    QLabel* textLabel1;
    QGroupBox* groupBox1;
    QLabel* textLabel1_3;
    QLineEdit* preferredOrderLE;
    QLabel* textLabel2_2;
    QCheckBox* dontDisplayProblematicCB;
    QWidget* startupPage;
    QCheckBox* readPhoneBookChkBox;
    QCheckBox* connPhoneChkBox;
    QCheckBox* readKabChkBox;
    QWidget* confirmationPage;
    QCheckBox* overwriteKab2PhoneChkBox;
    QCheckBox* overwritePhone2PKabChkBox;
    QWidget* syncPage;
    QButtonGroup* buttonGroup1;
    QRadioButton* syncAskChkBox;
    QRadioButton* syncPhoneRulesChkBox;
    QRadioButton* syncKabRulesChkBox;
    QWidget* tab;
    QCheckBox* setFmtNameInKabCB;
    QLabel* textLabel2;
    QLineEdit* separatorLE;
    QButtonGroup* buttonGroup6;
    QRadioButton* W1NameRB;
    QRadioButton* W1SurnameRB;
    QRadioButton* W1AskRB;
    QRadioButton* W1IgnoreRB;
    QButtonGroup* buttonGroup2_2;
    QRadioButton* W3AskRB;
    QRadioButton* W3IgnoreRB;
    QRadioButton* W3NameAddSurnameRB;
    QRadioButton* W3SurnameAddNameRB;
    QButtonGroup* buttonGroup2_3;
    QRadioButton* W3PAskRB;
    QRadioButton* W3PIgnoreRB;
    QButtonGroup* buttonGroup2;
    QRadioButton* W2AskRB;
    QRadioButton* W2IgnoreRB;
    QRadioButton* W2SurnameNameRB;
    QRadioButton* W2NameSurnameRB;
    QWidget* tab_2;
    QButtonGroup* buttonGroup5;
    QRadioButton* pAddressFormatNotifyRB;
    QRadioButton* pAddressFormatIgnoreRB;
    QButtonGroup* buttonGroup3;
    QLineEdit* pAddrFrmtLEdit_2;
    QLabel* textLabel2_4;
    QLabel* textLabel1_2_3_2;
    QLabel* textLabel1_2_4_2;
    QLabel* textLabel1_2_6;
    QLabel* textLabel1_2_5_2;
    QLabel* textLabel1_2_2_2;
    QLineEdit* pAddrFrmtLEdit;
    QWidget* tab_3;
    QGroupBox* groupBox2;
    QLabel* textLabel2_3;
    QLineEdit* groupName1LE;
    QLineEdit* groupName2LE;
    QLineEdit* groupName3LE;
    QLineEdit* groupName4LE;
    QLineEdit* groupName5LE;
    QLabel* textLabel2_3_2;
    QLabel* textLabel2_3_3;
    QLabel* textLabel2_3_4;
    QLabel* textLabel2_3_5;
    QLabel* textLabel2_3_6;
    QLineEdit* groupName0LE;

public slots:
    void OKClicked();
    virtual void cancelClicked();
    void pAddrLEChanged( const QString & newText );
    void separatorLEChanged( const QString & newText );
    void separatorLELostFocus();

signals:
    void confChanged();

protected:
    QGridLayout* cfgDlgLayout;
    QGridLayout* generalPageLayout;
    QGridLayout* groupBox1Layout;
    QGridLayout* syncPageLayout;
    QGridLayout* buttonGroup1Layout;
    QGridLayout* tabLayout;
    QGridLayout* buttonGroup6Layout;
    QGridLayout* buttonGroup2_2Layout;
    QGridLayout* buttonGroup2_3Layout;
    QGridLayout* buttonGroup2Layout;
    QGridLayout* tabLayout_2;
    QGridLayout* buttonGroup5Layout;
    QGridLayout* buttonGroup3Layout;
    QGridLayout* tabLayout_3;
    QGridLayout* groupBox2Layout;

protected slots:
    virtual void languageChange();
private:
    void init();
    void destroy();

};

#endif // CFGDLG_H
