/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <kapp.h>
#include <kdebug.h>
#include <klocale.h>
#include <kconfig.h>
#include <kstandarddirs.h>
#include <kabc/phonenumber.h>
#include <kmessagebox.h>

extern KApplication * globalKApp;

void
AddresseeDlg::OKClicked ()
{
	if (frmtdNameLE->text () == "")
		{
			int reply = KMessageBox::warningYesNo (0, i18n ("You have not set a formatted name for this contact. If you do not do so, you will not be able to upload this contact to the phone. Do you wish to go back and set a formatted name?"));
			if (reply == KMessageBox::Yes)
				return;
		}

	hide ();
	emit OKButtonClicked ();
}

void AddresseeDlg::cancelClicked ()
{
	hide ();
}

void
AddresseeDlg::init ()
{
	QPixmap qpm;
	QString tmp;
	KConfig * knsync_cfg=globalKApp->config ();

	knsync_cfg->setGroup("Caller Groups");
	tmp = knsync_cfg->readEntry ("Group0Name");
	if (tmp == "")
		tmp = i18n ("Family");
	groupsCB->insertItem (tmp);

	tmp = knsync_cfg->readEntry ("Group1Name");
	if (tmp == "")
		tmp = i18n ("VIP");
	groupsCB->insertItem (tmp);

	tmp = knsync_cfg->readEntry ("Group2Name");
	if (tmp == "")
		tmp = i18n ("Friends");
	groupsCB->insertItem (tmp);

	tmp = knsync_cfg->readEntry ("Group3Name");
	if (tmp == "")
		tmp = i18n ("Colleagues");
	groupsCB->insertItem (tmp);

	tmp = knsync_cfg->readEntry ("Group4Name");
	if (tmp == "")
		tmp = i18n ("Other");
	groupsCB->insertItem (tmp);

	tmp = knsync_cfg->readEntry ("Group5Name");
	if (tmp == "")
		tmp = i18n ("No group");
	groupsCB->insertItem (tmp);

	qpm = *(phoneTypeCB1->pixmap (0));
	phoneTypeCB1->changeItem (qpm, i18n ("General Number"), 0);
	qpm = *(phoneTypeCB1->pixmap (1));
	phoneTypeCB1->changeItem (qpm, i18n ("Mobile Number"), 1);
	qpm = *(phoneTypeCB1->pixmap (2));
	phoneTypeCB1->changeItem (qpm, i18n ("Home Number"), 2);
	qpm = *(phoneTypeCB1->pixmap (3));
	phoneTypeCB1->changeItem (qpm, i18n ("Business Number"), 3);
	qpm = *(phoneTypeCB1->pixmap (4));
	phoneTypeCB1->changeItem (qpm, i18n ("Fax Number"), 4);

	qpm = *(phoneTypeCB2->pixmap (0));
	phoneTypeCB2->changeItem (qpm, i18n ("General Number"), 0);
	qpm = *(phoneTypeCB2->pixmap (1));
	phoneTypeCB2->changeItem (qpm, i18n ("Mobile Number"), 1);
	qpm = *(phoneTypeCB2->pixmap (2));
	phoneTypeCB2->changeItem (qpm, i18n ("Home Number"), 2);
	qpm = *(phoneTypeCB2->pixmap (3));
	phoneTypeCB2->changeItem (qpm, i18n ("Business Number"), 3);
	qpm = *(phoneTypeCB2->pixmap (4));
	phoneTypeCB2->changeItem (qpm, i18n ("Fax Number"), 4);

	qpm = *(phoneTypeCB3->pixmap (0));
	phoneTypeCB3->changeItem (qpm, i18n ("General Number"), 0);
	qpm = *(phoneTypeCB3->pixmap (1));
	phoneTypeCB3->changeItem (qpm, i18n ("Mobile Number"), 1);
	qpm = *(phoneTypeCB3->pixmap (2));
	phoneTypeCB3->changeItem (qpm, i18n ("Home Number"), 2);
	qpm = *(phoneTypeCB3->pixmap (3));
	phoneTypeCB3->changeItem (qpm, i18n ("Business Number"), 3);
	qpm = *(phoneTypeCB3->pixmap (4));
	phoneTypeCB3->changeItem (qpm, i18n ("Fax Number"), 4);

	qpm = *(phoneTypeCB4->pixmap (0));
	phoneTypeCB4->changeItem (qpm, i18n ("General Number"), 0);
	qpm = *(phoneTypeCB4->pixmap (1));
	phoneTypeCB4->changeItem (qpm, i18n ("Mobile Number"), 1);
	qpm = *(phoneTypeCB4->pixmap (2));
	phoneTypeCB4->changeItem (qpm, i18n ("Home Number"), 2);
	qpm = *(phoneTypeCB4->pixmap (3));
	phoneTypeCB4->changeItem (qpm, i18n ("Business Number"), 3);
	qpm = *(phoneTypeCB4->pixmap (4));
	phoneTypeCB4->changeItem (qpm, i18n ("Fax Number"), 4);

	qpm = *(phoneTypeCB5->pixmap (0));
	phoneTypeCB5->changeItem (qpm, i18n ("General Number"), 0);
	qpm = *(phoneTypeCB5->pixmap (1));
	phoneTypeCB5->changeItem (qpm, i18n ("Mobile Number"), 1);
	qpm = *(phoneTypeCB5->pixmap (2));
	phoneTypeCB5->changeItem (qpm, i18n ("Home Number"), 2);
	qpm = *(phoneTypeCB5->pixmap (3));
	phoneTypeCB5->changeItem (qpm, i18n ("Business Number"), 3);
	qpm = *(phoneTypeCB5->pixmap (4));
	phoneTypeCB5->changeItem (qpm, i18n ("Fax Number"), 4);

	prefPhoneQRBList.append (prefPhoneRB1);
	prefPhoneQRBList.append (prefPhoneRB2);
	prefPhoneQRBList.append (prefPhoneRB3);
	prefPhoneQRBList.append (prefPhoneRB4);
	prefPhoneQRBList.append (prefPhoneRB5);

	phoneQCBList.append (phoneTypeCB1);
	phoneQCBList.append (phoneTypeCB2);
	phoneQCBList.append (phoneTypeCB3);
	phoneQCBList.append (phoneTypeCB4);
	phoneQCBList.append (phoneTypeCB5);

	phoneQLEList.append (phoneLE1);
	phoneQLEList.append (phoneLE2);
	phoneQLEList.append (phoneLE3);
	phoneQLEList.append (phoneLE4);
	phoneQLEList.append (phoneLE5);

	emailQLEList.append (emailLE1);
	emailQLEList.append (emailLE2);
	emailQLEList.append (emailLE3);
	emailQLEList.append (emailLE4);

	urlQLEList.append (urlLE1);
	urlQLEList.append (urlLE2);
	urlQLEList.append (urlLE3);
	urlQLEList.append (urlLE4);

	streetQLEList.append (streetLE1);
	streetQLEList.append (streetLE2);
	streetQLEList.append (streetLE3);
	streetQLEList.append (streetLE4);

	localityQLEList.append (localityLE1);
	localityQLEList.append (localityLE2);
	localityQLEList.append (localityLE3);
	localityQLEList.append (localityLE4);

	regionQLEList.append (regionLE1);
	regionQLEList.append (regionLE2);
	regionQLEList.append (regionLE3);
	regionQLEList.append (regionLE4);

	postalCodeQLEList.append (postalCodeLE1);
	postalCodeQLEList.append (postalCodeLE2);
	postalCodeQLEList.append (postalCodeLE3);
	postalCodeQLEList.append (postalCodeLE4);

	countryQLEList.append (countryLE1);
	countryQLEList.append (countryLE2);
	countryQLEList.append (countryLE3);
	countryQLEList.append (countryLE4);

	notesQLEList.append (noteLE1);
	notesQLEList.append (noteLE2);
	notesQLEList.append (noteLE3);
	notesQLEList.append (noteLE4);

	phoneTypeCB1->setEditable (FALSE);
	phoneTypeCB2->setEditable (FALSE);
	phoneTypeCB3->setEditable (FALSE);
	phoneTypeCB4->setEditable (FALSE);
	phoneTypeCB5->setEditable (FALSE);

	prefPhoneRB1->setEnabled (FALSE);
	prefPhoneRB2->setEnabled (FALSE);
	prefPhoneRB3->setEnabled (FALSE);
	prefPhoneRB4->setEnabled (FALSE);
	prefPhoneRB5->setEnabled (FALSE);

	phoneLE1->setReadOnly (TRUE);
	phoneLE2->setReadOnly (TRUE);
	phoneLE3->setReadOnly (TRUE);
	phoneLE4->setReadOnly (TRUE);
	phoneLE5->setReadOnly (TRUE);

	emailLE1->setReadOnly (TRUE);
	emailLE2->setReadOnly (TRUE);
	emailLE3->setReadOnly (TRUE);
	emailLE4->setReadOnly (TRUE);

	urlLE1->setReadOnly (TRUE);
	urlLE2->setReadOnly (TRUE);
	urlLE3->setReadOnly (TRUE);
	urlLE4->setReadOnly (TRUE);

	streetLE1->setReadOnly (TRUE);
	localityLE1->setReadOnly (TRUE);
	regionLE1->setReadOnly (TRUE);
	postalCodeLE1->setReadOnly (TRUE);
	countryLE1->setReadOnly (TRUE);

	streetLE2->setReadOnly (TRUE);
	localityLE2->setReadOnly (TRUE);
	regionLE2->setReadOnly (TRUE);
	postalCodeLE2->setReadOnly (TRUE);
	countryLE2->setReadOnly (TRUE);

	streetLE3->setReadOnly (TRUE);
	localityLE3->setReadOnly (TRUE);
	regionLE3->setReadOnly (TRUE);
	postalCodeLE3->setReadOnly (TRUE);
	countryLE3->setReadOnly (TRUE);

	streetLE4->setReadOnly (TRUE);
	localityLE4->setReadOnly (TRUE);
	regionLE4->setReadOnly (TRUE);
	postalCodeLE4->setReadOnly (TRUE);
	countryLE4->setReadOnly (TRUE);

	noteLE1->setReadOnly (TRUE);
	noteLE2->setReadOnly (TRUE);
	noteLE3->setReadOnly (TRUE);
	noteLE4->setReadOnly (TRUE);

	mAddressee = 0;
}

void
AddresseeDlg::setAddressee (KABC::Addressee * addressee)
{
	mAddressee = addressee;
}

KABC::Addressee *
AddresseeDlg::getAddressee ()
{
	int namesCount = 0, group_id = 0;
	QString tmp1, tmp2;
	bool modified = 0;

	QString formattedName = frmtdNameLE->text ();
	if (formattedName != "")
		mAddressee->setFormattedName (frmtdNameLE->text ());

	group_id = groupsCB->currentItem ();
	mAddressee->insertCustom ("KnokiiSync", "GROUP_ID", QString::number (group_id));

	tmp1 = nameLE->text ();
	tmp2 = mAddressee->givenName ();
	if (tmp1 != tmp2 && tmp1 != "")
		{
			mAddressee->setGivenName (tmp1);
			mAddressee->insertCustom ("KnokiiSync", "E_NAME_OV", "1");
			modified = 1;
		}

	tmp1 = surnameLE->text ();
	tmp2 = mAddressee->familyName ();
	if (tmp1 != tmp2 && tmp1 != "")
		{
			mAddressee->setFamilyName (tmp1);
			mAddressee->insertCustom ("KnokiiSync", "E_SURNAME_OV", "1");
			modified = 1;
		}

	tmp1 = addNameLE->text ();
	tmp2 = mAddressee->additionalName ();
	if (tmp1 != tmp2 && tmp1 != "")
		{
			mAddressee->setAdditionalName (tmp1);
			mAddressee->insertCustom ("KnokiiSync", "E_ADDNAME_OV", "1");
			modified = 1;
		}

	if (modified)
		mAddressee->insertCustom ("KnokiiSync", "E_OVERRIDEN", "1");

	if (mAddressee->givenName () != "")
		namesCount++;

	if (mAddressee->familyName () != "")
		namesCount++;

	if (mAddressee->additionalName () != "")
		namesCount++;

	switch (namesCount)
		{
			case 1: mAddressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W1");
							break;
			case 2: mAddressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W2");
							break;
			case 3: mAddressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W3");
							break;
			default: mAddressee->insertCustom ("KnokiiSync", "E_CLASS", "E_W3P");
							break;
		}

	return mAddressee;
}

void
AddresseeDlg::displayAddressee ()
{
	typedef QValueListIterator<KABC::PhoneNumber> PhoneIterator;
	typedef QValueListIterator<KABC::Address> AddressIterator;
	QComboBox * phoneQCBIt = 0;
	QLineEdit * phoneQLEIt = 0;
	QLineEdit * emailQLEIt = 0;
	QLineEdit * urlQLEIt = 0;
	QLineEdit * notesQLEIt = 0;
	QLineEdit * streetQLEIt = 0;
	QLineEdit * localityQLEIt = 0;
	QLineEdit * regionQLEIt = 0;
	QLineEdit * postalCodeQLEIt = 0;
	QLineEdit * countryQLEIt = 0;

	QRadioButton * prefPhoneQRBIt = 0;
	PhoneIterator pI = 0;

	nameInPhoneLE->setText (mAddressee->custom ("KnokiiSync", "E_NAMEINPHONE"));
	nameLE->setText (mAddressee->givenName ());
	surnameLE->setText (mAddressee->familyName ());
	addNameLE->setText (mAddressee->additionalName ());
	frmtdNameLE->setText (mAddressee->formattedName ());

	int group_id = 0;
	QString tmp = mAddressee->custom ("KnokiiSync", "GROUP_ID");
	group_id = tmp.toInt ();

	groupsCB->setCurrentItem (group_id);

	KABC::PhoneNumber::List phoneNumbers = mAddressee->phoneNumbers ();
	phoneQCBIt = phoneQCBList.first ();
	phoneQLEIt = phoneQLEList.first ();
	prefPhoneQRBIt = prefPhoneQRBList.first ();
	pI = phoneNumbers.begin ();

	for (;pI != phoneNumbers.end () && phoneQCBIt;
			++pI, phoneQCBIt = phoneQCBList.next (),
			phoneQLEIt = phoneQLEList.next (), prefPhoneQRBIt = prefPhoneQRBList.next ())
		{
			int type = (*pI).type ();

			if (type & KABC::PhoneNumber::Pref)
				{
					prefPhoneQRBIt->setDown (TRUE);
					type -= KABC::PhoneNumber::Pref;
				}

			if (type & KABC::PhoneNumber::Voice)
				{
					phoneQCBIt->setCurrentItem (0);
					phoneQLEIt->setText ((*pI).number ());
				}

			if (type & KABC::PhoneNumber::Cell)
				{
					phoneQCBIt->setCurrentItem (1);
					phoneQLEIt->setText ((*pI).number ());
				}

			if (type & KABC::PhoneNumber::Home)
				{
					phoneQCBIt->setCurrentItem (2);
					phoneQLEIt->setText ((*pI).number ());
				}

			if (type & KABC::PhoneNumber::Work)
				{
					phoneQCBIt->setCurrentItem (3);
					phoneQLEIt->setText ((*pI).number ());
				}

			if (type & KABC::PhoneNumber::Fax)
				{
					phoneQCBIt->setCurrentItem (4);
					phoneQLEIt->setText ((*pI).number ());
				}
		}

	QStringList emails = mAddressee->emails ();
	QStringList::Iterator emI = emails.begin();
	emailQLEIt = emailQLEList.first ();
	for (; emI != emails.end () && emailQLEIt;
			++emI, emailQLEIt = emailQLEList.next ())
		{
			emailQLEIt->setText (*emI);
		}

	KURL kurl = mAddressee->url ();
	QString url = kurl.prettyURL ();
	QStringList urls;
	if (url != "")
		{
			urls.append (url);
			QString tmpName;
			int tmpCounter = 1;
			do
				{
					tmpName = "E_URL" + QString::number (tmpCounter);
					url = mAddressee->custom ("KnokiiSync", tmpName);
					if (url != "")
						urls.append (url);
					tmpCounter++;
				}
			while (url != "");
		}

	urlQLEIt = urlQLEList.first ();
	for (QStringList::Iterator urI = urls.begin ();
			urI != urls.end () && urlQLEIt; ++urI, urlQLEIt = urlQLEList.next ())
		{
			url = *urI;
			if (url != "")
				{
					urlQLEIt->setText (*urI);
				}
		}

	KABC::Address::List addresses = mAddressee->addresses ();

	streetQLEIt = streetQLEList.first ();
	localityQLEIt = localityQLEList.first ();
	regionQLEIt = regionQLEList.first ();
	postalCodeQLEIt = postalCodeQLEList.first ();
	countryQLEIt = countryQLEList.first ();
	for (AddressIterator aI = addresses.begin ();
			aI != addresses.end () && streetQLEIt; ++aI)
		{
			streetQLEIt->setText ((*aI).street ());
			localityQLEIt->setText ((*aI).locality ());
			regionQLEIt->setText ((*aI).region ());
			postalCodeQLEIt->setText ((*aI).postalCode ());
			countryQLEIt->setText ((*aI).country ());
			streetQLEIt = streetQLEList.next ();
			localityQLEIt = localityQLEList.next ();
			regionQLEIt = regionQLEList.next ();
			postalCodeQLEIt = postalCodeQLEList.next ();
			countryQLEIt = countryQLEList.next ();
		}

	QString note = mAddressee->note ();
	QStringList notes;
	if (note != "")
		{
			notes.append (note);
			QString tmpName;
			int tmpCounter = 1;
			do
				{
					tmpName = "E_NOTE" + QString::number (tmpCounter);
					note = mAddressee->custom ("KnokiiSync", tmpName);
					if (note != "")
						notes.append (note);
					tmpCounter++;
				}
			while (note != "");
		}

	notesQLEIt = notesQLEList.first ();
	for (QStringList::Iterator nI = notes.begin ();
			nI != notes.end () && notesQLEIt; ++nI, notesQLEIt = notesQLEList.next ())
		{
			note = *nI;
			notesQLEIt->setText (note);
		}
}

void
AddresseeDlg::clrOvrDataClicked ()
{
	emit clrOvrButtonClicked ();
}
