/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <kmessagebox.h>
#include <kconfig.h>
#include <kapp.h>
#include <klocale.h>

extern KApplication * globalKApp;

void
cfgDlg::init()
{
	KConfig * knsync_cfg=globalKApp->config ();

	knsync_cfg->setGroup ("General");
	transferKab2PhoneChkBox->setChecked( knsync_cfg->readBoolEntry ("TranferAllKAB2Phone"));
	transferPhone2KabChkBox->setChecked (knsync_cfg->readBoolEntry ("TranferAllPhone2KAB"));
	dontDisplayProblematicCB->setChecked (knsync_cfg->readBoolEntry ("DontDisplayProblematic"));

	QString encoding=knsync_cfg->readEntry ("PhoneEncoding", "ISO8859-1");
	int i=0, i_max = encComboBox->count ();

	for (i=0; encComboBox->text (i)!= encoding && i<i_max; i++){;}

	encComboBox->setCurrentItem (i);
	encComboBox->setEditable (FALSE);

	QString memtype=knsync_cfg->readEntry ("MemoryType", i18n("Phone + SIM"));

	if (memtype != i18n ("Phone + SIM") && memtype != i18n ("Phone") && memtype != i18n ("SIM"))
		memtype = i18n ("Phone + SIM");

	i=0, i_max=memComboBox->count ();

	for (i=0; memComboBox->text (i)!=memtype && i<i_max; i++){;}

	memComboBox->setCurrentItem (i);
	memComboBox->setEditable (FALSE);

	preferredOrderLE->setText (knsync_cfg->readEntry ("PreferredPhoneOrder", "%C %H %B %G %F"));

	knsync_cfg->setGroup ("Startup");
	connPhoneChkBox->setChecked (knsync_cfg->readBoolEntry ("ConnectToPhone"));
	readKabChkBox->setChecked (knsync_cfg->readBoolEntry ("ReadKAB"));
	readPhoneBookChkBox->setChecked (knsync_cfg->readBoolEntry ("ReadPhoneBook"));

	knsync_cfg->setGroup ("Confirmation");
	overwriteKab2PhoneChkBox->setChecked (knsync_cfg->readBoolEntry ("OverwriteKab2Phone"));
	overwritePhone2PKabChkBox->setChecked (knsync_cfg->readBoolEntry ("OverwritePhone2Kab"));    

	knsync_cfg->setGroup ("Sync Conflicts");

	QString conflictAction=knsync_cfg->readEntry ("SyncAction", "Ask");

	if (conflictAction=="KabRules")
		syncKabRulesChkBox->setChecked (TRUE);
	else
		if (conflictAction=="PhoneRules")
			syncPhoneRulesChkBox->setChecked (TRUE);
		else		
			syncAskChkBox->setChecked (TRUE);

	knsync_cfg->setGroup ("Name Format");
	QString sep = knsync_cfg->readEntry ("SeparatorString", " ");
	separatorLE->setText (sep);
	separatorLEChanged (sep);
	separatorLELostFocus ();
	setFmtNameInKabCB->setChecked (knsync_cfg->readBoolEntry ("SetFormattedNameInKAB"));

	QString action = knsync_cfg->readEntry ("W1Action", "Ask");
	if (action == "Ask")
		W1AskRB->setChecked (TRUE);
	else
		if (action == "Name")
			W1NameRB->setChecked (TRUE);
		else
			if (action == "Surname")
				W1SurnameRB->setChecked (TRUE);
			else
				W1IgnoreRB->setChecked (TRUE);

	action = knsync_cfg->readEntry ("W2Action", "Ask");
	if (action == "Ask")
		W2AskRB->setChecked (TRUE);
	else
		if (action == "NameSurname")
			W2NameSurnameRB->setChecked (TRUE);
		else
			if (action == "SurnameName")
				W2SurnameNameRB->setChecked (TRUE);
			else
				W2IgnoreRB->setChecked (TRUE);

	action = knsync_cfg->readEntry ("W3Action", "Ask");
	if (action == "Ask")
		W3AskRB->setChecked (TRUE);
	else
		if (action == "NameAddSurname")
			W3NameAddSurnameRB->setChecked (TRUE);
		else
			if (action == "SurnameAddName")
				W3SurnameAddNameRB->setChecked (TRUE);
			else
				W3IgnoreRB->setChecked (TRUE);

	action = knsync_cfg->readEntry ("W3PAction", "Ask");
	if (action == "Ask")
		W3PAskRB->setChecked (TRUE);
	else
		W3PIgnoreRB->setChecked (TRUE);

	knsync_cfg->setGroup ("Address Format");

	setFmtNameInKabCB->setChecked (knsync_cfg->readBoolEntry ("SetFormattedNameInKAB", FALSE));

	pAddrFrmtLEdit->setText (knsync_cfg->readEntry ("PostalAddressFormat", "%S, %L, %P, %R, %C"));
	QString pAddressFormatMismatch=knsync_cfg->readEntry ("PostalAddressFormatMismatch", "Notify");

	if (pAddressFormatMismatch=="Notify")
		pAddressFormatNotifyRB->setChecked (TRUE);
	else
		if (pAddressFormatMismatch=="Ignore")
			pAddressFormatIgnoreRB->setChecked (TRUE);

	knsync_cfg->setGroup("Caller Groups");

	groupName0LE->setText (knsync_cfg->readEntry ("Group0Name", i18n ("Family")));
	groupName1LE->setText (knsync_cfg->readEntry ("Group1Name", i18n ("VIP")));
	groupName2LE->setText (knsync_cfg->readEntry ("Group2Name", i18n ("Friends")));
	groupName3LE->setText (knsync_cfg->readEntry ("Group3Name", i18n ("Colleagues")));
	groupName4LE->setText (knsync_cfg->readEntry ("Group4Name", i18n ("Other")));
	groupName5LE->setText (knsync_cfg->readEntry ("Group5Name", i18n ("No group")));
}

void cfgDlg::destroy()
{
    
}

void cfgDlg::OKClicked ()
{
	KConfig * knsync_cfg=globalKApp->config ();
	knsync_cfg->setGroup ("General");
	knsync_cfg->writeEntry ("TranferAllKAB2Phone", transferKab2PhoneChkBox->isChecked ());
	knsync_cfg->writeEntry ("TranferAllPhone2KAB", transferPhone2KabChkBox->isChecked ());
	knsync_cfg->writeEntry ("DontDisplayProblematic", dontDisplayProblematicCB->isChecked ());
	knsync_cfg->writeEntry ("PhoneEncoding", encComboBox->currentText ());
	knsync_cfg->writeEntry ("MemoryType", memComboBox->currentText ());
	knsync_cfg->writeEntry ("PreferredPhoneOrder", preferredOrderLE->text ());

	knsync_cfg->setGroup ("Startup");
	knsync_cfg->writeEntry ("ConnectToPhone", connPhoneChkBox->isChecked ());
	knsync_cfg->writeEntry ("ReadKAB", readKabChkBox->isChecked ());
	knsync_cfg->writeEntry ("ReadPhoneBook", readPhoneBookChkBox->isChecked ());

	knsync_cfg->setGroup ("Confirmation");
	knsync_cfg->writeEntry ("OverwriteKab2Phone", overwriteKab2PhoneChkBox->isChecked ());
	knsync_cfg->writeEntry ("OverwritePhone2Kab", overwritePhone2PKabChkBox->isChecked ());

	knsync_cfg->setGroup ("Sync Conflicts");

	if(syncKabRulesChkBox->isChecked ())
		knsync_cfg->writeEntry ("SyncAction", "KabRules");
	else
		if(syncPhoneRulesChkBox->isChecked ())
			knsync_cfg->writeEntry ("SyncAction", "PhoneRules");
		else
			if(syncAskChkBox->isChecked ())
				knsync_cfg->writeEntry ("SyncAction", "Ask");

	knsync_cfg->setGroup ("Name Format");

	knsync_cfg->writeEntry ("SeparatorString", separatorLE->text ());
	knsync_cfg->writeEntry ("SetFormattedNameInKAB", setFmtNameInKabCB->isChecked ());

	if(W1AskRB->isChecked ())
		knsync_cfg->writeEntry ("W1Action", "Ask");
	else
		if(W1NameRB->isChecked ())
			knsync_cfg->writeEntry ("W1Action", "Name");
		else
			if(W1SurnameRB->isChecked ())
				knsync_cfg->writeEntry ("W1Action", "Surname");
			else
				knsync_cfg->writeEntry ("W1Action", "Ignore");

	if(W2AskRB->isChecked ())
		knsync_cfg->writeEntry ("W2Action", "Ask");
	else
		if(W2NameSurnameRB->isChecked ())
			knsync_cfg->writeEntry ("W2Action", "NameSurname");
		else
			if(W2SurnameNameRB->isChecked ())
				knsync_cfg->writeEntry ("W2Action", "SurnameName");
			else
				knsync_cfg->writeEntry ("W2Action", "Ignore");

	if(W3AskRB->isChecked ())
		knsync_cfg->writeEntry ("W3Action", "Ask");
	else
		if(W3NameAddSurnameRB->isChecked ())
			knsync_cfg->writeEntry ("W3Action", "NameAddSurname");
		else
			if(W3SurnameAddNameRB->isChecked ())
				knsync_cfg->writeEntry ("W3Action", "SurnameAddName");
			else
				knsync_cfg->writeEntry ("W3Action", "Ignore");

	if(W3PAskRB->isChecked ())
		knsync_cfg->writeEntry ("W3PAction", "Ask");
	else
		knsync_cfg->writeEntry ("W3PAction", "Ignore");

	knsync_cfg->setGroup ("Address Format");

	knsync_cfg->writeEntry ("PostalAddressFormat", pAddrFrmtLEdit->text ());
	if (pAddressFormatNotifyRB->isChecked ())
		knsync_cfg->writeEntry("PostalAddressFormatMismatch", "Notify");
	else
		knsync_cfg->writeEntry("PostalAddressFormatMismatch", "Ignore");

	knsync_cfg->setGroup ("Caller Groups");
	knsync_cfg->writeEntry ("Group0Name", groupName0LE->text ());
	knsync_cfg->writeEntry ("Group1Name", groupName1LE->text ());
	knsync_cfg->writeEntry ("Group2Name", groupName2LE->text ());
	knsync_cfg->writeEntry ("Group3Name", groupName3LE->text ());
	knsync_cfg->writeEntry ("Group4Name", groupName4LE->text ());
	knsync_cfg->writeEntry ("Group5Name", groupName5LE->text ());

	knsync_cfg->sync ();
	close ();
	emit confChanged ();
}

void cfgDlg::cancelClicked ()
{
	close();
}

void cfgDlg::pAddrLEChanged ( const QString & newText )
{
	QString replacementText=newText;
	replacementText.replace (QString ("%S"), QString ("1 Bond Street"));
	replacementText.replace (QString ("%R"), QString ("London"));
	replacementText.replace (QString ("%L"), QString ("Mains Park"));
	replacementText.replace (QString ("%P"), QString ("XY123Z4"));
	replacementText.replace (QString ("%C"), QString ("UK"));
	replacementText.replace (QString ("%"), QString (""));
	pAddrFrmtLEdit_2->setText (replacementText.simplifyWhiteSpace ());
}

void cfgDlg::separatorLEChanged ( const QString & newText )
{
	W2NameSurnameRB->setText ("Assume \"Name"+newText+"Surname\"");
	W2SurnameNameRB->setText ("Assume \"Surname"+newText+"Name\"");

	W3NameAddSurnameRB->setText ("Assume \"Name"+newText+"Additional Name"+newText+"Surname\"");
	W3SurnameAddNameRB->setText ("Assume \"Surname"+newText+"Additional Name"+newText+"Name\"");
}

void cfgDlg::separatorLELostFocus ()
{
	if (separatorLE->text () == "")
		separatorLE->setText (" ");
}
