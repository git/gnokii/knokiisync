/***************************************************************************
                          knokiisync.cpp  -  description
                             -------------------
    begin                : ��� ��� 28 17:02:54 EEST 2003
    copyright            : (C) 2003 by Dimitrios Stasinopoulos
    email                : dimitrios@linea.gr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// include files for QT
#include <qdir.h>
#include <qprinter.h>
#include <qpainter.h>
#include <qtimer.h>

// include files for KDE
#include <kiconloader.h>
#include <kmessagebox.h>
#include <kfiledialog.h>
#include <kmenubar.h>
#include <kstatusbar.h>
#include <klocale.h>
#include <kconfig.h>
#include <kstdaction.h>
#include <klocale.h>
#include <kpassivepopup.h>

// application specific includes
#include "knokiisync.h"
#include "syncview.h"

extern KApplication * globalKApp;

#define ID_STATUS_MSG 1

KnokiiSyncApp::KnokiiSyncApp(QWidget* , const char* name):KMainWindow(0, name)
{
	KConfig *knsync_cfg = globalKApp->config ();

	knsync_cfg->setGroup ("Startup");
	startupConnectToPhone = knsync_cfg->readBoolEntry ("ConnectToPhone", FALSE);

	config=kapp->config();

	///////////////////////////////////////////////////////////////////
	// call inits to invoke all other construction parts
	initStatusBar();
	initActions();
	initView();

	readOptions();

	KApp = 0;
	cfgDialog = 0;

	///////////////////////////////////////////////////////////////////
	// disable actions at startup
	filePrint->setEnabled(false);
	editCut->setEnabled(false);
	editCopy->setEnabled(false);
	editPaste->setEnabled(false);


}

KnokiiSyncApp::~KnokiiSyncApp()
{

}

void
KnokiiSyncApp::initActions()
{
	fileClose = KStdAction::close(this, SLOT(slotFileClose()), actionCollection());
	filePrint = KStdAction::print(this, SLOT(slotFilePrint()), actionCollection());
	fileQuit = KStdAction::quit(this, SLOT(slotFileQuit()), actionCollection());
	editCut = KStdAction::cut(this, SLOT(slotEditCut()), actionCollection());
	editCopy = KStdAction::copy(this, SLOT(slotEditCopy()), actionCollection());
	editPaste = KStdAction::paste(this, SLOT(slotEditPaste()), actionCollection());
	viewToolBar = KStdAction::showToolbar(this, SLOT(slotViewToolBar()), actionCollection());
	viewStatusBar = KStdAction::showStatusbar(this, SLOT(slotViewStatusBar()), actionCollection());
	settingsConfigure = new KAction(i18n ("Preferences"), KShortcut::null(), this, SLOT(slotShowCfgDlg()), actionCollection(), "PrefsDialog");

	fileClose->setStatusText(i18n ("Closes the actual document"));
	filePrint ->setStatusText(i18n ("Prints out the actual document"));
	fileQuit->setStatusText(i18n ("Quits the application"));
	editCut->setStatusText(i18n ("Cuts the selected section and puts it to the clipboard"));
	editCopy->setStatusText(i18n ("Copies the selected section to the clipboard"));
	editPaste->setStatusText(i18n ("Pastes the clipboard contents to actual position"));
	viewToolBar->setStatusText(i18n ("Enables/disables the toolbar"));
	viewStatusBar->setStatusText(i18n ("Enables/disables the statusbar"));

	// use the absolute path to your knokiisyncui.rc file for testing purpose in createGUI();
	createGUI();

	KMenuBar * mbar=menuBar();
	int id=mbar->idAt(2);
	QMenuItem * prefsMenu=mbar->findItem(id);
	QPopupMenu * prefsPopup=prefsMenu->popup();
	prefsPopup->insertSeparator();
	settingsConfigure->plug(prefsPopup);
}


void KnokiiSyncApp::initStatusBar()
{
  ///////////////////////////////////////////////////////////////////
  // STATUSBAR
  // TODO: add your own items you need for displaying current application status.
  statusBar()->insertItem(i18n ("Ready."), ID_STATUS_MSG);
}

void KnokiiSyncApp::initView()
{ 
  ////////////////////////////////////////////////////////////////////
  // create the main widget here that is managed by KTMainWindow's view-region and
  // connect the widget to your document to display document contents.

  view = new SyncView(this);
	cfgDialog=new cfgDlg(this);
  setCentralWidget(view);
	connect(view, SIGNAL(connecting()), this, SLOT(slotConnecting()));
	connect(view, SIGNAL(connected()), this, SLOT(slotConnected()));
	connect(view, SIGNAL(disconnecting()), this, SLOT(slotDisconnecting()));
	connect(view, SIGNAL(disconnected()), this, SLOT(slotDisconnected()));
	connect(cfgDialog, SIGNAL(confChanged ()), view, SLOT(slotParseConfig ()));
	view->show();
}

void KnokiiSyncApp::saveOptions()
{	
  config->setGroup("General Options");
  config->writeEntry("Geometry", size());
  config->writeEntry("Show Toolbar", viewToolBar->isChecked());
  config->writeEntry("Show Statusbar",viewStatusBar->isChecked());
  config->writeEntry("ToolBarPos", (int) toolBar("mainToolBar")->barPos());
}


void KnokiiSyncApp::readOptions()
{
	
  config->setGroup("General Options");

  // bar status settings
  bool bViewToolbar = config->readBoolEntry("Show Toolbar", true);
  viewToolBar->setChecked(bViewToolbar);
  slotViewToolBar();

  bool bViewStatusbar = config->readBoolEntry("Show Statusbar", true);
  viewStatusBar->setChecked(bViewStatusbar);
  slotViewStatusBar();


  // bar position settings
  KToolBar::BarPosition toolBarPos;
  toolBarPos=(KToolBar::BarPosition) config->readNumEntry("ToolBarPos", KToolBar::Top);
  toolBar("mainToolBar")->setBarPos(toolBarPos);
	
  QSize size=config->readSizeEntry("Geometry");
  if(!size.isEmpty())
  {
    resize(size);
  }
}

bool KnokiiSyncApp::queryExit()
{
  saveOptions();
  return true;
}

/////////////////////////////////////////////////////////////////////
// SLOT IMPLEMENTATION
/////////////////////////////////////////////////////////////////////

void KnokiiSyncApp::slotFileNewWindow()
{

}

void KnokiiSyncApp::slotFileNew()
{

}

void KnokiiSyncApp::slotFileOpen()
{

}

void KnokiiSyncApp::slotFileSave()
{

}

void KnokiiSyncApp::slotFileSaveAs()
{

}

void KnokiiSyncApp::slotFileClose()
{

}

void KnokiiSyncApp::slotFilePrint()
{

}

void KnokiiSyncApp::slotFileQuit()
{
	if (!startupConnectToPhone)
		{
			slotDisconnecting();
			view->busTerminate();
			slotDisconnected();
		}

  slotStatusMsg(i18n("Exiting..."));
  saveOptions();

	if(globalKApp)
	globalKApp->quit();
}

void KnokiiSyncApp::slotEditCut()
{
  slotStatusMsg(i18n("Cutting selection..."));

  slotStatusMsg(i18n("Ready."));
}

void KnokiiSyncApp::slotEditCopy()
{
  slotStatusMsg(i18n("Copying selection to clipboard..."));

  slotStatusMsg(i18n("Ready."));
}

void KnokiiSyncApp::slotEditPaste()
{
  slotStatusMsg(i18n("Inserting clipboard contents..."));

  slotStatusMsg(i18n("Ready."));
}

void KnokiiSyncApp::slotViewToolBar()
{
  slotStatusMsg(i18n("Toggling toolbar..."));
  ///////////////////////////////////////////////////////////////////
  // turn Toolbar on or off
  if(!viewToolBar->isChecked())
  {
    toolBar("mainToolBar")->hide();
  }
  else
  {
    toolBar("mainToolBar")->show();
  }		

  slotStatusMsg(i18n("Ready."));
}

void KnokiiSyncApp::slotViewStatusBar()
{
  slotStatusMsg(i18n("Toggle the statusbar..."));
  ///////////////////////////////////////////////////////////////////
  //turn Statusbar on or off
  if(!viewStatusBar->isChecked())
  {
    statusBar()->hide();
  }
  else
  {
    statusBar()->show();
  }

  slotStatusMsg(i18n ("Ready."));
}


void KnokiiSyncApp::slotStatusMsg(const QString &text)
{
  ///////////////////////////////////////////////////////////////////
  // change status message permanently
  statusBar()->clear();
  statusBar()->changeItem(text, ID_STATUS_MSG);
}

/** No descriptions */
void KnokiiSyncApp::slotConnecting()
{
KStatusBar * sb=statusBar();
sb->changeItem(i18n ("Connecting..."), ID_STATUS_MSG);
kapp->processEvents();
}

/** No descriptions */
void KnokiiSyncApp::slotConnected()
{
KStatusBar * sb=statusBar();
sb->changeItem(i18n ("Connected"), ID_STATUS_MSG);
KPassivePopup::message(i18n ("Connected to phone."), this );
kapp->processEvents();
}

/** No descriptions */
void KnokiiSyncApp::slotDisconnecting()
{
KStatusBar * sb=statusBar();
sb->changeItem(i18n ("Disconnecting..."), ID_STATUS_MSG);
kapp->processEvents();
}

/** No descriptions */
void KnokiiSyncApp::slotDisconnected()
{
KStatusBar * sb=statusBar();
sb->changeItem(i18n ("Disconnected"), ID_STATUS_MSG);
KPassivePopup::message(i18n ("Disconnected from phone."), this );
kapp->processEvents();
}

void KnokiiSyncApp::slotShowCfgDlg()
{
	if(!cfgDialog)
		cfgDialog=new cfgDlg(this);

	connect (cfgDialog, SIGNAL (confChanged ()), this, SLOT ( slotCfgDlgOKed()));
	cfgDialog->show();
}

void KnokiiSyncApp::slotCfgDlgOKed()
{
	if (view)
		view->slotParseConfig ();
}
