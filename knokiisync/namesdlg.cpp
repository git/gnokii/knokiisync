/****************************************************************************
** Form implementation generated from reading ui file 'namesdlg.ui'
**
** Created: Σαβ Αύγ 16 16:20:29 2003
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "namesdlg.h"

#include <qvariant.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qframe.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include "namesdlg.ui.h"

/* 
 *  Constructs a NamesDialog as a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
NamesDialog::NamesDialog( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )

{
    if ( !name )
	setName( "NamesDialog" );

    frame3 = new QFrame( this, "frame3" );
    frame3->setGeometry( QRect( 10, 10, 400, 50 ) );
    frame3->setFrameShape( QFrame::StyledPanel );
    frame3->setFrameShadow( QFrame::Raised );

    nameCB1 = new QComboBox( FALSE, frame3, "nameCB1" );
    nameCB1->setGeometry( QRect( 171, 10, 220, 30 ) );

    typeCB1 = new QComboBox( FALSE, frame3, "typeCB1" );
    typeCB1->setGeometry( QRect( 10, 10, 150, 30 ) );

    frame3_2 = new QFrame( this, "frame3_2" );
    frame3_2->setGeometry( QRect( 10, 60, 400, 50 ) );
    frame3_2->setFrameShape( QFrame::StyledPanel );
    frame3_2->setFrameShadow( QFrame::Raised );

    typeCB2 = new QComboBox( FALSE, frame3_2, "typeCB2" );
    typeCB2->setGeometry( QRect( 10, 10, 150, 30 ) );

    nameCB2 = new QComboBox( FALSE, frame3_2, "nameCB2" );
    nameCB2->setGeometry( QRect( 171, 10, 220, 30 ) );

    frame3_2_2 = new QFrame( this, "frame3_2_2" );
    frame3_2_2->setGeometry( QRect( 10, 110, 400, 50 ) );
    frame3_2_2->setFrameShape( QFrame::StyledPanel );
    frame3_2_2->setFrameShadow( QFrame::Raised );

    typeCB3 = new QComboBox( FALSE, frame3_2_2, "typeCB3" );
    typeCB3->setGeometry( QRect( 10, 10, 150, 30 ) );

    nameCB3 = new QComboBox( FALSE, frame3_2_2, "nameCB3" );
    nameCB3->setGeometry( QRect( 171, 10, 220, 30 ) );

    OKButton = new QPushButton( this, "OKButton" );
    OKButton->setGeometry( QRect( 30, 240, 140, 30 ) );

    cancelButton = new QPushButton( this, "cancelButton" );
    cancelButton->setGeometry( QRect( 250, 240, 140, 30 ) );

    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setGeometry( QRect( 10, 170, 161, 25 ) );

    frmtNameLE = new QLineEdit( this, "frmtNameLE" );
    frmtNameLE->setGeometry( QRect( 180, 170, 220, 25 ) );

    rememberChkBox = new QCheckBox( this, "rememberChkBox" );
    rememberChkBox->setGeometry( QRect( 90, 210, 240, 21 ) );
    languageChange();
    resize( QSize(419, 279).expandedTo(minimumSizeHint()) );

    // signals and slots connections
    connect( OKButton, SIGNAL( clicked() ), this, SLOT( OKButtonClicked() ) );
    connect( cancelButton, SIGNAL( clicked() ), this, SLOT( cancelButtonClicked() ) );
    connect( typeCB1, SIGNAL( activated(const QString&) ), this, SLOT( typeCB1Changed(const QString&) ) );
    connect( typeCB2, SIGNAL( activated(const QString&) ), this, SLOT( typeCB2Changed(const QString&) ) );
    connect( typeCB3, SIGNAL( activated(const QString&) ), this, SLOT( typeCB3Changed(const QString&) ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
NamesDialog::~NamesDialog()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void NamesDialog::languageChange()
{
    setCaption( tr( "Select names" ) );
    typeCB1->clear();
    typeCB1->insertItem( tr( "Name" ) );
    typeCB1->insertItem( tr( "Surname" ) );
    typeCB1->insertItem( tr( "Additional Name" ) );
    typeCB1->insertItem( QString::null );
    typeCB2->clear();
    typeCB2->insertItem( tr( "Name" ) );
    typeCB2->insertItem( tr( "Surname" ) );
    typeCB2->insertItem( tr( "Additional Name" ) );
    typeCB2->insertItem( QString::null );
    typeCB3->clear();
    typeCB3->insertItem( tr( "Name" ) );
    typeCB3->insertItem( tr( "Surname" ) );
    typeCB3->insertItem( tr( "Additional Name" ) );
    typeCB3->insertItem( QString::null );
    OKButton->setText( tr( "OK" ) );
    cancelButton->setText( tr( "Cancel" ) );
    textLabel1->setText( tr( "Formatted Name:" ) );
    rememberChkBox->setText( tr( "Remember this contact the next time" ) );
}

